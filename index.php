<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Minsk DataViz Day");
?>
<?$APPLICATION->IncludeComponent(
	"bitrix:news.list", 
	"slider", 
	array(
		"IBLOCK_ID" => "7",
		"IBLOCK_TYPE" => "magwai_conf",
		"ACTIVE_DATE_FORMAT" => "d.m.Y",
		"ADD_SECTIONS_CHAIN" => "N",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "Y",
		"CACHE_TIME" => "60",
		"CACHE_TYPE" => "A",
		"CHECK_DATES" => "Y",
		"DETAIL_URL" => "",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"DISPLAY_DATE" => "Y",
		"DISPLAY_NAME" => "Y",
		"DISPLAY_PICTURE" => "Y",
		"DISPLAY_PREVIEW_TEXT" => "Y",
		"DISPLAY_TOP_PAGER" => "N",
		"FIELD_CODE" => array(
			0 => "ID",
			1 => "CODE",
			2 => "XML_ID",
			3 => "NAME",
			4 => "SORT",
			5 => "PREVIEW_TEXT",
			6 => "DATE_ACTIVE_FROM",
			7 => "ACTIVE_FROM",
			8 => "DATE_ACTIVE_TO",
			9 => "ACTIVE_TO",
			10 => "",
		),
		"FILTER_NAME" => "",
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
		"INCLUDE_SUBSECTIONS" => "Y",
		"MESSAGE_404" => "",
		"NEWS_COUNT" => "20",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_TEMPLATE" => ".default",
		"PAGER_TITLE" => "",
		"PARENT_SECTION" => "",
		"PARENT_SECTION_CODE" => "",
		"PREVIEW_TRUNCATE_LEN" => "",
		"PROPERTY_CODE" => array(
			0 => "SUBTITLE",
			1 => "VIDEO",
			2 => "TIMER",
			3 => "BUTTON_NAME",
			4 => "SHOW_TYPE",
			5 => "BUTTON_LINK",
			6 => "",
			7 => "",
			8 => "",
		),
		"SET_BROWSER_TITLE" => "N",
		"SET_LAST_MODIFIED" => "N",
		"SET_META_DESCRIPTION" => "N",
		"SET_META_KEYWORDS" => "N",
		"SET_STATUS_404" => "N",
		"SET_TITLE" => "N",
		"SHOW_404" => "N",
		"SORT_BY1" => "SORT",
		"SORT_BY2" => "ID",
		"SORT_ORDER1" => "DESC",
		"SORT_ORDER2" => "DESC",
		"COMPONENT_TEMPLATE" => "slider",
		"TIMER_END_TEXT" => "Время регистрации закончилось",
		"RESIZE_PREVIEW" => ""
	),
	false,
	array(
		"ACTIVE_COMPONENT" => "Y"
	)
);?>
<div class="info-sect">
	<? $svgPath = $_SERVER['DOCUMENT_ROOT'] . SITE_DIR . 'include/conf/svg.php'; ?>
	<? if(file_exists($svgPath)) require $svgPath ?>
	<div class="container-fluid">
		<div class="row">
			<div class="col-xs-6 col-md-3">
				<div class="info-item">
					<div class="icon-i-i active"><svg width="34px" height="34px" viewBox="-1 -1 36 36"><use xlink:href="#icon1"></use></svg></div>
					<div class="overflow-hidden hidden-xs">
						<div class="inline-block"><strong><?$APPLICATION->IncludeComponent("bitrix:main.include", "", array(
										"AREA_FILE_SHOW" => "file",
										"PATH" => SITE_DIR . "include/conf/info-address.php",
									)
								);?></strong><?$APPLICATION->IncludeComponent("bitrix:main.include", "", array(
									"AREA_FILE_SHOW" => "file",
									"PATH" => SITE_DIR . "include/conf/info-address-subtitle.php",
								)
							);?></div>
					</div>
				</div>
			</div>
			<div class="col-xs-6 col-md-3">
				<div class="info-item">
					<div class="icon-i-i"><svg width="31px" height="30px" viewBox="-1 -1 33 32"><use xlink:href="#icon2"></use></svg></div>
					<div class="overflow-hidden hidden-xs">
						<div class="inline-block"><strong><?$APPLICATION->IncludeComponent("bitrix:main.include", "", array(
										"AREA_FILE_SHOW" => "file",
										"PATH" => SITE_DIR . "include/conf/info-date.php",
									)
								);?></strong><?$APPLICATION->IncludeComponent("bitrix:main.include", "", array(
									"AREA_FILE_SHOW" => "file",
									"PATH" => SITE_DIR . "include/conf/info-date-subtitle.php",
								)
							);?></div>
					</div>
				</div>
			</div>
			<div class="clearboth visible-xs visible-sm"></div>
			<div class="col-xs-6 col-md-3">
				<div class="info-item">
					<div class="icon-i-i"><svg width="36px" height="28" viewBox="-1 -1 38 30"><use xlink:href="#icon3"></use></svg></div>
					<div class="overflow-hidden hidden-xs">
						<div class="inline-block"><strong><?$APPLICATION->IncludeComponent("bitrix:main.include", "", array(
										"AREA_FILE_SHOW" => "file",
										"PATH" => SITE_DIR . "include/conf/info-speakers.php",
									)
								);?></strong><?$APPLICATION->IncludeComponent("bitrix:main.include", "", array(
									"AREA_FILE_SHOW" => "file",
									"PATH" => SITE_DIR . "include/conf/info-speakers-subtitle.php",
								)
							);?></div>
					</div>
				</div>
			</div>
			<div class="col-xs-6 col-md-3">
				<div class="info-item">
					<div class="icon-i-i"><svg width="38px" height="24" viewBox="-1 -1 40 26"><use xlink:href="#icon4"></use></svg></div>
					<div class="overflow-hidden hidden-xs">
						<div class="inline-block"><strong><?$APPLICATION->IncludeComponent("bitrix:main.include", "", array(
										"AREA_FILE_SHOW" => "file",
										"PATH" => SITE_DIR . "include/conf/info-invite.php",
									)
								);?></strong><?$APPLICATION->IncludeComponent("bitrix:main.include", "", array(
									"AREA_FILE_SHOW" => "file",
									"PATH" => SITE_DIR . "include/conf/info-invite-subtitle.php",
								)
							);?></div>
					</div>
				</div>
			</div>
		</div>
		<div class="txt-i-i-box visible-xs">
		<span class="txt-i-i active"><strong><?$APPLICATION->IncludeComponent("bitrix:main.include", "", array(
						"AREA_FILE_SHOW" => "file",
						"PATH" => SITE_DIR . "include/conf/info-address.php",
					)
				);?></strong><?$APPLICATION->IncludeComponent("bitrix:main.include", "", array(
					"AREA_FILE_SHOW" => "file",
					"PATH" => SITE_DIR . "include/conf/info-address-subtitle.php",
				)
			);?></span>
		<span class="txt-i-i"><strong><?$APPLICATION->IncludeComponent("bitrix:main.include", "", array(
						"AREA_FILE_SHOW" => "file",
						"PATH" => SITE_DIR . "include/conf/info-date.php",
					)
				);?></strong><?$APPLICATION->IncludeComponent("bitrix:main.include", "", array(
					"AREA_FILE_SHOW" => "file",
					"PATH" => SITE_DIR . "include/conf/info-date-subtitle.php",
				)
			);?></span>
		<span class="txt-i-i"><strong><?$APPLICATION->IncludeComponent("bitrix:main.include", "", array(
						"AREA_FILE_SHOW" => "file",
						"PATH" => SITE_DIR . "include/conf/info-speakers.php",
					)
				);?></strong><?$APPLICATION->IncludeComponent("bitrix:main.include", "", array(
					"AREA_FILE_SHOW" => "file",
					"PATH" => SITE_DIR . "include/conf/info-speakers-subtitle.php",
				)
			);?></span>
		<span class="txt-i-i"><strong><?$APPLICATION->IncludeComponent("bitrix:main.include", "", array(
						"AREA_FILE_SHOW" => "file",
						"PATH" => SITE_DIR . "include/conf/info-invite.php",
					)
				);?></strong><?$APPLICATION->IncludeComponent("bitrix:main.include", "", array(
					"AREA_FILE_SHOW" => "file",
					"PATH" => SITE_DIR . "include/conf/info-invite-subtitle.php",
				)
			);?></span>
		</div>
	</div>
</div>
<!--<section class="tableau-placeholder sect-type1" id="tableau-placeholder">-->
<!--    <div class="container-fluid">-->
<!--        <div class='tableauPlaceholder' id='viz1506510011706' style='position: relative'><noscript><a href='#'><img alt='DataVizDay ' src='https:&#47;&#47;public.tableau.com&#47;static&#47;images&#47;Da&#47;DataVizDay_v4&#47;DataVizDay&#47;1_rss.png' style='border: none' /></a></noscript><object class='tableauViz'  style='display:none;'><param name='host_url' value='https%3A%2F%2Fpublic.tableau.com%2F' /> <param name='embed_code_version' value='2' /> <param name='site_root' value='' /><param name='name' value='DataVizDay_v4&#47;DataVizDay' /><param name='tabs' value='no' /><param name='toolbar' value='yes' /><param name='static_image' value='https:&#47;&#47;public.tableau.com&#47;static&#47;images&#47;Da&#47;DataVizDay_v4&#47;DataVizDay&#47;1.png' /> <param name='animate_transition' value='yes' /><param name='display_static_image' value='yes' /><param name='display_spinner' value='yes' /><param name='display_overlay' value='yes' /><param name='display_count' value='yes' /></object></div>                <script type='text/javascript'>                    var divElement = document.getElementById('viz1506510011706');                    var vizElement = divElement.getElementsByTagName('object')[0];                    vizElement.style.width='1050px';vizElement.style.height='1260px';                    var scriptElement = document.createElement('script');                    scriptElement.src = 'https://public.tableau.com/javascripts/api/viz_v1.js';                    vizElement.parentNode.insertBefore(scriptElement, vizElement);                </script>-->
<!--    </div>-->
<!--</section>-->
<?
  $APPLICATION->IncludeComponent(
	"bitrix:news.list",
	"conference",
	array(
		"IBLOCK_ID" => "8",
		"IBLOCK_TYPE" => "magwai_conf",
		"ACTIVE_DATE_FORMAT" => "d.m.Y",
		"ADD_SECTIONS_CHAIN" => "N",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "Y",
		"CACHE_TIME" => "36000000",
		"CACHE_TYPE" => "A",
		"CHECK_DATES" => "Y",
		"DETAIL_URL" => "",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"DISPLAY_DATE" => "Y",
		"DISPLAY_NAME" => "Y",
		"DISPLAY_PICTURE" => "Y",
		"DISPLAY_PREVIEW_TEXT" => "Y",
		"DISPLAY_TOP_PAGER" => "N",
		"FIELD_CODE" => array(
			0 => "ID",
			1 => "CODE",
			2 => "XML_ID",
			3 => "NAME",
			4 => "SORT",
			5 => "PREVIEW_TEXT",
			6 => "DATE_ACTIVE_FROM",
			7 => "ACTIVE_FROM",
			8 => "DATE_ACTIVE_TO",
			9 => "ACTIVE_TO",
			10 => "",
		),
		"FILTER_NAME" => "",
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
		"INCLUDE_SUBSECTIONS" => "Y",
		"MESSAGE_404" => "",
		"NEWS_COUNT" => "20",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_TEMPLATE" => ".default",
		"PAGER_TITLE" => "",
		"PARENT_SECTION" => "",
		"PARENT_SECTION_CODE" => "",
		"PREVIEW_TRUNCATE_LEN" => "",
		"PROPERTY_CODE" => array(
			0 => "ICON",
			1 => "ICON2",
		),
		"SET_BROWSER_TITLE" => "N",
		"SET_LAST_MODIFIED" => "N",
		"SET_META_DESCRIPTION" => "N",
		"SET_META_KEYWORDS" => "N",
		"SET_STATUS_404" => "N",
		"SET_TITLE" => "N",
		"SHOW_404" => "N",
		"SORT_BY1" => "SORT",
		"SORT_BY2" => "ID",
		"SORT_ORDER1" => "DESC",
		"SORT_ORDER2" => "ASC",
		"COMPONENT_TEMPLATE" => "conference",
		"STRICT_SECTION_CHECK" => "N",
		"COMPOSITE_FRAME_MODE" => "A",
		"COMPOSITE_FRAME_TYPE" => "AUTO"
	),
	false
);
  ?>
<?$APPLICATION->IncludeComponent(
	"bitrix:news.list",
	"organizers",
	array(
		"IBLOCK_ID" => "3",
		"IBLOCK_TYPE" => "magwai_conf",
		"ACTIVE_DATE_FORMAT" => "d.m.Y",
		"ADD_SECTIONS_CHAIN" => "N",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "Y",
		"CACHE_TIME" => "36000000",
		"CACHE_TYPE" => "A",
		"CHECK_DATES" => "Y",
		"DETAIL_URL" => "",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"DISPLAY_DATE" => "Y",
		"DISPLAY_NAME" => "Y",
		"DISPLAY_PICTURE" => "Y",
		"DISPLAY_PREVIEW_TEXT" => "Y",
		"DISPLAY_TOP_PAGER" => "N",
		"FIELD_CODE" => array(
			0 => "ID",
			1 => "CODE",
			2 => "XML_ID",
			3 => "NAME",
			4 => "SORT",
			5 => "PREVIEW_PICTURE",
			6 => "",
		),
		"FILTER_NAME" => "",
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
		"INCLUDE_SUBSECTIONS" => "Y",
		"MESSAGE_404" => "",
		"NEWS_COUNT" => "20",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_TEMPLATE" => ".default",
		"PAGER_TITLE" => "",
		"PARENT_SECTION" => "",
		"PARENT_SECTION_CODE" => "",
		"PREVIEW_TRUNCATE_LEN" => "",
		"PROPERTY_CODE" => array(
			0 => "",
			1 => "",
		),
		"SET_BROWSER_TITLE" => "N",
		"SET_LAST_MODIFIED" => "N",
		"SET_META_DESCRIPTION" => "N",
		"SET_META_KEYWORDS" => "N",
		"SET_STATUS_404" => "N",
		"SET_TITLE" => "N",
		"SHOW_404" => "N",
		"SORT_BY1" => "SORT",
		"SORT_BY2" => "ID",
		"SORT_ORDER1" => "ASC",
		"SORT_ORDER2" => "ASC",
		"COMPONENT_TEMPLATE" => "organizers"
	),
	false,
	array(
	"ACTIVE_COMPONENT" => "N"
	)
);?>
<?$APPLICATION->IncludeComponent(
	"magwai:block",
	"online-translation",
	array(
		"ID" => "online-translation",
		"COMPONENT_TEMPLATE" => "online-translation"
	),
	false,
	array(
		"ACTIVE_COMPONENT" => "Y"
	)
);?>
<?$APPLICATION->IncludeComponent(
	"bitrix:news.list", 
	"ns_topics", 
	array(
		"IBLOCK_ID" => "14",
		"IBLOCK_TYPE" => "magwai_conf",
		"ACTIVE_DATE_FORMAT" => "d.m.Y",
		"ADD_SECTIONS_CHAIN" => "N",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "Y",
		"CACHE_TIME" => "36000000",
		"CACHE_TYPE" => "A",
		"CHECK_DATES" => "Y",
		"DETAIL_URL" => "",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"DISPLAY_DATE" => "Y",
		"DISPLAY_NAME" => "Y",
		"DISPLAY_PICTURE" => "Y",
		"DISPLAY_PREVIEW_TEXT" => "Y",
		"DISPLAY_TOP_PAGER" => "N",
		"FIELD_CODE" => array(
			0 => "ID",
			1 => "CODE",
			2 => "XML_ID",
			3 => "NAME",
			4 => "SORT",
			5 => "PREVIEW_TEXT",
			6 => "DATE_ACTIVE_FROM",
			7 => "ACTIVE_FROM",
			8 => "DATE_ACTIVE_TO",
			9 => "ACTIVE_TO",
			10 => "",
		),
		"FILTER_NAME" => "",
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
		"INCLUDE_SUBSECTIONS" => "Y",
		"MESSAGE_404" => "",
		"NEWS_COUNT" => "20",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_TEMPLATE" => ".default",
		"PAGER_TITLE" => "",
		"PARENT_SECTION" => "",
		"PARENT_SECTION_CODE" => "",
		"PREVIEW_TRUNCATE_LEN" => "",
		"PROPERTY_CODE" => array(
			0 => "ICON",
			1 => "ICON2",
		),
		"SET_BROWSER_TITLE" => "N",
		"SET_LAST_MODIFIED" => "N",
		"SET_META_DESCRIPTION" => "N",
		"SET_META_KEYWORDS" => "N",
		"SET_STATUS_404" => "N",
		"SET_TITLE" => "N",
		"SHOW_404" => "N",
		"SORT_BY1" => "SORT",
		"SORT_BY2" => "ID",
		"SORT_ORDER1" => "DESC",
		"SORT_ORDER2" => "ASC",
		"COMPONENT_TEMPLATE" => "ns_topics",
		"STRICT_SECTION_CHECK" => "N",
		"COMPOSITE_FRAME_MODE" => "A",
		"COMPOSITE_FRAME_TYPE" => "AUTO"
	),
	false
);?>
<div class="container-fluid">
    <a href="#program" class="btn btn-style1 btn-listeners pull-right hidden-xs"><?$APPLICATION->IncludeComponent(
            "bitrix:main.include",
            "",
            array(
                "AREA_FILE_SHOW" => "file",
                "PATH" => SITE_DIR . "include/conf/registration-button_2.php",
            )
        );?></a>
</div>
<?$APPLICATION->IncludeComponent(
    "bitrix:news.list",
    "speakers",
    array(
        "IBLOCK_ID" => "5",
        "IBLOCK_TYPE" => "magwai_conf",
        "ACTIVE_DATE_FORMAT" => "d.m.Y",
        "ADD_SECTIONS_CHAIN" => "N",
        "AJAX_MODE" => "N",
        "AJAX_OPTION_ADDITIONAL" => "",
        "AJAX_OPTION_HISTORY" => "N",
        "AJAX_OPTION_JUMP" => "N",
        "AJAX_OPTION_STYLE" => "Y",
        "CACHE_FILTER" => "N",
        "CACHE_GROUPS" => "Y",
        "CACHE_TIME" => "36000000",
        "CACHE_TYPE" => "A",
        "CHECK_DATES" => "Y",
        "DETAIL_URL" => "",
        "DISPLAY_BOTTOM_PAGER" => "Y",
        "DISPLAY_DATE" => "Y",
        "DISPLAY_NAME" => "Y",
        "DISPLAY_PICTURE" => "Y",
        "DISPLAY_PREVIEW_TEXT" => "Y",
        "DISPLAY_TOP_PAGER" => "N",
        "FIELD_CODE" => array(
            0 => "ID",
            1 => "CODE",
            2 => "XML_ID",
            3 => "NAME",
            4 => "SORT",
            5 => "PREVIEW_TEXT",
            6 => "PREVIEW_PICTURE",
            7 => "DATE_ACTIVE_FROM",
            8 => "ACTIVE_FROM",
            9 => "DATE_ACTIVE_TO",
            10 => "ACTIVE_TO",
            11 => "",
        ),
        "FILTER_NAME" => "",
        "HIDE_LINK_WHEN_NO_DETAIL" => "N",
        "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
        "INCLUDE_SUBSECTIONS" => "Y",
        "MESSAGE_404" => "",
        "NEWS_COUNT" => "30",
        "PAGER_BASE_LINK_ENABLE" => "N",
        "PAGER_DESC_NUMBERING" => "N",
        "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
        "PAGER_SHOW_ALL" => "N",
        "PAGER_SHOW_ALWAYS" => "N",
        "PAGER_TEMPLATE" => ".default",
        "PAGER_TITLE" => "",
        "PARENT_SECTION" => "",
        "PARENT_SECTION_CODE" => "",
        "PREVIEW_TRUNCATE_LEN" => "",
        "PROPERTY_CODE" => array(
            0 => "POSITION",
            1 => "FB_LINK",
            2 => "TW_LINK",
            3 => "VK_LINK",
            4 => "",
        ),
        "SET_BROWSER_TITLE" => "N",
        "SET_LAST_MODIFIED" => "N",
        "SET_META_DESCRIPTION" => "N",
        "SET_META_KEYWORDS" => "N",
        "SET_STATUS_404" => "N",
        "SET_TITLE" => "N",
        "SHOW_404" => "N",
        "SORT_BY1" => "SORT",
        "SORT_BY2" => "ID",
        "SORT_ORDER1" => "ASC",
        "SORT_ORDER2" => "ASC",
        "COMPONENT_TEMPLATE" => "speakers"
    ),
    false
);?>
<?$APPLICATION->IncludeComponent(
	"bitrix:news.list", 
	"ns_program", 
	array(
		"IBLOCK_ID" => "6",
		"IBLOCK_TYPE" => "magwai_conf",
		"ACTIVE_DATE_FORMAT" => "d.m.Y",
		"ADD_SECTIONS_CHAIN" => "N",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "Y",
		"CACHE_TIME" => "36000000",
		"CACHE_TYPE" => "A",
		"CHECK_DATES" => "N",
		"DETAIL_URL" => "",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"DISPLAY_DATE" => "Y",
		"DISPLAY_NAME" => "Y",
		"DISPLAY_PICTURE" => "Y",
		"DISPLAY_PREVIEW_TEXT" => "Y",
		"DISPLAY_TOP_PAGER" => "N",
		"FIELD_CODE" => array(
			0 => "ID",
			1 => "CODE",
			2 => "XML_ID",
			3 => "NAME",
			4 => "SORT",
			5 => "PREVIEW_TEXT",
			6 => "PREVIEW_PICTURE",
			7 => "DATE_ACTIVE_FROM",
			8 => "ACTIVE_FROM",
			9 => "DATE_ACTIVE_TO",
			10 => "ACTIVE_TO",
			11 => "TOP_BLOCK",
		),
		"FILTER_NAME" => "",
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
		"INCLUDE_SUBSECTIONS" => "Y",
		"MESSAGE_404" => "",
		"NEWS_COUNT" => "100",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_TEMPLATE" => ".default",
		"PAGER_TITLE" => "",
		"PARENT_SECTION" => "",
		"PARENT_SECTION_CODE" => "programma-na-2017-god",
		"PREVIEW_TRUNCATE_LEN" => "",
		"PROPERTY_CODE" => array(
			0 => "",
			1 => "HALL",
			2 => "SPEAKER",
			3 => "",
		),
		"SET_BROWSER_TITLE" => "N",
		"SET_LAST_MODIFIED" => "N",
		"SET_META_DESCRIPTION" => "N",
		"SET_META_KEYWORDS" => "N",
		"SET_STATUS_404" => "N",
		"SET_TITLE" => "N",
		"SHOW_404" => "N",
		"SORT_BY1" => "PROPERTY_HALL",
		"SORT_BY2" => "ACTIVE_FROM",
		"SORT_ORDER1" => "ASC",
		"SORT_ORDER2" => "ASC",
		"COMPONENT_TEMPLATE" => "ns_program",
		"DISPLAY_SPEAKER_POSITION" => "N",
		"RESIZE_PREVIEW" => "Y",
		"RESIZE_TYPE_PREVIEW" => "0",
		"WIDTH_PREVIEW" => "103",
		"HEIGHT_PREVIEW" => "103",
		"STRICT_SECTION_CHECK" => "N",
		"COMPOSITE_FRAME_MODE" => "A",
		"COMPOSITE_FRAME_TYPE" => "AUTO"
	),
	false
);?>
<?$APPLICATION->IncludeComponent(
	"magwai:block",
	"online-description",
	array(
		"ID" => "online-translation",
		"COMPONENT_TEMPLATE" => "online-description",
		"COMPOSITE_FRAME_MODE" => "A",
		"COMPOSITE_FRAME_TYPE" => "AUTO"
	),
	false,
	array(
		"ACTIVE_COMPONENT" => "Y"
	)
);?>
<?$APPLICATION->IncludeComponent("magwai:block", "press-center", array(
	"ID" => "press-center",
		"COMPONENT_TEMPLATE" => "press-center"
	),
	false,
	array(
	"ACTIVE_COMPONENT" => "N"
	)
);?>
<?
//  $APPLICATION->IncludeComponent(
//  "bitrix:news.list",
//  "ns_reviews",
//  array(
//    "IBLOCK_ID" => "16",
//    "IBLOCK_TYPE" => "magwai_conf",
//    "ACTIVE_DATE_FORMAT" => "d.m.Y",
//    "ADD_SECTIONS_CHAIN" => "N",
//    "AJAX_MODE" => "N",
//    "AJAX_OPTION_ADDITIONAL" => "",
//    "AJAX_OPTION_HISTORY" => "N",
//    "AJAX_OPTION_JUMP" => "N",
//    "AJAX_OPTION_STYLE" => "Y",
//    "CACHE_FILTER" => "N",
//    "CACHE_GROUPS" => "Y",
//    "CACHE_TIME" => "36000000",
//    "CACHE_TYPE" => "A",
//    "CHECK_DATES" => "Y",
//    "DETAIL_URL" => "",
//    "DISPLAY_BOTTOM_PAGER" => "Y",
//    "DISPLAY_DATE" => "Y",
//    "DISPLAY_NAME" => "Y",
//    "DISPLAY_PICTURE" => "Y",
//    "DISPLAY_PREVIEW_TEXT" => "Y",
//    "DISPLAY_TOP_PAGER" => "N",
//    "FIELD_CODE" => array(
//      0 => "ID",
//      1 => "CODE",
//      2 => "XML_ID",
//      3 => "NAME",
//      4 => "SORT",
//      5 => "PREVIEW_TEXT",
//      6 => "PREVIEW_PICTURE",
//      7 => "DATE_ACTIVE_FROM",
//      8 => "ACTIVE_FROM",
//      9 => "DATE_ACTIVE_TO",
//      10 => "ACTIVE_TO",
//      11 => "",
//    ),
//    "FILTER_NAME" => "",
//    "HIDE_LINK_WHEN_NO_DETAIL" => "N",
//    "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
//    "INCLUDE_SUBSECTIONS" => "Y",
//    "MESSAGE_404" => "",
//    "NEWS_COUNT" => "20",
//    "PAGER_BASE_LINK_ENABLE" => "N",
//    "PAGER_DESC_NUMBERING" => "N",
//    "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
//    "PAGER_SHOW_ALL" => "N",
//    "PAGER_SHOW_ALWAYS" => "N",
//    "PAGER_TEMPLATE" => ".default",
//    "PAGER_TITLE" => "",
//    "PARENT_SECTION" => "",
//    "PARENT_SECTION_CODE" => "",
//    "PREVIEW_TRUNCATE_LEN" => "",
//    "PROPERTY_CODE" => array(
//      0 => "",
//    ),
//    "SET_BROWSER_TITLE" => "N",
//    "SET_LAST_MODIFIED" => "N",
//    "SET_META_DESCRIPTION" => "N",
//    "SET_META_KEYWORDS" => "N",
//    "SET_STATUS_404" => "N",
//    "SET_TITLE" => "N",
//    "SHOW_404" => "N",
//    "SORT_BY1" => "SORT",
//    "SORT_BY2" => "ID",
//    "SORT_ORDER1" => "ASC",
//    "SORT_ORDER2" => "DESC",
//    "COMPONENT_TEMPLATE" => "news"
//  ),
//  false
//);
  ?>
<?
//  $APPLICATION->IncludeComponent(
//    "bitrix:news.list",
//    "ns_photogalery",
//    array(
//        "IBLOCK_ID" => "17",
//        "IBLOCK_TYPE" => "magwai_conf",
//        "ACTIVE_DATE_FORMAT" => "d.m.Y",
//        "ADD_SECTIONS_CHAIN" => "N",
//        "AJAX_MODE" => "N",
//        "AJAX_OPTION_ADDITIONAL" => "",
//        "AJAX_OPTION_HISTORY" => "N",
//        "AJAX_OPTION_JUMP" => "N",
//        "AJAX_OPTION_STYLE" => "Y",
//        "CACHE_FILTER" => "N",
//        "CACHE_GROUPS" => "Y",
//        "CACHE_TIME" => "36000000",
//        "CACHE_TYPE" => "A",
//        "CHECK_DATES" => "Y",
//        "DETAIL_URL" => "",
//        "DISPLAY_BOTTOM_PAGER" => "Y",
//        "DISPLAY_DATE" => "Y",
//        "DISPLAY_NAME" => "Y",
//        "DISPLAY_PICTURE" => "Y",
//        "DISPLAY_PREVIEW_TEXT" => "Y",
//        "DISPLAY_TOP_PAGER" => "N",
//        "FIELD_CODE" => array(
//            0 => "ID",
//            1 => "CODE",
//            2 => "XML_ID",
//            3 => "NAME",
//            4 => "SORT",
//            5 => "PREVIEW_PICTURE",
//            6 => "",
//        ),
//        "FILTER_NAME" => "",
//        "HIDE_LINK_WHEN_NO_DETAIL" => "N",
//        "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
//        "INCLUDE_SUBSECTIONS" => "Y",
//        "MESSAGE_404" => "",
//        "NEWS_COUNT" => "20",
//        "PAGER_BASE_LINK_ENABLE" => "N",
//        "PAGER_DESC_NUMBERING" => "N",
//        "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
//        "PAGER_SHOW_ALL" => "N",
//        "PAGER_SHOW_ALWAYS" => "N",
//        "PAGER_TEMPLATE" => ".default",
//        "PAGER_TITLE" => "",
//        "PARENT_SECTION" => "",
//        "PARENT_SECTION_CODE" => "",
//        "PREVIEW_TRUNCATE_LEN" => "",
//        "PROPERTY_CODE" => array(
//            0 => "",
//        ),
//        "SET_BROWSER_TITLE" => "N",
//        "SET_LAST_MODIFIED" => "N",
//        "SET_META_DESCRIPTION" => "N",
//        "SET_META_KEYWORDS" => "N",
//        "SET_STATUS_404" => "N",
//        "SET_TITLE" => "N",
//        "SHOW_404" => "N",
//        "SORT_BY1" => "SORT",
//        "SORT_BY2" => "ID",
//        "SORT_ORDER1" => "ASC",
//        "SORT_ORDER2" => "DESC",
//        "COMPONENT_TEMPLATE" => "ns_photogalery"
//    ),
//    false
//);
  ?>
<!--<div class="container-fluid">-->
<!--    <a href="https://drive.google.com/drive/folders/0B07XDFGqbMLSNzNyYzFrY1MtT2s" target="_blank" class="btn btn-style1 btn-listeners pull-right hidden-xs">--><?//$APPLICATION->IncludeComponent(
//            "bitrix:main.include",
//            "",
//            array(
//                "AREA_FILE_SHOW" => "file",
//                "PATH" => SITE_DIR . "include/conf/photogalery-button.php",
//            )
//        );?><!--</a>-->
<!--</div>-->
<?$APPLICATION->IncludeComponent(
	"bitrix:news.list", 
	"partners", 
	array(
		"IBLOCK_ID" => "4",
		"IBLOCK_TYPE" => "magwai_conf",
		"ACTIVE_DATE_FORMAT" => "d.m.Y",
		"ADD_SECTIONS_CHAIN" => "N",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "Y",
		"CACHE_TIME" => "36000000",
		"CACHE_TYPE" => "A",
		"CHECK_DATES" => "Y",
		"DETAIL_URL" => "",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"DISPLAY_DATE" => "Y",
		"DISPLAY_NAME" => "Y",
		"DISPLAY_PICTURE" => "Y",
		"DISPLAY_PREVIEW_TEXT" => "Y",
		"DISPLAY_TOP_PAGER" => "N",
		"FIELD_CODE" => array(
			0 => "ID",
			1 => "CODE",
			2 => "XML_ID",
			3 => "NAME",
			4 => "SORT",
			5 => "PREVIEW_TEXT",
			6 => "DATE_ACTIVE_FROM",
			7 => "ACTIVE_FROM",
			8 => "DATE_ACTIVE_TO",
			9 => "ACTIVE_TO",
			10 => "",
		),
		"FILTER_NAME" => "",
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
		"INCLUDE_SUBSECTIONS" => "Y",
		"MESSAGE_404" => "",
		"NEWS_COUNT" => "20",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_TEMPLATE" => ".default",
		"PAGER_TITLE" => "",
		"PARENT_SECTION" => "",
		"PARENT_SECTION_CODE" => "",
		"PREVIEW_TRUNCATE_LEN" => "",
		"PROPERTY_CODE" => array(
			0 => "LINK",
			1 => "",
		),
		"SET_BROWSER_TITLE" => "N",
		"SET_LAST_MODIFIED" => "N",
		"SET_META_DESCRIPTION" => "N",
		"SET_META_KEYWORDS" => "N",
		"SET_STATUS_404" => "N",
		"SET_TITLE" => "N",
		"SHOW_404" => "N",
		"SORT_BY1" => "SORT",
		"SORT_BY2" => "ID",
		"SORT_ORDER1" => "ASC",
		"SORT_ORDER2" => "ASC",
		"COMPONENT_TEMPLATE" => "partners",
		"ROWS" => "0"
	),
	false
);?>
<?$APPLICATION->IncludeComponent(
    "magwai:block",
    "online-translation2",
    array(
        "ID" => "online-translation2",
        "COMPONENT_TEMPLATE" => "online-translation2",
        "COMPOSITE_FRAME_MODE" => "A",
        "COMPOSITE_FRAME_TYPE" => "AUTO"
    ),
    false,
    array(
        "ACTIVE_COMPONENT" => "Y"
    )
);?>
<?
//  $APPLICATION->IncludeComponent(
//    "magwai:block",
//    "online-description2",
//    array(
//        "ID" => "online-description2",
//        "COMPONENT_TEMPLATE" => "online-description2",
//        "COMPOSITE_FRAME_MODE" => "A",
//        "COMPOSITE_FRAME_TYPE" => "AUTO"
//    ),
//    false,
//    array(
//        "ACTIVE_COMPONENT" => "Y"
//    )
//);
  ?>
<section class="registration sect-type1" id="registration">
    <div class="container-fluid">
      <h2 class="h1 title-style1"><?$APPLICATION->IncludeComponent("bitrix:main.include", "", array(
            "AREA_FILE_SHOW" => "file",
            "PATH" => SITE_DIR . "include/conf/registration.php",
          )
        );?></h2>
      <?
        $successMessagePath = $_SERVER['DOCUMENT_ROOT'] . SITE_DIR . 'include/conf/success-register.php';
        $successMessage = file_exists($successMessagePath) ? file_get_contents($successMessagePath) : '';
        $successMessageDescriptionPath = $_SERVER['DOCUMENT_ROOT'] . SITE_DIR . 'include/conf/success-register-description.php';
        $successMessageDescription = file_exists($successMessageDescriptionPath) ? file_get_contents($successMessageDescriptionPath) : '';
      ?>
      <?$APPLICATION->IncludeComponent(
        "magwai:iblock.element.add.form",
        "register",
        array(
          "IBLOCK_ID" => "13",
          "IBLOCK_TYPE" => "magwai_conf",
          "AJAX_MODE" => "Y",
          "AJAX_OPTION_HISTORY" => "N",
          "AJAX_OPTION_JUMP" => "N",
          "AJAX_OPTION_SHADOW" => "N",
          "AJAX_OPTION_STYLE" => "Y",
          "CUSTOM_TITLE_DATE_ACTIVE_FROM" => "",
          "CUSTOM_TITLE_DATE_ACTIVE_TO" => "",
          "CUSTOM_TITLE_DETAIL_PICTURE" => "",
          "CUSTOM_TITLE_DETAIL_TEXT" => "",
          "CUSTOM_TITLE_IBLOCK_SECTION" => "",
          "CUSTOM_TITLE_NAME" => "Фамилия",
          "CUSTOM_TITLE_PREVIEW_PICTURE" => "",
          "CUSTOM_TITLE_PREVIEW_TEXT" => "",
          "CUSTOM_TITLE_TAGS" => "",
          "DEFAULT_INPUT_SIZE" => "30",
          "DETAIL_TEXT_USE_HTML_EDITOR" => "N",
          "ELEMENT_ASSOC" => "CREATED_BY",
          "EVENT_NAME" => "CONF_REGISTER",
          "GROUPS" => array(
          ),
          "LEVEL_LAST" => "Y",
          "LIST_URL" => "",
          "MAX_FILE_SIZE" => "0",
          "MAX_LEVELS" => "10",
          "MAX_USER_ENTRIES" => "10",
          "PREVIEW_TEXT_USE_HTML_EDITOR" => "N",
          "PROPERTY_CODES" => array(
            0 => "25",
            1 => "26",
            2 => "28",
            3 => "30",
            4 => "32",
            5 => "33",
            6 => "39",
            7 => "40",
            8 => "41",
            9 => "NAME",
          ),
          "PROPERTY_CODES_REQUIRED" => array(
            0 => "25",
            1 => "26",
            2 => "28",
            3 => "33",
            4 => "NAME",
          ),
          "RESIZE_IMAGES" => "Y",
          "SEF_MODE" => "N",
          "STATUS" => "ANY",
          "STATUS_NEW" => "NEW",
          "USER_MESSAGE_ADD" => "",
          "USER_MESSAGE_EDIT" => "",
          "USE_CAPTCHA" => "N",
          "COMPONENT_TEMPLATE" => "register",
          "CHECK_UNIQUE_EMAIL" => "Y",
          "SUCCESS_MESSAGE" => $successMessage,
          "SUCCESS_MESSAGE_DESCRIPTION" => $successMessageDescription,
          "IS_BUY" => "Y",
          "COMPOSITE_FRAME_MODE" => "A",
          "COMPOSITE_FRAME_TYPE" => "AUTO"
        ),
        false
      );?>
    </div>
  </section>
<?
//  $APPLICATION->IncludeComponent("bitrix:news.list", "tickets", array(
//  "IBLOCK_ID" => "10",
//  "IBLOCK_TYPE" => "magwai_conf",
//  "ACTIVE_DATE_FORMAT" => "d.m.Y",
//  "ADD_SECTIONS_CHAIN" => "N",
//  "AJAX_MODE" => "N",
//  "AJAX_OPTION_ADDITIONAL" => "",
//  "AJAX_OPTION_HISTORY" => "N",
//  "AJAX_OPTION_JUMP" => "N",
//  "AJAX_OPTION_STYLE" => "Y",
//  "CACHE_FILTER" => "N",
//  "CACHE_GROUPS" => "Y",
//  "CACHE_TIME" => "36000000",
//  "CACHE_TYPE" => "A",
//  "CHECK_DATES" => "Y",
//  "DETAIL_URL" => "",
//  "DISPLAY_BOTTOM_PAGER" => "Y",
//  "DISPLAY_DATE" => "Y",
//  "DISPLAY_NAME" => "Y",
//  "DISPLAY_PICTURE" => "Y",
//  "DISPLAY_PREVIEW_TEXT" => "Y",
//  "DISPLAY_TOP_PAGER" => "N",
//  "FIELD_CODE" => array(
//    0 => "ID",
//    1 => "CODE",
//    2 => "XML_ID",
//    3 => "NAME",
//    4 => "SORT",
//    5 => "PREVIEW_TEXT",
//    6 => "DATE_ACTIVE_FROM",
//    7 => "ACTIVE_FROM",
//    8 => "DATE_ACTIVE_TO",
//    9 => "ACTIVE_TO",
//    10 => "",
//  ),
//  "FILTER_NAME" => "",
//  "HIDE_LINK_WHEN_NO_DETAIL" => "N",
//  "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
//  "INCLUDE_SUBSECTIONS" => "Y",
//  "MESSAGE_404" => "",
//  "NEWS_COUNT" => "0",
//  "PAGER_BASE_LINK_ENABLE" => "N",
//  "PAGER_DESC_NUMBERING" => "N",
//  "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
//  "PAGER_SHOW_ALL" => "N",
//  "PAGER_SHOW_ALWAYS" => "N",
//  "PAGER_TEMPLATE" => ".default",
//  "PAGER_TITLE" => "",
//  "PARENT_SECTION" => "",
//  "PARENT_SECTION_CODE" => "",
//  "PREVIEW_TRUNCATE_LEN" => "",
//  "PROPERTY_CODE" => array(
//    0 => "IS_NEW",
//    1 => "DISCOUNT",
//    2 => "PRICE",
//    3 => "VALID",
//  ),
//  "SET_BROWSER_TITLE" => "N",
//  "SET_LAST_MODIFIED" => "N",
//  "SET_META_DESCRIPTION" => "N",
//  "SET_META_KEYWORDS" => "N",
//  "SET_STATUS_404" => "N",
//  "SET_TITLE" => "N",
//  "SHOW_404" => "N",
//  "SORT_BY1" => "SORT",
//  "SORT_BY2" => "ID",
//  "SORT_ORDER1" => "ASC",
//  "SORT_ORDER2" => "ASC",
//  "COMPONENT_TEMPLATE" => "tickets",
//  "STRICT_SECTION_CHECK" => "N",
//  "COMPOSITE_FRAME_MODE" => "A",
//  "COMPOSITE_FRAME_TYPE" => "AUTO"
//),
//  false
//);
  ?>
<!--<section class="contacts" id="contacts">-->
	<?
//      $APPLICATION->IncludeComponent(
//	"magwai:map.google.view",
//	"map",
//	array(
//		"CONTROLS" => array(
//			0 => "SMALL_ZOOM_CONTROL",
//		),
//		"INIT_MAP_TYPE" => "ROADMAP",
//		"MAP_DATA" => "a:4:{s:10:\"google_lat\";d:53.86959150852441;s:10:\"google_lon\";d:27.48776914119679;s:12:\"google_scale\";i:15;s:10:\"PLACEMARKS\";a:1:{i:0;a:3:{s:4:\"TEXT\";s:46:\"IBB###RN###пр. Газеты Правда, 11\";s:3:\"LON\";d:27.487122416496;s:3:\"LAT\";d:53.869142130326;}}}",
//		"MAP_HEIGHT" => "100%",
//		"MAP_ID" => "",
//		"MAP_WIDTH" => "100%",
//		"OPTIONS" => array(
//			0 => "ENABLE_DBLCLICK_ZOOM",
//			1 => "ENABLE_DRAGGING",
//			2 => "ENABLE_KEYBOARD",
//		),
//		"COMPONENT_TEMPLATE" => "map",
//		"API_KEY" => "AIzaSyDKGnDLdIwKxYZRFCA-6a7J6KvJ3sJLW9o",
//		"COMPOSITE_FRAME_MODE" => "A",
//		"COMPOSITE_FRAME_TYPE" => "AUTO"
//	),
//	false,
//	array(
//		"ACTIVE_COMPONENT" => "Y"
//	)
//);
      ?>
<!--</section>-->
  <section class="contacts online-translation" style="background: url('/upload/medialibrary/728/7287d76738575016f15d1ec5a3ebfa14.jpg') no-repeat 50% 0/cover;" id="online-translation">
    <div class="container-fluid" style="position: relative;">
      <h2 class="h1 title-style1 type2">Место проведени DataVizDay</h2>
      <div class="col1" style="color: #FFF;">
        DoubleTree by Hilton Hotel Minsk ─ современная гостиница, расположена в центре Минска, в шаговой доступности от
        старого города, 4 зала, 3 ресторана, современное оборудование
      </div>
      <div class="contacts-data  contacts-data--custom">
        <div class="contact"><span class="fa fa-map-marker"></span><span class="semibold"><?$APPLICATION->IncludeComponent("bitrix:main.include", "", array(
                "AREA_FILE_SHOW" => "file",
                "PATH" => SITE_DIR . "include/conf/map-address-name.php",
              )
            );?>:</span><?$APPLICATION->IncludeComponent("bitrix:main.include", "", array(
              "AREA_FILE_SHOW" => "file",
              "PATH" => SITE_DIR . "include/conf/map-address.php",
            )
          );?></div>
        <div class="contact"><span class="fa fa-phone"></span><span class="semibold"><?$APPLICATION->IncludeComponent("bitrix:main.include", "", array(
                "AREA_FILE_SHOW" => "file",
                "PATH" => SITE_DIR . "include/conf/map-phone-name.php",
              )
            );?>:</span>
          <?
            $phonePath = $_SERVER['DOCUMENT_ROOT'] . SITE_DIR . "include/conf/map-phone.php";
            $phone = (file_exists($phonePath)) ? str_replace(array("\040", "(", ")"), "", strip_tags(file_get_contents($phonePath))) : '';
          ?>
          <a href="tel:<?=$phone?>"><?$APPLICATION->IncludeComponent("bitrix:main.include", "", array(
                "AREA_FILE_SHOW" => "file",
                "PATH" => SITE_DIR . "include/conf/map-phone.php",
              )
            );?></a>
        </div>
        <div class="contact"><span class="fa fa-envelope"></span><span class="semibold"><?$APPLICATION->IncludeComponent("bitrix:main.include", "", array(
                "AREA_FILE_SHOW" => "file",
                "PATH" => SITE_DIR . "include/conf/map-email-name.php",
              )
            );?>:</span>
          <? $emailPath = $_SERVER['DOCUMENT_ROOT'] . SITE_DIR . "include/conf/map-email.php"; ?>
          <a href="mailto:<? if(file_exists($emailPath)) require $emailPath; ?>"><?$APPLICATION->IncludeComponent("bitrix:main.include", "", array(
                "AREA_FILE_SHOW" => "file",
                "PATH" => SITE_DIR . "include/conf/map-email.php",
              )
            );?></a>
        </div>
      </div>
      <div class="images-block">
        <div class="images-block__col">
          <img src="/upload/contacts/3.jpg" alt="">
        </div>
        <div class="images-block__col">
          <div class="images-block__row">
            <img src="/upload/contacts/2.jpg" alt="">
            <img src="/upload/contacts/5.jpg" alt="">
          </div>
          <img src="/upload/contacts/1.jpg" alt="">
        </div>
      </div>
    </div>
  </section>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>