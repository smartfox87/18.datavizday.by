<?
define('STOP_STATISTICS', true);
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
$GLOBALS['APPLICATION']->RestartBuffer();

if (ctype_xdigit($_REQUEST['theme-color'])) {
	COption::SetOptionString("main", "conf_theme_color", htmlspecialcharsbx($_REQUEST['theme-color']));
}

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_after.php");
?>