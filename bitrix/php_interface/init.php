<?
ini_set('xdebug.var_display_max_depth', '10');

/**
 * Выводит дамп переменной
 * @param $var
 * @param bool $show_dump
 * @param bool $die
 * @param bool $all
 */
function prent($var, $varName = '$var', $show_dump = true, $die = false, $all = false)
{
    global $USER;
    if(($USER->GetID() == 4) || ($all == true))
    {
        echo $varName;
        echo "<pre style=\"text-align:left; background-color:#CCC;color:#000; font-size:12px; padding-bottom: 10px; border-bottom:1px solid #000;\">";
        if($show_dump)
        {
            var_dump($var);
        }
        else
        {
            print_r($var);
        }
        echo "</pre>";

        if($die) {
            die;
        }
    }
}

?>