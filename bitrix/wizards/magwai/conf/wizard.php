
<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
/**
 * Created by: Magwai (Vadim Belyaev)
 * Date: 31.06.2016 15:55
 * http://magwai.ru
 */


require_once($_SERVER['DOCUMENT_ROOT'] . "/bitrix/modules/main/install/wizard_sol/wizard.php");

class SelectSiteStep extends CSelectSiteWizardStep
{
	function InitStep()
	{
		parent::InitStep();
		$wizard =& $this->GetWizard();
		$wizard->solutionName = "magwai_conf";
	}
}

class SelectTemplateStep extends CSelectTemplateWizardStep {
	function ShowStep()
	{
		$wizard =& $this->GetWizard();

		$templatesPath = WizardServices::GetTemplatesPath($wizard->GetPath()."/site");
		$arTemplates = WizardServices::GetTemplates($templatesPath);

		if (empty($arTemplates))
			return;

		$templateID = $wizard->GetVar("templateID");
		if(isset($templateID) && array_key_exists($templateID, $arTemplates)){

			$defaultTemplateID = $templateID;
			$wizard->SetDefaultVar("templateID", $templateID);

		} else {

			$defaultTemplateID = COption::GetOptionString("main", "wizard_template_id", "", $wizard->GetVar("siteID"));
			if (!(strlen($defaultTemplateID) > 0 && array_key_exists($defaultTemplateID, $arTemplates)))
			{
				if (strlen($defaultTemplateID) > 0 && array_key_exists($defaultTemplateID, $arTemplates))
					$wizard->SetDefaultVar("templateID", $defaultTemplateID);
				else
					$defaultTemplateID = "";
			}
		}

		CFile::DisableJSFunction();

		$this->content .= '<div id="solutions-container" class="inst-template-list-block">';
		foreach ($arTemplates as $templateID => $arTemplate)
		{
			if ($defaultTemplateID == "")
			{
				$defaultTemplateID = $templateID;
			}
			$wizard->SetDefaultVar("templateID", $defaultTemplateID);

			$this->content .= '<div class="inst-template-description">';
			$this->content .= $this->ShowRadioField("templateID", $templateID, Array("id" => $templateID, "class" => "inst-template-list-inp"));
			if ($arTemplate["SCREENSHOT"] && $arTemplate["PREVIEW"])
				$this->content .= CFile::Show2Images($arTemplate["PREVIEW"], $arTemplate["SCREENSHOT"], 150, 150, ' class="inst-template-list-img"');
			else
				$this->content .= CFile::ShowImage($arTemplate["SCREENSHOT"], 150, 150, ' class="inst-template-list-img"', "", true);

			$this->content .= '<label for="'.$templateID.'" class="inst-template-list-label">'.$arTemplate["NAME"].'<p>'.$arTemplate["DESCRIPTION"].'</p></label>';
			$this->content .= "</div>";

		}

		$this->content .= '</div>';
	}
}

class SelectThemeStep extends CSelectThemeWizardStep {

	function ShowStep()
	{
		$wizard =& $this->GetWizard();
		$templateID = $wizard->GetVar("templateID");

		$templatesPath = WizardServices::GetTemplatesPath($wizard->GetPath()."/site");
		$arThemes = WizardServices::GetThemes($templatesPath."/".$templateID."/themes");

		/* move to top "green-orange" */
		$default = "green-orange";
		if (array_key_exists($default, $arThemes))
		{
			$temp = array($default => $arThemes[$default]);
			unset($arThemes[$default]);
			$arThemes = $temp + $arThemes;
		}

		if (empty($arThemes))
			return;

		$themeVarName = $templateID."_themeID";
		$ThemeID = $wizard->GetVar($templateID."_themeID");

		if(isset($ThemeID) && array_key_exists($ThemeID, $arThemes)){
			$defaultThemeID = $ThemeID;
			$wizard->SetDefaultVar($themeVarName, $ThemeID);
		} else {
			$defaultThemeID = COption::GetOptionString("main", "wizard_".$templateID."_theme_id", "", $wizard->GetVar("siteID"));

			if (!(strlen($defaultThemeID) > 0 && array_key_exists($defaultThemeID, $arThemes)))
			{
				$defaultThemeID = COption::GetOptionString("main", "wizard_".$templateID."_theme_id", "");
				if (strlen($defaultThemeID) > 0 && array_key_exists($defaultThemeID, $arThemes))
					$wizard->SetDefaultVar($themeVarName, $defaultThemeID);
				else
					$defaultThemeID = "";
			}
		}

		$this->content =
			'<script type="text/javascript">
function SelectTheme(element, solutionId, imageUrl)
{
	var container = document.getElementById("solutions-container");
	var anchors = container.getElementsByTagName("SPAN");
	for (var i = 0; i < anchors.length; i++)
	{
		if (anchors[i].parentNode == container)
			anchors[i].className = "inst-template-color";
	}
	element.className = "inst-template-color inst-template-color-selected";
	var hidden = document.getElementById("selected-solution");
	if (!hidden)
	{
		hidden = document.createElement("INPUT");
		hidden.type = "hidden"
		hidden.id = "selected-solution";
		hidden.name = "selected-solution";
		container.appendChild(hidden);
	}
	hidden.value = solutionId;

	var preview = document.getElementById("solution-preview");
	if (!imageUrl)
		preview.style.display = "none";
	else
	{
		document.getElementById("solution-preview-image").src = imageUrl;
		preview.style.display = "";
	}
}
</script>'.
			'<div id="html_container">'.
			'<div class="inst-template-color-block" id="solutions-container">';
		$ii = 0;
		$arDefaultTheme = array();
		foreach ($arThemes as $themeID => $arTheme)
		{
			if ($defaultThemeID == "")
			{
				$defaultThemeID = $themeID;
				$wizard->SetDefaultVar($themeVarName, $defaultThemeID);
			}
			if ($defaultThemeID == $themeID)
				$arDefaultTheme = $arTheme;
			$ii++;

			$this->content .= '
					<span class="inst-template-color'.($defaultThemeID == $themeID ? " inst-template-color-selected" : "").'" ondblclick="SubmitForm(\'next\');"  onclick="SelectTheme(this, \''.$themeID.'\', \''.$arTheme["SCREENSHOT"].'\');">
						<span class="inst-templ-color-img">'.CFile::ShowImage($arTheme["PREVIEW"], 70, 70, ' border="0" class="solution-image"').'</span>
						<span class="inst-templ-color-name">'.$ii.'. '.$arTheme["NAME"].'</span>
					</span>';
		}

		$this->content .= $this->ShowHiddenField($themeVarName, $defaultThemeID, array("id" => "selected-solution"));
		$this->content .=
			'</div>'.
			'<div id="solution-preview">'.
			'<b class="r3"></b><b class="r1"></b><b class="r1"></b>'.
			'<div class="solution-inner-item">'.
			CFile::ShowImage($arDefaultTheme["SCREENSHOT"], 450, 450, ' border="0" id="solution-preview-image"').
			'</div>'.
			'<b class="r1"></b><b class="r1"></b><b class="r3"></b>'.
			'</div>'.
			'</div>';
	}
}

class SiteSettingsStep extends CSiteSettingsWizardStep {
	function InitStep() {
		$wizard =& $this->GetWizard();
		$wizard->solutionName = "magwai_conf";
		parent::InitStep();
		$templateID = $wizard->GetVar("templateID");
		$themeID = $wizard->GetVar($templateID."_themeID");

//		$siteLogo = $this->GetFileContentImgSrc(
//			WIZARD_SITE_PATH."include/conf/logo.php",
//			"/bitrix/wizards/magwai/conf/site/templates/conf/themes/".$themeID."/lang/" . LANGUAGE_ID . "/logo.png"
//		);
//		if (!file_exists(WIZARD_SITE_PATH."include/conf/logo.png"))
//			$siteLogo = "/bitrix/wizards/magwai/conf/site/templates/conf/themes/".$themeID."/lang/".LANGUAGE_ID."/logo.png";

		$logoPath = $_SERVER['DOCUMENT_ROOT'] . "/bitrix/wizards/magwai/conf/site/public/ru/include/conf/logo.php";
		if (file_exists($logoPath)) {
			$siteLogo = file_get_contents($logoPath);
		}

		$wizard->SetDefaultVars(
			Array(
				"siteLogo" => $siteLogo,
				"siteCompanyTitle" => GetMessage('WIZ_COMPANY_TITLE_DEMO'),
				"siteCompanyPhone" => GetMessage('WIZ_COMPANY_PHONE_DEMO'),
				"siteCompanyEmail" => GetMessage('WIZ_COMPANY_EMAIL_DEMO'),
				"siteCompanyCity" => GetMessage('WIZ_COMPANY_CITY_DEMO'),
				"siteCompanyAddress" => GetMessage('WIZ_COMPANY_ADDRESS_DEMO'),
				"siteCompanyVK" => GetMessage('WIZ_COMPANY_VK_DEMO'),
				"siteCompanyFB" => GetMessage('WIZ_COMPANY_FB_DEMO'),
				"siteCompanyTW" => GetMessage('WIZ_COMPANY_TW_DEMO'),
			)
		);
	}

	function ShowStep()
	{
		$wizard =& $this->GetWizard();

		$siteLogo = $wizard->GetVar("siteLogo", true);

		$this->content .= '<div class="wizard-upload-img-block"><div class="wizard-catalog-title">'.GetMessage("WIZ_COMPANY_LOGO").'</div>';

		$this->content .= '<style type="text/css">
								.logo-default { font-family: "Open Sans", sans-serif; color: #1F2C39; font-size: 25px; font-weight: 700; text-transform: uppercase; cursor: default; user-select: none; -moz-user-select: none; -khtml-user-select: none; -webkit-user-select: none; -o-user-select: none; }
								.logo-default:before { content: ""; display: inline-block; vertical-align: middle; width: 19px; height: 19px; border: 10px solid #D7D7D7; border-radius: 50%; margin: -4px 10px 0 0; background-color: #e87162; }
							</style>';
		$this->content .= $siteLogo;

		$this->content .= "<br /><br />".$this->ShowFileField("siteLogo", Array("show_file_info" => "N", "id" => "site-logo")).'</div>';

		$this->content .= '<div class="wizard-upload-img-block"><div class="wizard-catalog-title">'.GetMessage("WIZ_COMPANY_TITLE").'</div>';
		$this->content .= $this->ShowInputField("text", "siteCompanyTitle", Array("id" => "site-company-title", "class" => "wizard-field")).'</div>';

		$this->content .= '<div class="wizard-upload-img-block"><div class="wizard-catalog-title">'.GetMessage("WIZ_COMPANY_PHONE").'</div>';
		$this->content .= $this->ShowInputField("text", "siteCompanyPhone", Array("id" => "site-company-phone", "class" => "wizard-field")).'</div>';

		$this->content .= '<div class="wizard-upload-img-block"><div class="wizard-catalog-title">'.GetMessage("WIZ_COMPANY_EMAIL").'</div>';
		$this->content .= $this->ShowInputField("text", "siteCompanyEmail", Array("id" => "site-company-email", "class" => "wizard-field")).'</div>';

		$this->content .= '<div class="wizard-upload-img-block"><div class="wizard-catalog-title">'.GetMessage("WIZ_COMPANY_CITY").'</div>';
		$this->content .= $this->ShowInputField("text", "siteCompanyCity", Array("id" => "site-company-city", "class" => "wizard-field")).'</div>';

		$this->content .= '<div class="wizard-upload-img-block"><div class="wizard-catalog-title">'.GetMessage("WIZ_COMPANY_ADDRESS").'</div>';
		$this->content .= $this->ShowInputField("text", "siteCompanyAddress", Array("id" => "site-company-address", "class" => "wizard-field")).'</div>';

		$this->content .= '<div class="wizard-upload-img-block"><div class="wizard-catalog-title">'.GetMessage("WIZ_COMPANY_VK").'</div>';
		$this->content .= $this->ShowInputField("text", "siteCompanyVK", Array("id" => "site-company-vk", "class" => "wizard-field")).'</div>';

		$this->content .= '<div class="wizard-upload-img-block"><div class="wizard-catalog-title">'.GetMessage("WIZ_COMPANY_FB").'</div>';
		$this->content .= $this->ShowInputField("text", "siteCompanyFB", Array("id" => "site-company-fb", "class" => "wizard-field")).'</div>';

		$this->content .= '<div class="wizard-upload-img-block"><div class="wizard-catalog-title">'.GetMessage("WIZ_COMPANY_TW").'</div>';
		$this->content .= $this->ShowInputField("text", "siteCompanyTW", Array("id" => "site-company-tw", "class" => "wizard-field")).'</div>';

		$firstStep = COption::GetOptionString("main", "wizard_first" . substr($wizard->GetID(), 7)  . "_" . $wizard->GetVar("siteID"), false, $wizard->GetVar("siteID"));

		$this->content .= $this->ShowHiddenField("installDemoData","Y");

		$this->content .= '<div class="wizard-upload-img-block"><strong>'.GetMessage('WIZ_OPTION_WARNING').'</strong>
						   <br>'.GetMessage('WIZ_OPTION_WARNING_TEXT').'<br>'.GetMessage('WIZ_OPTION_WARNING_TEXT_2').'
						   </div>';

		$arDemo = array(
			'faq' => GetMessage('WIZ_faq'),
			'news' => GetMessage('WIZ_news'),
			'organizers' => GetMessage('WIZ_organizers'),
			'partners' => GetMessage('WIZ_partners'),
			'programs' => GetMessage('WIZ_programs'),
			'slider' => GetMessage('WIZ_slider'),
			'speakers' => GetMessage('WIZ_speakers'),
			'themes_conf' => GetMessage('WIZ_themes_conf'),
			'tickets' => GetMessage('WIZ_tickets'),
			'tickets_params' => GetMessage('WIZ_tickets_params'),
			'subscribe' => GetMessage('WIZ_subscribe'),
			'buy' => GetMessage('WIZ_buy'),
			'register' => GetMessage('WIZ_register'),
		);

		$this->content .= 	'<div class="wizard-input-form">
								<div class="wizard-input-form-block">
									<h4><label for="siteName">'.GetMessage('WIZ_CONTENT_INSTALL').'</label></h4>
									<div class="wizard-input-form-block-content">';

		foreach($arDemo as $m_id => $m_name)
			$this->content .= 	'<label>'.$this->ShowCheckboxField('demo[]', $m_id).' '.$m_name.'</label><br />';

		$this->content .= 				'<br><a href="" onclick="checkAllModules(); return false;">'.GetMessage('WIZ_CHECK_ALL').'</a> | <a href="" onclick="uncheckAllModules(); return false;">'.GetMessage('WIZ_UNCHECK_ALL').'</a>
									<br><br>
									'.GetMessage('WIZ_DEMO_TEXT').'
									</div>
								</div>
							</div>
							<script type="text/javascript">
								function checkAllModules()
								{
									var inputs = document.getElementsByTagName("input");
									for (var i = 0; i < inputs.length; i++)
									{
										if (inputs[i].type == "checkbox")
										{
											if (!inputs[i].checked)
												inputs[i].checked = true;
										}
									}
								}

								function uncheckAllModules()
								{
									var inputs = document.getElementsByTagName("input");
									for (var i = 0; i < inputs.length; i++)
									{
										if (inputs[i].type == "checkbox")
										{
											if (inputs[i].checked)
												inputs[i].checked = false;
										}
									}
								}
							</script>';

//		$formName = $wizard->GetFormName();
//		$installCaption = $this->GetNextCaption();
//		$nextCaption = GetMessage("NEXT_BUTTON");
	}


	function OnPostForm() {
		$wizard =& $this->GetWizard();
		$res = $this->SaveFile("siteLogo", Array("extensions" => "gif,jpg,jpeg,png", "max_height" => 88, "max_width" => 212, "make_preview" => "Y"));
		COption::SetOptionString("main", "wizard_site_logo", $res, "", $wizard->GetVar("siteID"));
	}
}



class DataInstallStep extends CDataInstallWizardStep {
	function CorrectServices(&$arServices)	{
		$wizard =& $this->GetWizard();
		//if($wizard->GetVar("installDemoData") != "Y"){}
	}
}

class FinishStep extends CFinishWizardStep {
	function CreateNewIndex()
	{
		$wizard =& $this->GetWizard();
		$siteID = WizardServices::GetCurrentSiteID($wizard->GetVar("siteID"));

		define("WIZARD_SITE_ID", $siteID);

		$WIZARD_SITE_ROOT_PATH = $_SERVER["DOCUMENT_ROOT"];

		$rsSites = CSite::GetByID($siteID);
		if ($arSite = $rsSites->Fetch())
		{
			if($arSite["DOC_ROOT"] <> '')
			{
				$WIZARD_SITE_ROOT_PATH = $arSite["DOC_ROOT"];
			}
			define("WIZARD_SITE_DIR", $arSite["DIR"]);
		}
		else
		{
			define("WIZARD_SITE_DIR", "/");
		}

		define("WIZARD_SITE_ROOT_PATH", $WIZARD_SITE_ROOT_PATH);
		define("WIZARD_SITE_PATH", str_replace("//", "/", WIZARD_SITE_ROOT_PATH."/".WIZARD_SITE_DIR."/"));

		//Copy index page
		CopyDirFiles(
			WIZARD_SITE_PATH."/_index.php",
			WIZARD_SITE_PATH."/index.php",
			$rewrite = true,
			$recursive = true,
			$delete_after_copy = true
		);

		$demo = $wizard->GetVar('demo');


		CModule::IncludeModule('iblock');
		$el = new CIBlockElement;
		$bs = new CIBlockSection;
		$iblockType = "magwai_conf";
		foreach ($demo as $v) {
			$iblockCode = "conf_".$v."_".WIZARD_SITE_ID;
			$rsIBlock = CIBlock::GetList(array(), array("CODE" => $iblockCode, "TYPE" => $iblockType));
			if ($arIBlock = $rsIBlock->Fetch()) {
				$arSelect = array('ID');
				$arSort = array();
				$arFilter = array(
					'IBLOCK_ID'=>$arIBlock['ID'],
					'ACTIVE'=>'Y'
				);
				$res = CIBlockElement::GetList($arSort, $arFilter, false, false, $arSelect);
				while($ar_fields = $res->GetNext(true,false)){
					$el->Update($ar_fields['ID'], array('ACTIVE'=>'N'));
				}

				$arFilter = Array(
					'IBLOCK_ID'=> $arIBlock['ID'],
					'ACTIVE'=>'Y'
				);
				$rsSections = CIBlockSection::GetList(Array($by=>$order), $arFilter);
				while($arSection = $rsSections->GetNext())
				{
					$bs->Update($arSection['ID'], array('ACTIVE'=>'N'));
				}
			}
		}
		bx_accelerator_reset();
	}

}