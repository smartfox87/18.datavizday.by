<?
$MESS["wiz_structure_data"] = "Установить демонстрационные данные";
$MESS["wiz_restructure_data"] = "Переустановить демонстрационные данные";

$MESS["WIZ_COMPANY_LOGO"] = "Ваш логотип (рекомендуемый размер 182 X 42)";

$MESS["WIZ_COMPANY_TITLE"] = "Заголовок страницы";
$MESS["WIZ_COMPANY_TITLE_DEMO"] = "Главная страница";

$MESS["WIZ_COMPANY_PHONE"] = "Основной номер телефона:";
$MESS["WIZ_COMPANY_PHONE_DEMO"] = "+7 800 888 33 22";

$MESS["WIZ_COMPANY_EMAIL"] = "Адрес элекстронной почты:";
$MESS["WIZ_COMPANY_EMAIL_DEMO"] = "name@gmail.com";

$MESS["WIZ_COMPANY_CITY"] = "Город места проведения мероприятия:";
$MESS["WIZ_COMPANY_CITY_DEMO"] = "Москва";

$MESS["WIZ_COMPANY_ADDRESS"] = "Адрес места проведения мероприятия (без города):";
$MESS["WIZ_COMPANY_ADDRESS_DEMO"] = "ул. Неизвестная, д. 1";

$MESS["WIZ_COMPANY_VK"] = "Страница в ВКонтакте";
$MESS["WIZ_COMPANY_VK_DEMO"] = "https://vk.com/";
$MESS["WIZ_COMPANY_FB"] = "Страница в Facebook";
$MESS["WIZ_COMPANY_FB_DEMO"] = "https://facebook.com/";
$MESS["WIZ_COMPANY_TW"] = "Страница в Twitter";
$MESS["WIZ_COMPANY_TW_DEMO"] = "https://twitter.com/";

$MESS["WIZ_CONTENT_SETTINGS"] = "Настройка демо данных";
$MESS["WIZ_CONTENT_INSTALL"] = "Отключить демо данные";
$MESS["WIZ_CHECK_ALL"] = "Отметить все";
$MESS["WIZ_UNCHECK_ALL"] = "Снять выделение";

$MESS["WIZ_OPTION_WARNING"] = "Внимание";
$MESS["WIZ_OPTION_WARNING_TEXT"] = "После установки решения, все настройки сайта будут доступны под административной учетной записью в \"Режиме правки\", поэтому на текущем шаге НЕ обязательно указывать все данные в вышеперечисленных полях.";
$MESS["WIZ_OPTION_WARNING_TEXT_2"] = "Настройки оплаты и другие данные, недоступные для изменения через \"Режим правки\", будут располагаться в нижней части страницы сайта.";


$MESS["WIZ_faq"] = "F.A.Q.";
$MESS["WIZ_news"] = "Новости";
$MESS["WIZ_organizers"] = "Организаторы";
$MESS["WIZ_partners"] = "Партнеры";
$MESS["WIZ_programs"] = "Программы";
$MESS["WIZ_slider"] = "Слайдер";
$MESS["WIZ_speakers"] = "Спикеры";
$MESS["WIZ_themes_conf"] = "Темы конференции";
$MESS["WIZ_tickets"] = "Билеты";
$MESS["WIZ_tickets_params"] = "Характеристики билетов";
$MESS["WIZ_subscribe"] = "Подписка на новости";
$MESS["WIZ_buy"] = "Заявки на покупку билетов";
$MESS["WIZ_register"] = "Заявки на регистрацию";

$MESS["WIZ_DEMO_TEXT"] = "Выбранные демо-данные будут деактивированы и перестанут отображаться на сайте.";

?>