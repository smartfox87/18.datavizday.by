<?
$MESS['SETTING_AREAS'] = 'Изменение настроек сайта (видно только администратору)';
$MESS['SETTING_AREAS_AREAS'] = 'Область';
$MESS['SETTING_AREAS_VALUE'] = 'Значение';

$MESS['SETTING_AREAS_logo'] = 'Логотип';
$MESS['SETTING_AREAS_translation-link'] = 'Онлайн трансляция на YouTube';
$MESS['SETTING_AREAS_background-tickets'] = 'Изображение для фона блока "Билеты" (рекомендуемый размер 1024x434 px)';
$MESS['SETTING_AREAS_vk'] = 'Адрес ссылки на vk.com';
$MESS['SETTING_AREAS_fb'] = 'Адрес ссылки facebook.com';
$MESS['SETTING_AREAS_tw'] = 'Адрес ссылки twitter.ru';
$MESS['SETTING_AREAS_success-buy'] = 'Заголовок сообщения после успешной отправки формы "Купить билет"';
$MESS['SETTING_AREAS_success-buy-description'] = 'Описание сообщения после успешной отправки формы "Купить билет"';
$MESS['SETTING_AREAS_paid-success'] = 'Заголовок сообщения после успешной оплаты заказа';
$MESS['SETTING_AREAS_paid-success-description'] = 'Описание сообщения после успешной оплаты заказа';
$MESS['SETTING_AREAS_paid-fail'] = 'Заголовок сообщения после отказа от оплаты заказа';
$MESS['SETTING_AREAS_success-register'] = 'Заголовок сообщения после успешной отправки формы "Регистрация"';
$MESS['SETTING_AREAS_success-register-description'] = 'Описание сообщения после успешной отправки формы "Регистрация"';
$MESS['SETTING_AREAS_success-subscribe'] = 'Заголовок сообщения после успешной отправки формы "Подпишитесь на новости"';
$MESS['SETTING_AREAS_success-subscribe-description'] = 'Описание сообщения после успешной отправки формы "Подпишитесь на новости"';

$MESS['SETTING_AREAS_component-buy'] = 'Настройка оплаты';

$MESS['MAGWAI_FOOTER_COPYRIGHT'] = 'Разработка сайтов';
$MESS['MAGWAI_FOOTER_COPYRIGHT_NAME'] = 'Магвай';
$MESS['MAGWAI_FOOTER_SETTING'] = 'Настройки решения';
$MESS['MAGWAI_FOOTER_SETTING_COLOR'] = 'Базовый цвет';
$MESS['MAGWAI_FOOTER_SETTING_DEFAULT'] = 'По умолчанию';