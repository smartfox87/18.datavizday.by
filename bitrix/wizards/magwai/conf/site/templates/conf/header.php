<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die(); ?>
<?
IncludeTemplateLangFile(__FILE__);
?><!DOCTYPE html>
<!--[if IE 8]><html class="no-js ie8" lang="<?=strtolower(LANGUAGE_ID).'-'.strtoupper(LANGUAGE_ID)?>"><![endif]-->
<!--[if gt IE 8]><!--><html class="no-js" lang="<?=strtolower(LANGUAGE_ID).'-'.strtoupper(LANGUAGE_ID)?>"><!--<![endif]-->
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

	<title><? $APPLICATION->ShowTitle(); ?></title>
	<?
	$APPLICATION->ShowHead();
	$APPLICATION->AddHeadString('<link rel="shortcut icon" href="' . SITE_TEMPLATE_PATH . '/favicon.ico" type="image/x-icon">');
	$APPLICATION->SetAdditionalCSS('https://fonts.googleapis.com/css?family=Open+Sans:400,400italic,600,700,800,700italic&subset=latin,cyrillic-ext,cyrillic,latin-ext');
	$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH . '/css/owl.carousel.min.css');
	$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH . '/css/jquery.arcticmodal-0.3.css');
	$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH . '/css/jquery.jscrollpane.css');
	$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH . '/css/spectrum.css');
	
	if (CModule::IncludeModule("magwai.conf")) {
		$APPLICATION->AddHeadString('<link href="' . SITE_TEMPLATE_PATH . '/css/theme.php" type="text/css"  rel="stylesheet" />');
	} else {
		$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH . '/css/theme-default.css');
	}
	
	CJSCore::Init(array('jquery2', 'ajax'));
	$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . '/js/modernizr.custom.44528.js');
	$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . '/js/jquery.mobile-events.min.js');
	$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . '/js/jquery.ikSelect.min.js');
	$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . '/js/spectrum.js');

	$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . '/js/phone-mask.js');
	$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . '/js/sticky-header.js');
	$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . '/js/jquery.dotdotdot.min.js');
	$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . '/js/owl.carousel.min.js');
	$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . '/js/image-scale.min.js');
	$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . '/js/jquery.arcticmodal-0.3.min.js');
	$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . '/js/jquery.mousewheel.js');
	$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . '/js/jquery.jscrollpane.min.js');

	$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . '/js/main.js');
	?>
</head>
<body>
	<div id="panel"><? $APPLICATION->ShowPanel(); ?></div>
	<div class="wrap-site">
		<header class="header">
			<div class="container-fluid">
				<a href="/" class="logo"><?$APPLICATION->IncludeComponent("bitrix:main.include", "", array(
						"AREA_FILE_SHOW" => "file",
						"PATH" => SITE_DIR . "include/conf/logo.php",
					)
				);?></a>
				<a class="toggle-nav hidden-lg"><span></span><span></span><span></span></a>
				<a href="#registration"
				   class="btn btn-style1 pull-right hidden-xs"><?$APPLICATION->IncludeComponent("bitrix:main.include", "", array(
				   		"AREA_FILE_SHOW" => "file",
				   		"PATH" => SITE_DIR . "include/conf/registration-button.php",
				   	)
				   );?></a>
				<?$APPLICATION->IncludeComponent(
					"bitrix:menu",
					"main",
					array(
						"ALLOW_MULTI_SELECT" => "N",
						"CHILD_MENU_TYPE" => "",
						"DELAY" => "N",
						"MAX_LEVEL" => "1",
						"MENU_CACHE_GET_VARS" => array(
						),
						"MENU_CACHE_TIME" => "3600",
						"MENU_CACHE_TYPE" => "Y",
						"MENU_CACHE_USE_GROUPS" => "Y",
						"ROOT_MENU_TYPE" => "top",
						"USE_EXT" => "N",
						"COMPONENT_TEMPLATE" => "main",
						"CACHE_SELECTED_ITEMS" => "N"
					),
					false
				);?>
			</div>
		</header>
		<div class="content">