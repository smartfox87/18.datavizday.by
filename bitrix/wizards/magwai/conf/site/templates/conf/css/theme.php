<?
define('STOP_STATISTICS', true);
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
$GLOBALS['APPLICATION']->RestartBuffer();

header("Content-type: text/css", true);

if (!CModule::IncludeModule("magwai.conf")) {
	return;
}

$themeColorDafault = 'e87162';

$themeColor = COption::GetOptionString("main", "conf_theme_color");
$themeColor = ($themeColor) ? $themeColor : $themeColorDafault;

$arThemesMap = array(
	'e87162' => array(
		'#e87162',
		'#e14936',
		'#dd3620',
		'#1f2c39',
		'rgba(61, 83, 104, 0.79)',
		'rgba(31, 44, 57, 0.8)',
	),
	'05a4cd' => array(
		'#05a4cd',
		'#047c9b',
		'#036882',
		'#333333',
		'rgba(85, 86, 102, 0.79)',
		'rgba(51, 51, 51, 0.8)',
	),
	'13c08d' => array(
		'#13c08d',
		'#0e926b',
		'#0c7a5a',
		'#0e2a36',
		'rgba(14, 42, 54, 0.79)',
		'rgba(14, 42, 54, 0.8)',
	),
);


if (array_key_exists($themeColor, $arThemesMap)) {
	$arColors = $arThemesMap[$themeColor];
} else {
	$color = new CMagwaiThemeColor($themeColor);
	$arColors = array(
		$color->getColorMain(),
		$color->getTransform(0, 30, -10),
		$color->getTransform(0, 50, -5),
		'#1f2c39',
		'rgba(61, 83, 104, 0.79)',
		'rgba(31, 44, 57, 0.8)',
	);
}

?>
a {
	color: <?=$arColors[0]?>;
}
a:hover, a:focus {
	color: <?=$arColors[1]?>;
}
.btn.btn-style1 {
	background-color: <?=$arColors[0]?>;
}
.btn.btn-style1:hover, .btn.btn-style1:focus {
	background-color: <?=$arColors[1]?>;
}
.btn.btn-style1:active {
	background-color: <?=$arColors[2]?>;
}

.btn.btn-style2 {
	background-color: <?=$arColors[0]?>;
}
.btn.btn-style2:hover, .btn.btn-style2:focus {
	background-color: <?=$arColors[1]?>;
}
.btn.btn-style2:active {
	background-color: <?=$arColors[2]?>;
}
.header .logo .logo-default:before {
	background-color: <?=$arColors[0]?>;
}

.toggle-nav.opened {
	border-color: <?=$arColors[0]?>;
	background-color: <?=$arColors[0]?>;
}

.main-nav a:hover, .main-nav a.active {
	background-color: <?=$arColors[0]?>;
}

.footer {
	background: <?=$arColors[0]?>;
}

.footer .soc-icons a {
	color: <?=$arColors[0]?>;
}

.owl-nav .owl-prev,
.owl-nav .owl-next {
	background-color: <?=$arColors[0]?>;
}

.carousel1 .slide {
	background-color: <?=$arColors[3]?>;
}

.info-sect {
	background: <?=$arColors[3]?>;
}

.info-sect .info-item .icon-i-i {
	border-color: <?=$arColors[0]?>;
	fill: <?=$arColors[0]?>;
	stroke: <?=$arColors[0]?>;
}

.speaker-item hr {
	background-color: <?=$arColors[0]?>;
}
.speaker-item .overlay-s-i a:hover, .speaker-item .overlay-s-i a:focus {
	color: <?=$arColors[0]?>;
}
.speaker-item .title-s-i a:hover, .speaker-item .title-s-i a:focus {
	color: <?=$arColors[0]?>;
}

.tabs1 .ctrl-tabs strong {
	color: <?=$arColors[0]?>;
}
.tabs1 .ctrl-tabs.active {
	background: <?=$arColors[0]?>;
}

.write-item2 .el-w-i2 .fa {
	color: <?=$arColors[0]?>;
}

.scroll-pane .jspVerticalBar .jspDrag {
	background-color: <?=$arColors[0]?>;
}

.tickets:before {
	background: <?=$arColors[4]?>;
}

.color1 {
	color: <?=$arColors[0]?>;
}

.new-item .title-n-i a:hover, .new-item .title-n-i a:focus {
	color: <?=$arColors[0]?>;
}

.question-item .title-q-i:before {
	color: <?=$arColors[0]?>;
}

.registration {
	background: <?=$arColors[3]?>;
}

.req {
	color: <?=$arColors[0]?>;
}

.infoBox .place-lbl {
	background: url(data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0idXRmLTgiPz4NCjwhLS0gR2VuZXJhdG9yOiBBZG9iZSBJbGx1c3RyYXRvciAxNy4wLjEsIFNWRyBFeHBvcnQgUGx1Zy1JbiAuIFNWRyBWZXJzaW9uOiA2LjAwIEJ1aWxkIDApICAtLT4NCjwhRE9DVFlQRSBzdmcgUFVCTElDICItLy9XM0MvL0RURCBTVkcgMS4xLy9FTiIgImh0dHA6Ly93d3cudzMub3JnL0dyYXBoaWNzL1NWRy8xLjEvRFREL3N2ZzExLmR0ZCI+DQo8c3ZnIHZlcnNpb249IjEuMSIgaWQ9ItCh0LvQvtC5XzEiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiIHg9IjBweCIgeT0iMHB4Ig0KCSB3aWR0aD0iMjVweCIgaGVpZ2h0PSIzNi45MDhweCIgdmlld0JveD0iMCAwIDI1IDM2LjkwOCIgc3R5bGU9ImVuYWJsZS1iYWNrZ3JvdW5kOm5ldyAwIDAgMjUgMzYuOTA4OyIgeG1sOnNwYWNlPSJwcmVzZXJ2ZSI+DQo8c3R5bGUgdHlwZT0idGV4dC9jc3MiPg0KCS5zdDB7Y2xpcC1wYXRoOnVybCgjU1ZHSURfMl8pO2ZpbGw6I0ZGRkZGRjt9DQoJLnN0MXtmaWxsOiNGRkZGRkY7fQ0KPC9zdHlsZT4NCjxnPg0KCTxnPg0KCQk8ZGVmcz4NCgkJCTxyZWN0IGlkPSJTVkdJRF8xXyIgeT0iMCIgd2lkdGg9IjI1IiBoZWlnaHQ9IjM2LjkwOCIvPg0KCQk8L2RlZnM+DQoJCTxjbGlwUGF0aCBpZD0iU1ZHSURfMl8iPg0KCQkJPHVzZSB4bGluazpocmVmPSIjU1ZHSURfMV8iICBzdHlsZT0ib3ZlcmZsb3c6dmlzaWJsZTsiLz4NCgkJPC9jbGlwUGF0aD4NCgkJPHBhdGggY2xhc3M9InN0MCIgZD0iTTEyLjUwMywwQzUuNjA5LDAsMCw1LjYwOCwwLDEyLjUwMmMwLDIuNzcsMS42MzEsNi45ODQsNS4xMzMsMTMuMjU5YzIuMzksNC4yODIsNC43NjgsNy44OTksNC44NjksOC4wNTENCgkJCWwxLjcxOSwyLjYwNmMwLjE3MywwLjI2MywwLjQ2NywwLjQyMSwwLjc4MiwwLjQyMWMwLjMxNSwwLDAuNjA5LTAuMTU4LDAuNzgyLTAuNDIxbDEuNzE5LTIuNjA2DQoJCQljMC4wOTktMC4xNSwyLjQ2LTMuNzM2LDQuODY5LTguMDUxYzMuNTAyLTYuMjc1LDUuMTMzLTEwLjQ4OCw1LjEzMy0xMy4yNTlDMjUuMDA1LDUuNjA4LDE5LjM5NywwLDEyLjUwMywwTDEyLjUwMywweg0KCQkJIE0xOC4yMzYsMjQuODQ4Yy0yLjM3NCw0LjI1NS00LjY5OCw3Ljc4NC00Ljc5Niw3LjkzMmwtMC45MzcsMS40MjFsLTAuOTM3LTEuNDJjLTAuMDk4LTAuMTQ5LTIuNDQtMy43MS00Ljc5Ny03LjkzMg0KCQkJYy0zLjI0OC01LjgyMS00Ljg5NS05Ljk3NS00Ljg5NS0xMi4zNDZjMC01Ljg2LDQuNzY4LTEwLjYyOSwxMC42MjktMTAuNjI5YzUuODYxLDAsMTAuNjI5LDQuNzY4LDEwLjYyOSwxMC42MjkNCgkJCUMyMy4xMzIsMTQuODc0LDIxLjQ4NSwxOS4wMjcsMTguMjM2LDI0Ljg0OEwxOC4yMzYsMjQuODQ4eiBNMTguMjM2LDI0Ljg0OCIvPg0KCTwvZz4NCgk8cGF0aCBjbGFzcz0ic3QxIiBkPSJNMTIuNTAzLDUuNDkyYy0zLjgyMiwwLTYuOTMyLDMuMTEtNi45MzIsNi45MzJjMCwzLjgyMiwzLjExLDYuOTMyLDYuOTMyLDYuOTMyDQoJCWMzLjgyMiwwLDYuOTMyLTMuMTEsNi45MzItNi45MzJDMTkuNDM1LDguNjAyLDE2LjMyNSw1LjQ5MiwxMi41MDMsNS40OTJMMTIuNTAzLDUuNDkyeiBNMTIuNTAzLDE3LjQ4Mw0KCQljLTIuNzg5LDAtNS4wNTgtMi4yNjktNS4wNTgtNS4wNThjMC0yLjc4OSwyLjI2OS01LjA1OCw1LjA1OC01LjA1OGMyLjc4OSwwLDUuMDU4LDIuMjY5LDUuMDU4LDUuMDU4DQoJCUMxNy41NjEsMTUuMjE0LDE1LjI5MiwxNy40ODMsMTIuNTAzLDE3LjQ4M0wxMi41MDMsMTcuNDgzeiBNMTIuNTAzLDE3LjQ4MyIvPg0KPC9nPg0KPC9zdmc+DQo=) no-repeat 50% 50% <?=$arColors[0]?>;
}
.infoBox .place-lbl:after {
	background: <?=$arColors[0]?>;
}
.infoBox .place-lbl .title-place {
	background: <?=$arColors[3]?>;
}

.arcticmodal-overlay {
	background: <?=$arColors[5]?> !important;
}

.modal-box .close-modal {
	background-color: <?=$arColors[0]?>;
}
.modal-box .close-modal:hover, .modal-box .close-modal:focus {
	background-color: <?=$arColors[1]?>;
}

.cols2 hr {
	background-color: <?=$arColors[0]?>;
}

.soc-prof a {
	color: <?=$arColors[3]?>;
}
.soc-prof a:hover, .soc-prof a:focus {
	color: <?=$arColors[0]?>;
}

.to-top {
	background: <?=$arColors[0]?>;
}

.style-switcher__header {
	background: <?=$arColors[3]?>;
	color: <?=$arColors[0]?>;
}
.style-switcher__icon {
	background: <?=$arColors[3]?>;
}

.sp-container .sp-choose {
	background: <?=$arColors[0]?> !important;
}
.sp-container .sp-choose:hover, .sp-container .sp-choose:focus {
	background: <?=$arColors[1]?> !important;
}
.sp-container .sp-choose:active {
	background: <?=$arColors[2]?> !important;
}

@media screen and (min-width: 768px) {
	.info-sect .info-item .icon-i-i.active {
		border-color: <?=$arColors[0]?>;
		fill: <?=$arColors[0]?>;
		stroke: <?=$arColors[0]?>;
	}
}
@media (min-width: 1230px) {
	.main-nav a:hover, .main-nav a.active {
		background-color: transparent;
		border-color: <?=$arColors[0]?>;
	}
}
<?
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_after.php");
?>
