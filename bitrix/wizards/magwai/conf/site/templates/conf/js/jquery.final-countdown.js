/*!
 * jQuery Final Countdown
 *
 * @author Pragmatic Mates, http://pragmaticmates.com
 * @license GPL 2
 * @link https://github.com/PragmaticMates/jquery-final-countdown
 */

(function ($) {

	var FinalCountdown = function (element, settings, callback) {

		var self = this;

		self.timer = {};

		/* todo */
		var settings;

		var circleSeconds;
		var circleMinutes;
		var circleHours;
		var circleDays;

		var layerSeconds;
		var layerMinutes;
		var layerHours;
		var layerDays;

		var element;
		var callbackFunction;


		if (typeof callback == 'function') { // make sure the callback is a function
			callbackFunction = callback;
		}

		self.responsive = function() {
			$(window).load(self.updateCircles);
			$(window).on('redraw', function() {
				switched = false;
				self.updateCircles();
			});
			$(window).on('resize', self.updateCircles);
		}

		self.updateCircles = function() {
			layerSeconds.draw();
			layerMinutes.draw();
			layerHours.draw();
			layerDays.draw();
		}

		self.convertToDeg = function(degree) {
			return (Math.PI/180)*degree - (Math.PI/180)*90
		}

		self.dispatchTimer = function() {
			self.timer = {
				total: Math.floor((settings.end - settings.start) / 86400),
				days: Math.floor((settings.end - settings.now ) / 86400),
				hours: 24 - Math.floor(((settings.end - settings.now) % 86400) / 3600),
				minutes: 60 - Math.floor((((settings.end - settings.now) % 86400) % 3600) / 60),
				seconds: 60 - Math.floor((((settings.end - settings.now) % 86400) % 3600) % 60 )
			}
		}

		self.prepareCounters = function() {
			// Seconds
			var seconds_width = $('#' + settings.selectors.canvas_seconds).width()

			var secondsStage = new Kinetic.Stage({
				container: settings.selectors.canvas_seconds,
				width: seconds_width,
				height: seconds_width
			});

			circleSeconds = new Kinetic.Shape({
				drawFunc: function(context) {
					var seconds_width = $('#' + settings.selectors.canvas_seconds).width()
					var radius = seconds_width / 2 - settings.seconds.borderWidth / 2;
					var x = seconds_width / 2;
					var y = seconds_width / 2;

					context.beginPath();
					context.arc(x, y, radius, self.convertToDeg(0), self.convertToDeg(self.timer.seconds * 6));
					context.fillStrokeShape(this);

					$(settings.selectors.value_seconds).html(60 - self.timer.seconds);
				},
				stroke: settings.seconds.borderColor,
				strokeWidth: settings.seconds.borderWidth
			});

			layerSeconds = new Kinetic.Layer();
			layerSeconds.add(circleSeconds);
			secondsStage.add(layerSeconds);

			// Minutes
			var minutes_width = $('#' + settings.selectors.canvas_minutes).width();
			var minutesStage = new Kinetic.Stage({
				container: settings.selectors.canvas_minutes,
				width: minutes_width,
				height: minutes_width
			});

			circleMinutes = new Kinetic.Shape({
				drawFunc: function(context) {
					var minutes_width = $('#' + settings.selectors.canvas_minutes).width();
					var radius = minutes_width / 2 - settings.minutes.borderWidth / 2;
					var x = minutes_width / 2;
					var y = minutes_width / 2;

					context.beginPath();
					context.arc(x, y, radius, self.convertToDeg(0), self.convertToDeg(self.timer.minutes * 6));
					context.fillStrokeShape(this);

					$(settings.selectors.value_minutes).html(60 - self.timer.minutes);

				},
				stroke: settings.minutes.borderColor,
				strokeWidth: settings.minutes.borderWidth
			});

			layerMinutes = new Kinetic.Layer();
			layerMinutes.add(circleMinutes);
			minutesStage.add(layerMinutes);

			// Hours
			var hours_width = $('#' + settings.selectors.canvas_hours).width();
			var hoursStage = new Kinetic.Stage({
				container: settings.selectors.canvas_hours,
				width: hours_width,
				height: hours_width
			});

			circleHours = new Kinetic.Shape({
				drawFunc: function(context) {
					var hours_width = $('#' + settings.selectors.canvas_hours).width();
					var radius = hours_width / 2 - settings.hours.borderWidth/2;
					var x = hours_width / 2;
					var y = hours_width / 2;

					context.beginPath();
					context.arc(x, y, radius, self.convertToDeg(0), self.convertToDeg(self.timer.hours * 360 / 24));
					context.fillStrokeShape(this);

					$(settings.selectors.value_hours).html(24 - self.timer.hours);

				},
				stroke: settings.hours.borderColor,
				strokeWidth: settings.hours.borderWidth
			});

			layerHours = new Kinetic.Layer();
			layerHours.add(circleHours);
			hoursStage.add(layerHours);

			// Days
			var days_width = $('#' + settings.selectors.canvas_days).width();
			var daysStage = new Kinetic.Stage({
				container: settings.selectors.canvas_days,
				width: days_width,
				height: days_width
			});

			circleDays = new Kinetic.Shape({
				drawFunc: function(context) {
					var days_width = $('#' + settings.selectors.canvas_days).width();
					var radius = days_width/2 - settings.days.borderWidth/2;
					var x = days_width / 2;
					var y = days_width / 2;


					context.beginPath();

					if (self.timer.total == 0) {
						context.arc(x, y, radius, self.convertToDeg(0), self.convertToDeg(360));
					} else {
						context.arc(x, y, radius, self.convertToDeg(0), self.convertToDeg((360 / self.timer.total) * (self.timer.total - self.timer.days)));
					}
					context.fillStrokeShape(this);

					$(settings.selectors.value_days).html(self.timer.days);

				},
				stroke: settings.days.borderColor,
				strokeWidth: settings.days.borderWidth
			});

			layerDays = new Kinetic.Layer();
			layerDays.add(circleDays);
			daysStage.add(layerDays);

			self.decTitle();
		}

		self.decTitle = function() {


			$('#' + settings.selectors.canvas_seconds).parent().find(settings.selectors.title).text(self.declOfNum(
				(60 - self.timer.seconds), settings.seconds.title
			));
			$('#' + settings.selectors.canvas_minutes).parent().find(settings.selectors.title).text(self.declOfNum(
				(60 - self.timer.minutes), settings.minutes.title
			));
			$('#' + settings.selectors.canvas_hours).parent().find(settings.selectors.title).text(self.declOfNum(
				(24 - self.timer.hours), settings.hours.title
			));
			$('#' + settings.selectors.canvas_days).parent().find(settings.selectors.title).text(self.declOfNum(
				self.timer.days, settings.days.title
			));
		}

		self.declOfNum = function(number, titles) {
			cases = [2, 0, 1, 1, 1, 2];
			return titles[ (number%100>4 && number%100<20)? 2 : cases[(number%10<5)?number%10:5] ];
		}

		self.startCounters = function() {
			var interval = setInterval( function() {
				if (self.timer.seconds > 59 ) {
					if (60 - self.timer.minutes == 0 && 24 - self.timer.hours == 0 && self.timer.days == 0) {
						clearInterval(interval);
						callbackFunction.call(this); // brings the scope to the callback
						return;
					}

					self.timer.seconds = 1;

					if (self.timer.minutes > 59) {
						self.timer.minutes = 1;
						layerMinutes.draw();
						if (self.timer.hours > 23) {
							self.timer.hours = 1;
							if (self.timer.days > 0) {
								self.timer.days--;
								layerDays.draw();
							}
						} else {
							self.timer.hours++;
						}
						layerHours.draw()
					} else {
						self.timer.minutes++;
					}

					layerMinutes.draw();
				} else {
					self.timer.seconds++;
				}

				layerSeconds.draw();

				self.decTitle();

			}, 1000);
		}
	};


	$.fn.final_countdown = function(options, callback) {
		element = $(this);

		var defaults = $.extend({
			start: '1362139200',
			end: '1388461320',
			now: '1387461319',
			selectors: {
				value_seconds: '.clock-seconds .val',
				canvas_seconds: 'canvas_seconds',
				value_minutes: '.clock-minutes .val',
				canvas_minutes: 'canvas_minutes',
				value_hours: '.clock-hours .val',
				canvas_hours: 'canvas_hours',
				value_days: '.clock-days .val',
				canvas_days: 'canvas_days',
				title: '.type-time'
			},
			seconds: {
				borderColor: '#26b6cb',
				borderWidth: '4',
				title: ['', '', '']
			},
			minutes: {
				borderColor: '#26b6cb',
				borderWidth: '4',
				title: ['', '', '']
			},
			hours: {
				borderColor: '#26b6cb',
				borderWidth: '4',
				title: ['', '', '']
			},
			days: {
				borderColor: '#26b6cb',
				borderWidth: '4',
				title: ['', '', '']
			}
		}, options);

		settings = $.extend({}, defaults, options);

		var finalCountdownStack = [];
		finalCountdownStack.push(new FinalCountdown(element, settings, callback));

		var finalCountdown = finalCountdownStack.pop();
		finalCountdown.responsive();
		finalCountdown.dispatchTimer();
		finalCountdown.prepareCounters();
		finalCountdown.startCounters();
	};

})(jQuery);