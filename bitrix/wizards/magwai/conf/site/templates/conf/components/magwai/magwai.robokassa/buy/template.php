<?
if(!defined("B_PROLOG_INCLUDED")||B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);
?>
<? switch($arResult['ACTION_CODE']):
	   case 'form': ?>

		<? //if($arResult['FORM_DATA']): ?>
			<form action="<?=$arResult['FORM_DATA']['PAYMENT_URL']?>" method="POST">
				<input type="hidden" name="MrchLogin" value="<?=$arResult['FORM_DATA']['PAYMENT_LOGIN']?>">
				<input type="hidden" name="OutSum" value="<?=$arResult['FORM_DATA']['COST']?>">
				<input type="hidden" name="InvId" value="<?=$arResult['FORM_DATA']['ORDER_ID']?>">
				<input type="hidden" name="Desc" value="<?=$arResult['FORM_DATA']['PRODUCT_NAME']?>">
				<input type="hidden" name="SignatureValue" value="<?=$arResult['FORM_DATA']['SIGNATURE']?>">
				<input type="hidden" name="Shp_item" value="<?=$arResult['FORM_DATA']['PRODUCT_ID']?>">
				<input type="hidden" name="IsTest" value="<?=$arResult['FORM_DATA']['IS_TEST']?>" />

				<input type="submit" class="btn btn-style2" value="<?=GetMessage('MAGWAI_ROBOKASSA_T_BUY')?>">
			</form>
		<? //endif; ?>

		<? break; ?>
	<? case 'success': ?>

		<script type="text/javascript">
			$(function(){
				$('.modal-box.arcticmodal-buy-success').arcticmodal();
			});
		</script>
		<!-- Modal -->
		<div style="display: none">
			<div class="modal-box send-msg arcticmodal-buy-success">
				<a href="javascript:void(0)" class="fa fa-close arcticmodal-close close-modal"></a>
				<h1 class="title-s-m"><?=str_replace('#ID#', $arResult['STATUS']['ORDER_ID'], $arParams['~PAID_SUCCESS_MESSAGE'])?></h1>
				<p><?=$arParams['~PAID_SUCCESS_MESSAGE_DESCRIPTION']?></p>
				<a href="javascript:void(0)" class="btn btn-style2 arcticmodal-close">OK</a>
			</div>
		</div>

		<? break; ?>
	<? default: ?>

		<script type="text/javascript">
			$(function(){
				$('.modal-box.arcticmodal-buy-fail').arcticmodal();
			});
		</script>
		<!-- Modal -->
		<div style="display: none">
			<div class="modal-box send-msg arcticmodal-buy-fail">
				<a href="javascript:void(0)" class="fa fa-close arcticmodal-close close-modal"></a>
				<h1 class="title-s-m"><?=$arParams['~PAID_FAIL_MESSAGE']?></h1>
				<a href="javascript:void(0)" class="btn btn-style2 arcticmodal-close">OK</a>
			</div>
		</div>

<? endswitch; ?>

