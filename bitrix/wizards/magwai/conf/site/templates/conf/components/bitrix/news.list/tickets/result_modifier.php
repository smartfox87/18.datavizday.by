<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die(); ?>
<?
$arTicketsParamsID = array();
$arTicketsParams = array();
foreach ($arResult['ITEMS'] as &$arItem) {
	if ($arItem['PROPERTIES']['PARAMS']['VALUE']) {
		$arTicketsParamsID = array_merge($arTicketsParamsID, $arItem['PROPERTIES']['PARAMS']['VALUE']);

//		$isLeft = true;
//		foreach($arItem['PROPERTIES']['PARAMS']['VALUE'] as $value) {
//			if ($isLeft) {
//				$arItem['PROPERTIES']['PARAMS']['VALUE_LEFT'][] = $value;
//			} else {
//				$arItem['PROPERTIES']['PARAMS']['VALUE_RIGHT'][] = $value;
//			}
//		}
//		$isLeft = !$isLeft;
	}
}
$arOrder = array("SORT" => "ASC");
$arFilter = array(
	'IBLOCK_ID' => $arResult['ITEMS'][0]['PROPERTIES']['PARAMS']['LINK_IBLOCK_ID'],
	'ID' => array_unique($arTicketsParamsID),
);
$arSelectFields = array("ID", "ACTIVE", "NAME");
$rsElements = CIBlockElement::GetList($arOrder, $arFilter, FALSE, FALSE, $arSelectFields);
while($arElement = $rsElements->GetNext())
{
	$arTicketsParams[$arElement['ID']] = $arElement;
}
$arResult['TICKETS_PARAMS'] = $arTicketsParams;