<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
$this->setFrameMode(true);
?>
<section class="online-translation sect-type1" id="<?=$arResult['ID']?>">
	<div class="container-fluid">
		<h2 class="h1 title-style1"><?
			$APPLICATION->IncludeFile(SITE_DIR . "include/conf/block-online-translation-name.php", Array(), Array(
				"MODE" => "text",
				"NAME" => GetMessage('MAGWAI_ONLINE_TRANSLATION_NAME'),
			));
			?></h2>
		<div class="col1">
			<?
			$APPLICATION->IncludeFile(SITE_DIR . "include/conf/translation-link.php", Array(), Array(
				"MODE" => "html",
				"NAME" => GetMessage('MAGWAI_ONLINE_TRANSLATION_LINK'),
			));
			?>
		</div>
		<div class="col2">
			<h3 class="h2"><?
				$APPLICATION->IncludeFile(SITE_DIR . "include/conf/block-online-translation-title.php", Array(), Array(
					"MODE" => "html",
					"NAME" => GetMessage('MAGWAI_ONLINE_TRANSLATION_TITLE'),
				));
				?></h3>
			<?
			$APPLICATION->IncludeFile(SITE_DIR . "include/conf/block-online-translation-description.php", Array(), Array(
				"MODE" => "html",
				"NAME" => GetMessage('MAGWAI_ONLINE_TRANSLATION_DESCRIPTION'),
			));
			?>
		</div>
	</div>
</section>