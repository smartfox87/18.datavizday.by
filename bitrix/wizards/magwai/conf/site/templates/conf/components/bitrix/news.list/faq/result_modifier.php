<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die(); ?>
<?
$isLeft = true;
foreach ($arResult['ITEMS'] as $key => $arItem) {
	if ($isLeft) {
		$arResult['ITEMS_LEFT'][] = $arItem;
	} else {
		$arResult['ITEMS_RIGHT'][] = $arItem;
	}
	$isLeft = !$isLeft;
}