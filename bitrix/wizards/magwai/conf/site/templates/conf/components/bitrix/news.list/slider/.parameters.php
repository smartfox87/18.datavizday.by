<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arTemplateParameters["TIMER_END_TEXT"] = Array(
	"NAME" => GetMessage('MAGWAI_SLIDER_PARAMS_TIMER_END_TEXT'),
	"TYPE" => "STRING",
	"DEFAULT" => "",
);