<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<section class="faq sect-type1" id="faq">
	<div class="container-fluid">
		<h2 class="h1 title-style1"><?=$arResult['NAME']?></h2>
		<? if($arResult["ITEMS_LEFT"]): ?>
		<div class="col1">
			<?foreach($arResult["ITEMS_LEFT"] as $arItem):?>
				<?
				$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
				$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
				?>
				<article class="question-item" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
					<h3 class="title-q-i"><?=$arItem['NAME']?></h3>
					<div class="answer-item"><?=$arItem['PREVIEW_TEXT']?></div>
				</article>
			<?endforeach;?>
		</div>
		<? endif; ?>
		<? if($arResult["ITEMS_RIGHT"]): ?>
			<div class="col2">
				<?foreach($arResult["ITEMS_RIGHT"] as $arItem):?>
					<?
					$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
					$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
					?>
					<article class="question-item" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
						<h3 class="title-q-i"><?=$arItem['NAME']?></h3>
						<div class="answer-item"><?=$arItem['PREVIEW_TEXT']?></div>
					</article>
				<?endforeach;?>
			</div>
		<? endif; ?>
	</div>
</section>