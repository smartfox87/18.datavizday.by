<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arTemplateParameters["ROWS"] = Array(
	"NAME" => GetMessage('MAGWAI_PARTNERS_PARAMS_ROWS'),
	"TYPE" => "LIST",
	"VALUES" => array(1, 2, 3),
	"DEFAULT" => "1",
);
