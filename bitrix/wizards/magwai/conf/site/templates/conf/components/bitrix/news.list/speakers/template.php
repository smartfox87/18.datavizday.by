<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<section id="speakers" class="speakers sect-type1">
	<div class="container-fluid">
		<h2 class="h1 title-style1"><?=$arResult['NAME']?></h2>
		<div class="owl-carousel carousel2">
		<?foreach($arResult["ITEMS"] as $arItem):?>
			<?
			$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
			$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
			?>
			<article class="speaker-item" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
					<div class="img-s-i">
						<?if($arParams["DISPLAY_PICTURE"]!="N" && is_array($arItem["PREVIEW_PICTURE"])):?>
							<img src="<?=$arItem["PREVIEW_PICTURE"]['SRC']?>" alt="<?=$arItem['NAME']?>">
						<? else: ?>
							<img src="<?=SITE_TEMPLATE_PATH . '/img/default-profile.png'?>" alt="<?=$arSpeaker['NAME']?>">
						<? endif; ?>
						<div class="overlay-s-i">
							<? if($arItem['PROPERTIES']['VK_LINK']['VALUE']): ?>
								<a href="<?=$arItem['PROPERTIES']['VK_LINK']['VALUE']?>" target="_blank" class="fa fa-vk" title="���������"></a>
							<? endif; ?>
							<? if($arItem['PROPERTIES']['FB_LINK']['VALUE']): ?>
								<a href="<?=$arItem['PROPERTIES']['FB_LINK']['VALUE']?>" target="_blank" class="fa fa-facebook" title="Facebook"></a>
							<? endif; ?>
							<? if($arItem['PROPERTIES']['TW_LINK']['VALUE']): ?>
								<a href="<?=$arItem['PROPERTIES']['TW_LINK']['VALUE']?>" target="_blank" class="fa fa-twitter" title="Twitter"></a>
							<? endif; ?>
							<a href="<?=SITE_DIR?>ajax/speaker.php?ncc=1&ELEMENT_ID=<?=$arItem['ID']?>"
							   class="link-s-i show-modal"></a>
						</div>
					</div>
				<hr>
				<h3 class="title-s-i">
					<a href="<?=SITE_DIR?>ajax/speaker.php?ncc=1&ELEMENT_ID=<?=$arItem['ID']?>"
					   class="show-modal"><?=$arItem['NAME']?></a>
				</h3>
				<? if($arItem['PROPERTIES']['POSITION']['VALUE']): ?>
					<p><?=$arItem['PROPERTIES']['POSITION']['VALUE']?></p>
				<? endif; ?>
			</article>
		<?endforeach;?>
		</div>
	</div>
</section>