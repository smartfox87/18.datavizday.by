<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<section class="sect-type2">
	<div class="container-fluid">
		<div class="owl-carousel carousel8">
			<?foreach($arResult['PROPERTIES']['PICTURES']['PREVIEW_PICTURES'] as $key => $arPicture):?>
				<a href="<?=$arResult['PROPERTIES']['PICTURES']['DETAIL_PICTURES'][$key]['SRC']?>">
					<img src="<?=$arPicture['SRC']?>" alt="">
				</a>
			<?endforeach;?>
		</div>
	</div>
</section>
<div style="display: none">
	<div class="loading">
		<div class="windows8">
			<div class="wBall" id="wBall_1">
				<div class="wInnerBall"></div>
			</div>
			<div class="wBall" id="wBall_2">
				<div class="wInnerBall"></div>
			</div>
			<div class="wBall" id="wBall_3">
				<div class="wInnerBall"></div>
			</div>
			<div class="wBall" id="wBall_4">
				<div class="wInnerBall"></div>
			</div>
			<div class="wBall" id="wBall_5">
				<div class="wInnerBall"></div>
			</div>
		</div>
	</div>
</div>