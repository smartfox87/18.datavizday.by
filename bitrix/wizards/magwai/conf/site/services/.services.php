<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$arServices = Array(
	"main" => Array(
		"NAME" => GetMessage("SERVICE_MAIN_SETTINGS"),
		"STAGES" => Array(
			"files.php",
			"template.php",
			"theme.php",
			"mail.php",
			"composite.php",
		),
	),
	"iblock" => Array(
		"NAME" => GetMessage("SERVICE_IBLOCK"),
		"STAGES" => Array(
			"types.php",
			/* �� ������ ������� �������, �.�. ���� ����������� �� ����������� ������� � ����� '�������� � ...' */
			"faq.php",
			"news.php",
			"organizers.php",
			"partners.php",
			"speakers.php",
			"programs.php",
			"slider.php",
			"themes_conf.php",
			"tickets_params.php",
			"tickets.php",
			"subscribe.php",
			"buy.php",
			"register.php",
		)
	)
);