<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
if(!CModule::IncludeModule("iblock")) return;
$iblockXMLFile = WIZARD_SERVICE_RELATIVE_PATH."/xml/magwai_programs.xml";
$iblockCode = "conf_programs_".WIZARD_SITE_ID;
$iblockType = "magwai_conf";
$rsIBlock = CIBlock::GetList(array(), array("CODE" => $iblockCode, "TYPE" => $iblockType));
$iblockID = false;
if ($arIBlock = $rsIBlock->Fetch()) {
	$iblockID = $arIBlock["ID"];
	CIBlock::Delete($arIBlock["ID"]);
	$iblockID = false;
}

if($iblockID == false) {
	$permissions = Array(
		"1" => "X",
		"2" => "R"
	);
	$dbGroup = CGroup::GetList($by = "", $order = "", Array("STRING_ID" => "content_editor"));
	if($arGroup = $dbGroup -> Fetch()) {
		$permissions[$arGroup["ID"]] = 'W';
	};
	$iblockID = WizardServices::ImportIBlockFromXML(
		$iblockXMLFile,
		"conf_programs",
		$iblockType,
		WIZARD_SITE_ID,
		$permissions
	);
	if ($iblockID < 1) return;
	$iblock = new CIBlock;
	$arFields = array(
		"ACTIVE" => "Y",
		"SORT" => 400,
		"FIELDS" => array(
			'IBLOCK_SECTION' => array(
				'IS_REQUIRED' => 'N',
				'DEFAULT_VALUE' => '',
			),
			'ACTIVE' => array(
				'IS_REQUIRED' => 'Y',
				'DEFAULT_VALUE' => 'Y',
			),
			'ACTIVE_FROM' => array(
				'IS_REQUIRED' => 'N',
				'DEFAULT_VALUE' => '=today',
			),
			'ACTIVE_TO' => array(
				'IS_REQUIRED' => 'N',
				'DEFAULT_VALUE' => '',
			),
			'SORT' => array(
				'IS_REQUIRED' => 'N',
				'DEFAULT_VALUE' => '',
			),
			'NAME' => array(
				'IS_REQUIRED' => 'Y',
				'DEFAULT_VALUE' => '',
			),
			'PREVIEW_PICTURE' => array(
				'IS_REQUIRED' => 'N',
				'DEFAULT_VALUE' => array(
					'FROM_DETAIL' => 'N',
					'SCALE' => 'N',
					'WIDTH' => '',
					'HEIGHT' => '',
					'IGNORE_ERRORS' => 'N',
					'METHOD' => '',
					'COMPRESSION' => '',
				),
			),
			'PREVIEW_TEXT_TYPE' => array(
				'IS_REQUIRED' => 'N',
				'DEFAULT_VALUE' => 'text',
			),
			'PREVIEW_TEXT' => array(
				'IS_REQUIRED' => 'N',
				'DEFAULT_VALUE' => '',
			),
			'DETAIL_PICTURE' => array(
				'IS_REQUIRED' => 'N',
				'DEFAULT_VALUE' => array(
					'SCALE' => 'N',
					'WIDTH' => '',
					'HEIGHT' => '',
					'IGNORE_ERRORS' => 'N',
					'METHOD' => '',
					'COMPRESSION' => '',
				),
			),
			'DETAIL_TEXT_TYPE' => array(
				'IS_REQUIRED' => 'N',
				'DEFAULT_VALUE' => 'text',
			),
			'DETAIL_TEXT' => array(
				'IS_REQUIRED' => 'N',
				'DEFAULT_VALUE' => '',
			),
			'XML_ID' => array(
				'IS_REQUIRED' => 'N',
				'DEFAULT_VALUE' => '',
			),
			'CODE' => array(
				'IS_REQUIRED' => 'N',
				'DEFAULT_VALUE' => '',
			),
			'TAGS' => array(
				'IS_REQUIRED' => 'N',
				'DEFAULT_VALUE' => '',
			),
			'SECTION_CODE' => array(
				'IS_REQUIRED' => 'Y',
				'DEFAULT_VALUE' => array(
					'UNIQUE' => 'Y',
					'TRANSLITERATION' => 'Y',
				),
			),
		),
		"CODE" => $iblockCode,
		"XML_ID" => $iblockCode,
		"NAME" => $iblock->GetArrayByID($iblockID, "NAME"),
	);
	$iblock->Update($iblockID, $arFields);
} else {
	$arSites = array();
	$db_res = CIBlock::GetSite($iblockID);
	while ($res = $db_res->Fetch())
		$arSites[] = $res["LID"];
	if (!in_array(WIZARD_SITE_ID, $arSites)) {
		$arSites[] = WIZARD_SITE_ID;
		$iblock = new CIBlock;
		$iblock->Update($iblockID, array("LID" => $arSites));
	}
}

/* ����������� �������� � ��������� */
$iblockCodeLink = "conf_speakers_".WIZARD_SITE_ID;
$arIblockCodeLink = CIBlock::GetList(
	Array(),
	Array(
		'SITE_ID' => WIZARD_SITE_ID,
		"CODE" => $iblockCodeLink
	),
	true
)->Fetch();
$arPropertyLink = CIBlockProperty::GetList(
	Array("sort"=>"asc", "name"=>"asc"),
	Array("IBLOCK_ID" => $iblockID, 'CODE' => 'SPEAKER')
)->Fetch();

$arFieldsProperty = array(
	"NAME" => GetMessage('MAGWAI_CONF_PROGRAMS_SPEAKER'),
	"ACTIVE" => "Y",
	"SORT" => "500",
	"CODE" => "SPEAKER",
	"PROPERTY_TYPE" => "E",
	"LINK_IBLOCK_ID" => $arIblockCodeLink['ID']
);

$ibp = new CIBlockProperty;
$ibp->Update($arPropertyLink['ID'], $arFieldsProperty);


/* ���������� �������� �������� � ��������� (���� �������)
 * (����� WizardServices::ImportIBlockFromXML �� ������ �����������)
 */
$arSelectFields = array('ID', 'CODE');
$rsElements = CIBlockElement::GetList(array('SORT' => 'ASC'), array("IBLOCK_ID" => $arIblockCodeLink['ID']), FALSE, FALSE, $arSelectFields);
$arPropertyByCode = array();
while($arElement = $rsElements->GetNext())
{
	$arPropertyByCode[$arElement['CODE']] = $arElement['ID'];
}

$arUpdateByCode = array(
	array($arPropertyByCode['mikhail-koklyaev']),
	array($arPropertyByCode['natalya-vodnikova']),
	array($arPropertyByCode['konstantin-ivanov']),
	array($arPropertyByCode['anna-leonova']),
	array($arPropertyByCode['andrey-parfenov']),
	array($arPropertyByCode['anna-leonova']),
	array($arPropertyByCode['konstantin-ivanov']),
	array($arPropertyByCode['andrey-parfenov']),
	array($arPropertyByCode['aleksandr-petrov']),
	array($arPropertyByCode['mikhail-koklyaev']),
	array($arPropertyByCode['andrey-parfenov']),
	array($arPropertyByCode['konstantin-ivanov']),
	array($arPropertyByCode['mikhail-koklyaev']),
	array($arPropertyByCode['natalya-vodnikova']),
	array($arPropertyByCode['andrey-parfenov']),
	array($arPropertyByCode['anna-leonova']),

);

$rsElements = CIBlockElement::GetList(array('ID' => 'ASC'), array("IBLOCK_ID" => $iblockID), FALSE, FALSE, array('ID'));
while($arElement = $rsElements->GetNext())
{
	CIBlockElement::SetPropertyValuesEx(
		$arElement['ID'], false, array('SPEAKER' => current($arUpdateByCode))
	);
	next($arUpdateByCode);
}



CWizardUtil::ReplaceMacros(WIZARD_SITE_PATH."_index.php", array("PROGRAMS_IBLOCK_ID" => $iblockID));
CWizardUtil::ReplaceMacros(WIZARD_SITE_PATH."/ajax/program.php", array("PROGRAMS_IBLOCK_ID" => $iblockID));