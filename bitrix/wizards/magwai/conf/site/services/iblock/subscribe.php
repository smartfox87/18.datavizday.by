<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
if(!CModule::IncludeModule("iblock")) return;
$iblockXMLFile = WIZARD_SERVICE_RELATIVE_PATH."/xml/magwai_subscribe.xml";
$iblockCode = "conf_subscribe_".WIZARD_SITE_ID;
$iblockType = "magwai_conf";
$rsIBlock = CIBlock::GetList(array(), array("CODE" => $iblockCode, "TYPE" => $iblockType));
$iblockID = false;
if ($arIBlock = $rsIBlock->Fetch()) {
	$iblockID = $arIBlock["ID"];
	CIBlock::Delete($arIBlock["ID"]);
	$iblockID = false;
}

if($iblockID == false) {
	$permissions = Array(
		"1" => "X",
		"2" => "R"
	);
	$dbGroup = CGroup::GetList($by = "", $order = "", Array("STRING_ID" => "content_editor"));
	if($arGroup = $dbGroup -> Fetch()) {
		$permissions[$arGroup["ID"]] = 'W';
	};
	$iblockID = WizardServices::ImportIBlockFromXML(
		$iblockXMLFile,
		"conf_subscribe",
		$iblockType,
		WIZARD_SITE_ID,
		$permissions
	);
	if ($iblockID < 1) return;
	$iblock = new CIBlock;
	$arFields = array(
		"ACTIVE" => "Y",
		"SORT" => 1000,
		"FIELDS" => array(
			'IBLOCK_SECTION' => array(
				'IS_REQUIRED' => 'N',
				'DEFAULT_VALUE' => '',
			),
			'ACTIVE' => array(
				'IS_REQUIRED' => 'Y',
				'DEFAULT_VALUE' => 'Y',
			),
			'ACTIVE_FROM' => array(
				'IS_REQUIRED' => 'N',
				'DEFAULT_VALUE' => '=today',
			),
			'ACTIVE_TO' => array(
				'IS_REQUIRED' => 'N',
				'DEFAULT_VALUE' => '',
			),
			'SORT' => array(
				'IS_REQUIRED' => 'N',
				'DEFAULT_VALUE' => '',
			),
			'NAME' => array(
				'IS_REQUIRED' => 'Y',
				'DEFAULT_VALUE' => '',
			),
			'PREVIEW_PICTURE' => array(
				'IS_REQUIRED' => 'N',
				'DEFAULT_VALUE' => array(
					'FROM_DETAIL' => 'N',
					'SCALE' => 'N',
					'WIDTH' => '',
					'HEIGHT' => '',
					'IGNORE_ERRORS' => 'N',
					'METHOD' => '',
					'COMPRESSION' => '',
				),
			),
			'PREVIEW_TEXT_TYPE' => array(
				'IS_REQUIRED' => 'N',
				'DEFAULT_VALUE' => 'text',
			),
			'PREVIEW_TEXT' => array(
				'IS_REQUIRED' => 'N',
				'DEFAULT_VALUE' => '',
			),
			'DETAIL_PICTURE' => array(
				'IS_REQUIRED' => 'N',
				'DEFAULT_VALUE' => array(
					'SCALE' => 'N',
					'WIDTH' => '',
					'HEIGHT' => '',
					'IGNORE_ERRORS' => 'N',
					'METHOD' => '',
					'COMPRESSION' => '',
				),
			),
			'DETAIL_TEXT_TYPE' => array(
				'IS_REQUIRED' => 'N',
				'DEFAULT_VALUE' => 'text',
			),
			'DETAIL_TEXT' => array(
				'IS_REQUIRED' => 'N',
				'DEFAULT_VALUE' => '',
			),
			'XML_ID' => array(
				'IS_REQUIRED' => 'N',
				'DEFAULT_VALUE' => '',
			),
			'CODE' => array(
				'IS_REQUIRED' => 'N',
				'DEFAULT_VALUE' => '',
			),
			'TAGS' => array(
				'IS_REQUIRED' => 'N',
				'DEFAULT_VALUE' => '',
			),
		),
		"CODE" => $iblockCode,
		"XML_ID" => $iblockCode,
		"NAME" => $iblock->GetArrayByID($iblockID, "NAME"),
	);
	$iblock->Update($iblockID, $arFields);
} else {
	$arSites = array();
	$db_res = CIBlock::GetSite($iblockID);
	while ($res = $db_res->Fetch())
		$arSites[] = $res["LID"];
	if (!in_array(WIZARD_SITE_ID, $arSites)) {
		$arSites[] = WIZARD_SITE_ID;
		$iblock = new CIBlock;
		$iblock->Update($iblockID, array("LID" => $arSites));
	}
}


$rsProperties = CIBlock::GetProperties($iblockID);
$arProperties = array();
while($arProperty = $rsProperties->GetNext()) {
	$arProperties[$arProperty['CODE']] = $arProperty;
}
CWizardUtil::ReplaceMacros(
	WIZARD_SITE_PATH."_index.php",
	array(
		"SUBSCRIBE_IBLOCK_ID" => $iblockID,
		"SUBSCRIBE_EMAIL" => $arProperties['EMAIL']['ID'],
	)
);

$templateID = $wizard->GetVar("templateID");
$themeID = $wizard->GetVar($templateID."_themeID");
$bitrixTemplateDir = BX_PERSONAL_ROOT."/templates/".$templateID."_".$themeID;
CWizardUtil::ReplaceMacros($_SERVER['DOCUMENT_ROOT'] . $bitrixTemplateDir . '/footer.php', array("SUBSCRIBE_IBLOCK_ID" => $iblockID));
