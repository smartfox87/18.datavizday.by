<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)
	die();

if (!defined("WIZARD_SITE_ID"))
	return;

if (!defined("WIZARD_SITE_DIR"))
	return;


$isComposite = (defined("SM_VERSION") && version_compare(SM_VERSION, "14.5.0") >= 0 ? true : false);

if(
	!$isComposite
	||
	CHTMLPagesCache::IsCompositeEnabled()
) {
	return;
}

COption::SetOptionString("main", "composite_welcome_screen", "N");
$arHTMLCacheOptions = CHTMLPagesCache::getOptions();
$arHTMLCacheOptions['COMPOSITE'] = "Y";
$arHTMLCacheOptions['ALLOW_HTTPS'] = "";
$arHTMLCacheOptions['NO_PARAMETERS'] = "N";
$domain = CHTMLPagesCache::getHttpHost();
$arHTMLCacheOptions["DOMAINS"] = array($domain => $domain);
CHTMLPagesCache::setEnabled(true, true);
CHTMLPagesCache::setOptions($arHTMLCacheOptions);
bx_accelerator_reset();

?>