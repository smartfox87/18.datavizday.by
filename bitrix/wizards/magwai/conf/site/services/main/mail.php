<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)
	die();

if (!defined("WIZARD_SITE_ID"))
	return;

if (!defined("WIZARD_SITE_DIR"))
	return;


/* ���� ������� */
$eventType = new CEventType;


$eventType->Add(array(
	"LID"           => 'ru',
	"EVENT_NAME"    => 'CONF_SUBSCRIBE',
	"NAME"          => GetMessage('WIZ_MAIL_TYPE_SUBSCRIBE_NAME'),
	"DESCRIPTION"   => GetMessage('WIZ_MAIL_TYPE_SUBSCRIBE_DESCRIPTION'),
));
$eventType->Add(array(
	"LID"           => 'ru',
	"EVENT_NAME"    => 'CONF_REGISTER',
	"NAME"          => GetMessage('WIZ_MAIL_TYPE_REGISTER_NAME'),
	"DESCRIPTION"   => GetMessage('WIZ_MAIL_TYPE_REGISTER_DESCRIPTION'),
));
$eventType->Add(array(
	"LID"           => 'ru',
	"EVENT_NAME"    => 'CONF_BUY',
	"NAME"          => GetMessage('WIZ_MAIL_TYPE_BUY_NAME'),
	"DESCRIPTION"   => GetMessage('WIZ_MAIL_TYPE_BUY_DESCRIPTION'),
));
$eventType->Add(array(
	"LID"           => 'ru',
	"EVENT_NAME"    => 'CONF_PAID',
	"NAME"          => GetMessage('WIZ_MAIL_TYPE_PAID_NAME'),
	"DESCRIPTION"   => GetMessage('WIZ_MAIL_TYPE_PAID_DESCRIPTION'),
));
/* ������� ������� */

$eventMessage = new CEventMessage;


/* ������� ������� ������� �� ���������� ��������� ������� */
$arFilter = Array(
	"TYPE_ID" => array('CONF_SUBSCRIBE','CONF_REGISTER','CONF_BUY','CONF_PAID'),
	"SITE_ID" => WIZARD_SITE_ID,
);
$rsMessages = $eventMessage::GetList($by="site_id", $order="desc", $arFilter);
while($arMessage = $rsMessages->GetNext())
{
	$eventMessage->Delete($arMessage['ID']);
}


$eventMessage->Add(array(
		"ACTIVE" => "Y",
		"EVENT_NAME" => "CONF_SUBSCRIBE",
		"LID" => WIZARD_SITE_ID,
		"EMAIL_FROM" => "#DEFAULT_EMAIL_FROM#",
		"EMAIL_TO" => "#AUTHOR_EMAIL#",
		"SUBJECT" => GetMessage('WIZ_MAIL_TEMPLATE_SUBSCRIBE_NAME'),
		"BODY_TYPE" => "html",
		"MESSAGE" => GetMessage('WIZ_MAIL_TEMPLATE_SUBSCRIBE_DESCRIPTION')
	)
);


$eventMessage->Add(array(
		"ACTIVE" => "Y",
		"EVENT_NAME" => "CONF_REGISTER",
		"LID" => WIZARD_SITE_ID,
		"EMAIL_FROM" => "#DEFAULT_EMAIL_FROM#",
		"EMAIL_TO" => "#AUTHOR_EMAIL#",
		"SUBJECT" => GetMessage('WIZ_MAIL_TEMPLATE_REGISTER_NAME'),
		"BODY_TYPE" => "html",
		"MESSAGE" => GetMessage('WIZ_MAIL_TEMPLATE_REGISTER_DESCRIPTION')
	)
);
$eventMessage->Add(array(
		"ACTIVE" => "Y",
		"EVENT_NAME" => "CONF_REGISTER",
		"LID" => WIZARD_SITE_ID,
		"EMAIL_FROM" => "#DEFAULT_EMAIL_FROM#",
		"EMAIL_TO" => "#DEFAULT_EMAIL_FROM#",
		"SUBJECT" => GetMessage('WIZ_MAIL_TEMPLATE_REGISTER_SYSTEM_NAME'),
		"BODY_TYPE" => "html",
		"MESSAGE" => GetMessage('WIZ_MAIL_TEMPLATE_REGISTER_SYSTEM_DESCRIPTION')
	)
);


$eventMessage->Add(array(
		"ACTIVE" => "Y",
		"EVENT_NAME" => "CONF_BUY",
		"LID" => WIZARD_SITE_ID,
		"EMAIL_FROM" => "#DEFAULT_EMAIL_FROM#",
		"EMAIL_TO" => "#AUTHOR_EMAIL#",
		"SUBJECT" => GetMessage('WIZ_MAIL_TEMPLATE_BUY_NAME'),
		"BODY_TYPE" => "html",
		"MESSAGE" => GetMessage('WIZ_MAIL_TEMPLATE_BUY_DESCRIPTION')
	)
);
$eventMessage->Add(array(
		"ACTIVE" => "Y",
		"EVENT_NAME" => "CONF_BUY",
		"LID" => WIZARD_SITE_ID,
		"EMAIL_FROM" => "#DEFAULT_EMAIL_FROM#",
		"EMAIL_TO" => "#DEFAULT_EMAIL_FROM#",
		"SUBJECT" => GetMessage('WIZ_MAIL_TEMPLATE_BUY_SYSTEM_NAME'),
		"BODY_TYPE" => "html",
		"MESSAGE" => GetMessage('WIZ_MAIL_TEMPLATE_BUY_SYSTEM_DESCRIPTION')
	)
);


$eventMessage->Add(array(
		"ACTIVE" => "Y",
		"EVENT_NAME" => "CONF_PAID",
		"LID" => WIZARD_SITE_ID,
		"EMAIL_FROM" => "#DEFAULT_EMAIL_FROM#",
		"EMAIL_TO" => "#AUTHOR_EMAIL#",
		"SUBJECT" => GetMessage('WIZ_MAIL_TEMPLATE_PAID_NAME'),
		"BODY_TYPE" => "html",
		"MESSAGE" => GetMessage('WIZ_MAIL_TEMPLATE_PAID_DESCRIPTION')
	)
);
$eventMessage->Add(array(
		"ACTIVE" => "Y",
		"EVENT_NAME" => "CONF_PAID",
		"LID" => WIZARD_SITE_ID,
		"EMAIL_FROM" => "#DEFAULT_EMAIL_FROM#",
		"EMAIL_TO" => "#DEFAULT_EMAIL_FROM#",
		"SUBJECT" => GetMessage('WIZ_MAIL_TEMPLATE_PAID_SYSTEM_NAME'),
		"BODY_TYPE" => "html",
		"MESSAGE" => GetMessage('WIZ_MAIL_TEMPLATE_PAID_SYSTEM_DESCRIPTION')
	)
);


?>