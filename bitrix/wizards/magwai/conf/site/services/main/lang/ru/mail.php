<?

$MESS['WIZ_MAIL_TYPE_SUBSCRIBE_NAME'] = 'Отправка сообщений подписавшимся на новости';
$MESS['WIZ_MAIL_TYPE_SUBSCRIBE_DESCRIPTION'] = '#AUTHOR_NAME# - Имя подписчика
#AUTHOR_EMAIL# - Email подписчика
#ACTIVE_FROM# - Дата создания новости
#NAME# - Название новости
#PREVIEW_PICTURE# - Адрес изображения анонса новости
#PREVIEW_TEXT# - Текст анонса новости
#DETAIL_PICTURE# - Адрес детального изображения новости
#DETAIL_TEXT# - Детальный текст новости
#ALL_NEWS# - Ссылка на все новости
#UNSUBSCRIBE# - Ссылка на отписку от рассылки';

$MESS['WIZ_MAIL_TYPE_REGISTER_NAME'] = 'Отправка сообщения через форму регистрации';
$MESS['WIZ_MAIL_TYPE_REGISTER_DESCRIPTION'] = '#AUTHOR_EMAIL# - Email автора сообщения
#TEXT# - Текст сообщения';

$MESS['WIZ_MAIL_TYPE_BUY_NAME'] = 'Отправка сообщения через форму покупки билета';
$MESS['WIZ_MAIL_TYPE_BUY_DESCRIPTION'] = '#AUTHOR_EMAIL# - Email автора сообщения
#TEXT# - Текст сообщения';

$MESS['WIZ_MAIL_TYPE_PAID_NAME'] = 'Отправка сообщения после оплаты билета';
$MESS['WIZ_MAIL_TYPE_PAID_DESCRIPTION'] = '#AUTHOR_EMAIL# - Email клиента, оплатившего заказ
#ORDER_ID# - Номер заказа
#PRODUCT_NAME# - Название товара
#PRODUCT_PRICE# - Цена товара
#TEXT# - Текст сообщения';







$MESS['WIZ_MAIL_TEMPLATE_SUBSCRIBE_NAME'] = '#SITE_NAME#: Новая новость - #NAME#';
$MESS['WIZ_MAIL_TEMPLATE_SUBSCRIBE_DESCRIPTION'] = 'Здравствуйте #AUTHOR_NAME#, на сайте #SITE_NAME# появилась новая новость:
<h3>#NAME#</h3>
<img src="#PREVIEW_PICTURE#" alt="" /><br>
#PREVIEW_TEXT#<br>
<br>
<a href="#ALL_NEWS#" target="_blank">Смотреть все новости</a><br>
---------------------------------<br>
<a href="#UNSUBSCRIBE#" target="_blank">Отписаться от рассылки</a>';





$MESS['WIZ_MAIL_TEMPLATE_REGISTER_NAME'] = 'Вы успешно зарегистрированы на конференцию';
$MESS['WIZ_MAIL_TEMPLATE_REGISTER_DESCRIPTION'] = 'Здравствуйте! <br>
<br>
Спасибо за регистрацию на конференцию!<br>
Мы свяжемся с вами в ближайшее время.<br>
Вопросы вы можете задать по контактам, указанным на нашем сайте #SERVER_NAME#<br>';

$MESS['WIZ_MAIL_TEMPLATE_REGISTER_SYSTEM_NAME'] = '#SITE_NAME#: Сообщение из формы регистрации';
$MESS['WIZ_MAIL_TEMPLATE_REGISTER_SYSTEM_DESCRIPTION'] = 'Информационное сообщение сайта #SITE_NAME#<br>
------------------------------------------<br>
<br>
Вам было отправлено сообщение через форму регистрации<br>
<br>
Данные из формы:<br>
#TEXT#<br>
<br>
Сообщение сгенерировано автоматически.';





$MESS['WIZ_MAIL_TEMPLATE_BUY_NAME'] = 'Вы успешно создали заявку на покупку билета';
$MESS['WIZ_MAIL_TEMPLATE_BUY_DESCRIPTION'] = 'Здравствуйте! <br>
<br>
Вы заказали билет на конференцию!<br>
Мы свяжемся с вами в ближайшее время.<br>
Вопросы вы можете задать по контактам, указанным на нашем сайте #SERVER_NAME#<br>';

$MESS['WIZ_MAIL_TEMPLATE_BUY_SYSTEM_NAME'] = '#SITE_NAME#: Сообщение из формы покупки билета';
$MESS['WIZ_MAIL_TEMPLATE_BUY_SYSTEM_DESCRIPTION'] = 'Информационное сообщение сайта #SITE_NAME#<br>
------------------------------------------<br>
<br>
Вам было отправлено сообщение через форму покупки билета<br>
<br>
Данные из формы:<br>
#TEXT#<br>
<br>
Сообщение сгенерировано автоматически.';





$MESS['WIZ_MAIL_TEMPLATE_PAID_NAME'] = 'Вы успешно произвели оплату билета';
$MESS['WIZ_MAIL_TEMPLATE_PAID_DESCRIPTION'] = 'Здравствуйте! <br>
<br>
Ваш заказ "Билет - #PRODUCT_NAME#" стоимостью #PRODUCT_PRICE# руб. успешно оплачен!<br>
Номер заказа № #ORDER_ID#.<br>
Мы свяжемся с вами в ближайшее время.<br>
Вопросы вы можете задать по контактам, указанным на нашем сайте #SERVER_NAME#<br>';

$MESS['WIZ_MAIL_TEMPLATE_PAID_SYSTEM_NAME'] = '#SITE_NAME#: Сообщение об оплате билета';
$MESS['WIZ_MAIL_TEMPLATE_PAID_SYSTEM_DESCRIPTION'] = 'Информационное сообщение сайта #SITE_NAME#<br>
------------------------------------------<br>
<br>
На сайте приобретен билет через онлайн оплату<br>
<br>
Данные о заказе:<br>
#TEXT#<br>
<br>
Сообщение сгенерировано автоматически.';

?>