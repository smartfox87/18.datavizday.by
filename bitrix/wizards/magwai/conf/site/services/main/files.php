<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)
	die();

if (!defined("WIZARD_SITE_ID"))
	return;

if (!defined("WIZARD_SITE_DIR"))
	return;


if (WIZARD_INSTALL_DEMO_DATA)
{
	$path = str_replace("//", "/", WIZARD_ABSOLUTE_PATH."/site/public/".LANGUAGE_ID."/");

	$handle = @opendir($path);
	if ($handle)
	{
		while ($file = readdir($handle))
		{
			if (in_array($file, array(".", "..")))
				continue;
			CopyDirFiles(
				$path.$file,
				WIZARD_SITE_PATH."/".$file,
				$rewrite = true,
				$recursive = true,
				$delete_after_copy = false
			);
		}
	}
}

function ___writeToAreasFile($fn, $text)
{
	if(file_exists($fn) && !is_writable($abs_path) && defined("BX_FILE_PERMISSIONS"))
		@chmod($abs_path, BX_FILE_PERMISSIONS);

	$fd = @fopen($fn, "wb");
	if(!$fd)
		return false;

	if(false === fwrite($fd, $text))
	{
		fclose($fd);
		return false;
	}

	fclose($fd);

	if(defined("BX_FILE_PERMISSIONS"))
		@chmod($fn, BX_FILE_PERMISSIONS);
}



/* areas */
CheckDirPath(WIZARD_SITE_PATH."include/conf/");

$wizard =& $this->GetWizard();
___writeToAreasFile(WIZARD_SITE_PATH."include/conf/phone.php", $wizard->GetVar("siteCompanyPhone"));
___writeToAreasFile(WIZARD_SITE_PATH."include/conf/map-phone.php", $wizard->GetVar("siteCompanyPhone"));

___writeToAreasFile(WIZARD_SITE_PATH."include/conf/map-email.php", $wizard->GetVar("siteCompanyEmail"));

___writeToAreasFile(WIZARD_SITE_PATH."include/conf/info-address.php", $wizard->GetVar("siteCompanyCity"));
___writeToAreasFile(WIZARD_SITE_PATH."include/conf/info-address-subtitle.php", $wizard->GetVar("siteCompanyAddress"));
___writeToAreasFile(WIZARD_SITE_PATH."include/conf/map-address.php", $wizard->GetVar("siteCompanyCity") . ",\040" . $wizard->GetVar("siteCompanyAddress"));

___writeToAreasFile(WIZARD_SITE_PATH."include/conf/vk.php", $wizard->GetVar("siteCompanyVK"));
___writeToAreasFile(WIZARD_SITE_PATH."include/conf/fb.php", $wizard->GetVar("siteCompanyFB"));
___writeToAreasFile(WIZARD_SITE_PATH."include/conf/tw.php", $wizard->GetVar("siteCompanyTW"));


CWizardUtil::ReplaceMacros(WIZARD_SITE_PATH."_index.php", array("TITLE" => $wizard->GetVar("siteCompanyTitle")));


/* logo */
$siteLogo = $wizard->GetVar("siteLogo");

$separator = (WIZARD_SITE_DIR != '/') ? '/' : '';

if($siteLogo > 0)
{
	$ff = CFile::GetByID($siteLogo);
	if($zr = $ff->Fetch())
	{
		$strOldFile = str_replace("//", "/", WIZARD_SITE_ROOT_PATH."/".(COption::GetOptionString("main", "upload_dir", "upload"))."/".$zr["SUBDIR"]."/".$zr["FILE_NAME"]);
		@copy($strOldFile, WIZARD_SITE_PATH."include/conf/logo.png");
		___writeToAreasFile(WIZARD_SITE_PATH."include/conf/logo.php", '<img src="'.WIZARD_SITE_DIR.$separator.'include/conf/logo.png" alt="" />');
		CFile::Delete($siteLogo);
	}
}
//elseif(!file_exists(WIZARD_SITE_PATH."include/conf/logo.php"))
//{
//	copy(WIZARD_THEME_ABSOLUTE_PATH."/lang/".LANGUAGE_ID."/logo.png", WIZARD_SITE_PATH."include/conf/logo.png");
//	___writeToAreasFile(WIZARD_SITE_PATH."include/conf/logo.php", '<img src="'.WIZARD_SITE_DIR.$separator.'include/conf/logo.png"  />');
//}

?>