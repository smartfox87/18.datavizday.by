<?
$successMessagePath = $_SERVER['DOCUMENT_ROOT'] . SITE_DIR . 'include/conf/paid-success.php';
$successMessage = file_exists($successMessagePath) ? file_get_contents($successMessagePath) : '';
$successMessageDescriptionPath = $_SERVER['DOCUMENT_ROOT'] . SITE_DIR . 'include/conf/paid-success-description.php';
$successMessageDescription = file_exists($successMessageDescriptionPath) ? file_get_contents($successMessageDescriptionPath) : '';
$failMessagePath = $_SERVER['DOCUMENT_ROOT'] . SITE_DIR . 'include/conf/paid-fail.php';
$failMessage = file_exists($failMessagePath) ? file_get_contents($failMessagePath) : '';
?>
<?$APPLICATION->IncludeComponent(
	"magwai:magwai.robokassa", 
	"buy", 
	array(
		"PAYMENT_TEST_MODE" => "Y",
		"PAYMENT_LOGIN" => "vadim_lasso",
		"PAYMENT_PASSWORD" => "MrPH1boU0R0qCC3c1yLj",
		"PAYMENT_PASSWORD_2" => "bwFJ1KO8UXt5chat8WJ5",
		"EVENT_TYPE_NAME" => "CONF_PAID",
		"COMPONENT_TEMPLATE" => "buy",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "36000000",
		"ORDER_ID" => $_REQUEST["buy"]["ORDER_ID"],
		"PRODUCT_ID" => $_REQUEST["buy"]["PRODUCT_ID"],
		"REQUEST_ORDER_ID" => $_REQUEST["InvId"],
		"REQUEST_SIGNATURE" => $_REQUEST["SignatureValue"],

		"IS_SETTING" => $isSetting,
		"PAID_SUCCESS_MESSAGE" => $successMessage,
		"PAID_SUCCESS_MESSAGE_DESCRIPTION" => $successMessageDescription,
		"PAID_FAIL_MESSAGE" => $failMessage,
	),
	false,
	array('HIDE_ICONS' => ($isSetting) ? 'N' : 'Y')
);?>