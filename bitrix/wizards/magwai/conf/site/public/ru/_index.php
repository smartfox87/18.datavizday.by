<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("#TITLE#");
?>
<?$APPLICATION->IncludeComponent(
	"bitrix:news.list", 
	"slider", 
	array(
		"IBLOCK_ID" => "#SLIDER_IBLOCK_ID#",
		"IBLOCK_TYPE" => "magwai_conf",
		"ACTIVE_DATE_FORMAT" => "d.m.Y",
		"ADD_SECTIONS_CHAIN" => "N",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "Y",
		"CACHE_TIME" => "60",
		"CACHE_TYPE" => "A",
		"CHECK_DATES" => "Y",
		"DETAIL_URL" => "",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"DISPLAY_DATE" => "Y",
		"DISPLAY_NAME" => "Y",
		"DISPLAY_PICTURE" => "Y",
		"DISPLAY_PREVIEW_TEXT" => "Y",
		"DISPLAY_TOP_PAGER" => "N",
		"FIELD_CODE" => array(
			0 => "ID",
			1 => "CODE",
			2 => "XML_ID",
			3 => "NAME",
			4 => "SORT",
			5 => "PREVIEW_TEXT",
			6 => "DATE_ACTIVE_FROM",
			7 => "ACTIVE_FROM",
			8 => "DATE_ACTIVE_TO",
			9 => "ACTIVE_TO",
			10 => "",
		),
		"FILTER_NAME" => "",
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
		"INCLUDE_SUBSECTIONS" => "Y",
		"MESSAGE_404" => "",
		"NEWS_COUNT" => "20",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_TEMPLATE" => ".default",
		"PAGER_TITLE" => "",
		"PARENT_SECTION" => "",
		"PARENT_SECTION_CODE" => "",
		"PREVIEW_TRUNCATE_LEN" => "",
		"PROPERTY_CODE" => array(
			0 => "SUBTITLE",
			1 => "VIDEO",
			2 => "TIMER",
			3 => "BUTTON_NAME",
			4 => "SHOW_TYPE",
			5 => "BUTTON_LINK",
			6 => "",
			7 => "",
			8 => "",
		),
		"SET_BROWSER_TITLE" => "N",
		"SET_LAST_MODIFIED" => "N",
		"SET_META_DESCRIPTION" => "N",
		"SET_META_KEYWORDS" => "N",
		"SET_STATUS_404" => "N",
		"SET_TITLE" => "N",
		"SHOW_404" => "N",
		"SORT_BY1" => "SORT",
		"SORT_BY2" => "ID",
		"SORT_ORDER1" => "DESC",
		"SORT_ORDER2" => "DESC",
		"COMPONENT_TEMPLATE" => "slider",
		"TIMER_END_TEXT" => "Время регистрации закончилось",
		"RESIZE_PREVIEW" => ""
	),
	false,
	array(
		"ACTIVE_COMPONENT" => "Y"
	)
);?>
<div class="info-sect">
	<? $svgPath = $_SERVER['DOCUMENT_ROOT'] . SITE_DIR . 'include/conf/svg.php'; ?>
	<? if(file_exists($svgPath)) require $svgPath ?>
	<div class="container-fluid">
		<div class="row">
			<div class="col-xs-6 col-md-3">
				<div class="info-item">
					<div class="icon-i-i active"><svg width="34px" height="34px" viewBox="-1 -1 36 36"><use xlink:href="#icon1"></use></svg></div>
					<div class="overflow-hidden hidden-xs">
						<div class="inline-block"><strong><?$APPLICATION->IncludeComponent("bitrix:main.include", "", array(
										"AREA_FILE_SHOW" => "file",
										"PATH" => SITE_DIR . "include/conf/info-address.php",
									)
								);?></strong><?$APPLICATION->IncludeComponent("bitrix:main.include", "", array(
									"AREA_FILE_SHOW" => "file",
									"PATH" => SITE_DIR . "include/conf/info-address-subtitle.php",
								)
							);?></div>
					</div>
				</div>
			</div>
			<div class="col-xs-6 col-md-3">
				<div class="info-item">
					<div class="icon-i-i"><svg width="31px" height="30px" viewBox="-1 -1 33 32"><use xlink:href="#icon2"></use></svg></div>
					<div class="overflow-hidden hidden-xs">
						<div class="inline-block"><strong><?$APPLICATION->IncludeComponent("bitrix:main.include", "", array(
										"AREA_FILE_SHOW" => "file",
										"PATH" => SITE_DIR . "include/conf/info-date.php",
									)
								);?></strong><?$APPLICATION->IncludeComponent("bitrix:main.include", "", array(
									"AREA_FILE_SHOW" => "file",
									"PATH" => SITE_DIR . "include/conf/info-date-subtitle.php",
								)
							);?></div>
					</div>
				</div>
			</div>
			<div class="clearboth visible-xs visible-sm"></div>
			<div class="col-xs-6 col-md-3">
				<div class="info-item">
					<div class="icon-i-i"><svg width="36px" height="28" viewBox="-1 -1 38 30"><use xlink:href="#icon3"></use></svg></div>
					<div class="overflow-hidden hidden-xs">
						<div class="inline-block"><strong><?$APPLICATION->IncludeComponent("bitrix:main.include", "", array(
										"AREA_FILE_SHOW" => "file",
										"PATH" => SITE_DIR . "include/conf/info-speakers.php",
									)
								);?></strong><?$APPLICATION->IncludeComponent("bitrix:main.include", "", array(
									"AREA_FILE_SHOW" => "file",
									"PATH" => SITE_DIR . "include/conf/info-speakers-subtitle.php",
								)
							);?></div>
					</div>
				</div>
			</div>
			<div class="col-xs-6 col-md-3">
				<div class="info-item">
					<div class="icon-i-i"><svg width="38px" height="24" viewBox="-1 -1 40 26"><use xlink:href="#icon4"></use></svg></div>
					<div class="overflow-hidden hidden-xs">
						<div class="inline-block"><strong><?$APPLICATION->IncludeComponent("bitrix:main.include", "", array(
										"AREA_FILE_SHOW" => "file",
										"PATH" => SITE_DIR . "include/conf/info-invite.php",
									)
								);?></strong><?$APPLICATION->IncludeComponent("bitrix:main.include", "", array(
									"AREA_FILE_SHOW" => "file",
									"PATH" => SITE_DIR . "include/conf/info-invite-subtitle.php",
								)
							);?></div>
					</div>
				</div>
			</div>
		</div>
		<div class="txt-i-i-box visible-xs">
		<span class="txt-i-i active"><strong><?$APPLICATION->IncludeComponent("bitrix:main.include", "", array(
						"AREA_FILE_SHOW" => "file",
						"PATH" => SITE_DIR . "include/conf/info-address.php",
					)
				);?></strong><?$APPLICATION->IncludeComponent("bitrix:main.include", "", array(
					"AREA_FILE_SHOW" => "file",
					"PATH" => SITE_DIR . "include/conf/info-address-subtitle.php",
				)
			);?></span>
		<span class="txt-i-i"><strong><?$APPLICATION->IncludeComponent("bitrix:main.include", "", array(
						"AREA_FILE_SHOW" => "file",
						"PATH" => SITE_DIR . "include/conf/info-date.php",
					)
				);?></strong><?$APPLICATION->IncludeComponent("bitrix:main.include", "", array(
					"AREA_FILE_SHOW" => "file",
					"PATH" => SITE_DIR . "include/conf/info-date-subtitle.php",
				)
			);?></span>
		<span class="txt-i-i"><strong><?$APPLICATION->IncludeComponent("bitrix:main.include", "", array(
						"AREA_FILE_SHOW" => "file",
						"PATH" => SITE_DIR . "include/conf/info-speakers.php",
					)
				);?></strong><?$APPLICATION->IncludeComponent("bitrix:main.include", "", array(
					"AREA_FILE_SHOW" => "file",
					"PATH" => SITE_DIR . "include/conf/info-speakers-subtitle.php",
				)
			);?></span>
		<span class="txt-i-i"><strong><?$APPLICATION->IncludeComponent("bitrix:main.include", "", array(
						"AREA_FILE_SHOW" => "file",
						"PATH" => SITE_DIR . "include/conf/info-invite.php",
					)
				);?></strong><?$APPLICATION->IncludeComponent("bitrix:main.include", "", array(
					"AREA_FILE_SHOW" => "file",
					"PATH" => SITE_DIR . "include/conf/info-invite-subtitle.php",
				)
			);?></span>
		</div>
	</div>
</div>
<?$APPLICATION->IncludeComponent(
	"bitrix:news.list",
	"topics",
	array(
		"IBLOCK_ID" => "#THEMES_CONF_IBLOCK_ID#",
		"IBLOCK_TYPE" => "magwai_conf",
		"ACTIVE_DATE_FORMAT" => "d.m.Y",
		"ADD_SECTIONS_CHAIN" => "N",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "Y",
		"CACHE_TIME" => "36000000",
		"CACHE_TYPE" => "A",
		"CHECK_DATES" => "Y",
		"DETAIL_URL" => "",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"DISPLAY_DATE" => "Y",
		"DISPLAY_NAME" => "Y",
		"DISPLAY_PICTURE" => "Y",
		"DISPLAY_PREVIEW_TEXT" => "Y",
		"DISPLAY_TOP_PAGER" => "N",
		"FIELD_CODE" => array(
			0 => "ID",
			1 => "CODE",
			2 => "XML_ID",
			3 => "NAME",
			4 => "SORT",
			5 => "PREVIEW_TEXT",
			6 => "DATE_ACTIVE_FROM",
			7 => "ACTIVE_FROM",
			8 => "DATE_ACTIVE_TO",
			9 => "ACTIVE_TO",
			10 => "",
		),
		"FILTER_NAME" => "",
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
		"INCLUDE_SUBSECTIONS" => "Y",
		"MESSAGE_404" => "",
		"NEWS_COUNT" => "20",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_TEMPLATE" => ".default",
		"PAGER_TITLE" => "",
		"PARENT_SECTION" => "",
		"PARENT_SECTION_CODE" => "",
		"PREVIEW_TRUNCATE_LEN" => "",
		"PROPERTY_CODE" => array(
			0 => "ICON",
			1 => "",
		),
		"SET_BROWSER_TITLE" => "N",
		"SET_LAST_MODIFIED" => "N",
		"SET_META_DESCRIPTION" => "N",
		"SET_META_KEYWORDS" => "N",
		"SET_STATUS_404" => "N",
		"SET_TITLE" => "N",
		"SHOW_404" => "N",
		"SORT_BY1" => "SORT",
		"SORT_BY2" => "ID",
		"SORT_ORDER1" => "DESC",
		"SORT_ORDER2" => "ASC",
		"COMPONENT_TEMPLATE" => "themes-conf"
	),
	false
);?>
<?$APPLICATION->IncludeComponent(
	"bitrix:news.list",
	"organizers",
	array(
		"IBLOCK_ID" => "#ORGANIZERS_IBLOCK_ID#",
		"IBLOCK_TYPE" => "magwai_conf",
		"ACTIVE_DATE_FORMAT" => "d.m.Y",
		"ADD_SECTIONS_CHAIN" => "N",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "Y",
		"CACHE_TIME" => "36000000",
		"CACHE_TYPE" => "A",
		"CHECK_DATES" => "Y",
		"DETAIL_URL" => "",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"DISPLAY_DATE" => "Y",
		"DISPLAY_NAME" => "Y",
		"DISPLAY_PICTURE" => "Y",
		"DISPLAY_PREVIEW_TEXT" => "Y",
		"DISPLAY_TOP_PAGER" => "N",
		"FIELD_CODE" => array(
			0 => "ID",
			1 => "CODE",
			2 => "XML_ID",
			3 => "NAME",
			4 => "SORT",
			5 => "PREVIEW_PICTURE",
			6 => "",
		),
		"FILTER_NAME" => "",
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
		"INCLUDE_SUBSECTIONS" => "Y",
		"MESSAGE_404" => "",
		"NEWS_COUNT" => "20",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_TEMPLATE" => ".default",
		"PAGER_TITLE" => "",
		"PARENT_SECTION" => "",
		"PARENT_SECTION_CODE" => "",
		"PREVIEW_TRUNCATE_LEN" => "",
		"PROPERTY_CODE" => array(
			0 => "",
			1 => "",
		),
		"SET_BROWSER_TITLE" => "N",
		"SET_LAST_MODIFIED" => "N",
		"SET_META_DESCRIPTION" => "N",
		"SET_META_KEYWORDS" => "N",
		"SET_STATUS_404" => "N",
		"SET_TITLE" => "N",
		"SHOW_404" => "N",
		"SORT_BY1" => "SORT",
		"SORT_BY2" => "ID",
		"SORT_ORDER1" => "ASC",
		"SORT_ORDER2" => "ASC",
		"COMPONENT_TEMPLATE" => "organizers"
	),
	false,
	array(
	"ACTIVE_COMPONENT" => "N"
	)
);?>
<?$APPLICATION->IncludeComponent(
	"magwai:block",
	"online-translation",
	array(
		"ID" => "online-translation",
		"COMPONENT_TEMPLATE" => "online-translation"
	),
	false,
	array(
		"ACTIVE_COMPONENT" => "Y"
	)
);?>
<?$APPLICATION->IncludeComponent(
	"bitrix:news.list",
	"speakers",
	array(
		"IBLOCK_ID" => "#SPEAKERS_IBLOCK_ID#",
		"IBLOCK_TYPE" => "magwai_conf",
		"ACTIVE_DATE_FORMAT" => "d.m.Y",
		"ADD_SECTIONS_CHAIN" => "N",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "Y",
		"CACHE_TIME" => "36000000",
		"CACHE_TYPE" => "A",
		"CHECK_DATES" => "Y",
		"DETAIL_URL" => "",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"DISPLAY_DATE" => "Y",
		"DISPLAY_NAME" => "Y",
		"DISPLAY_PICTURE" => "Y",
		"DISPLAY_PREVIEW_TEXT" => "Y",
		"DISPLAY_TOP_PAGER" => "N",
		"FIELD_CODE" => array(
			0 => "ID",
			1 => "CODE",
			2 => "XML_ID",
			3 => "NAME",
			4 => "SORT",
			5 => "PREVIEW_TEXT",
			6 => "PREVIEW_PICTURE",
			7 => "DATE_ACTIVE_FROM",
			8 => "ACTIVE_FROM",
			9 => "DATE_ACTIVE_TO",
			10 => "ACTIVE_TO",
			11 => "",
		),
		"FILTER_NAME" => "",
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
		"INCLUDE_SUBSECTIONS" => "Y",
		"MESSAGE_404" => "",
		"NEWS_COUNT" => "20",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_TEMPLATE" => ".default",
		"PAGER_TITLE" => "",
		"PARENT_SECTION" => "",
		"PARENT_SECTION_CODE" => "",
		"PREVIEW_TRUNCATE_LEN" => "",
		"PROPERTY_CODE" => array(
			0 => "POSITION",
			1 => "FB_LINK",
			2 => "TW_LINK",
			3 => "VK_LINK",
			4 => "",
		),
		"SET_BROWSER_TITLE" => "N",
		"SET_LAST_MODIFIED" => "N",
		"SET_META_DESCRIPTION" => "N",
		"SET_META_KEYWORDS" => "N",
		"SET_STATUS_404" => "N",
		"SET_TITLE" => "N",
		"SHOW_404" => "N",
		"SORT_BY1" => "SORT",
		"SORT_BY2" => "ID",
		"SORT_ORDER1" => "ASC",
		"SORT_ORDER2" => "ASC",
		"COMPONENT_TEMPLATE" => "speakers"
	),
	false
);?>
<?$APPLICATION->IncludeComponent(
	"bitrix:news.list", 
	"program",
	array(
		"IBLOCK_ID" => "#PROGRAMS_IBLOCK_ID#",
		"IBLOCK_TYPE" => "magwai_conf",
		"ACTIVE_DATE_FORMAT" => "d.m.Y",
		"ADD_SECTIONS_CHAIN" => "N",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "Y",
		"CACHE_TIME" => "36000000",
		"CACHE_TYPE" => "A",
		"CHECK_DATES" => "N",
		"DETAIL_URL" => "",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"DISPLAY_DATE" => "Y",
		"DISPLAY_NAME" => "Y",
		"DISPLAY_PICTURE" => "Y",
		"DISPLAY_PREVIEW_TEXT" => "Y",
		"DISPLAY_TOP_PAGER" => "N",
		"FIELD_CODE" => array(
			0 => "ID",
			1 => "CODE",
			2 => "XML_ID",
			3 => "NAME",
			4 => "SORT",
			5 => "PREVIEW_TEXT",
			6 => "PREVIEW_PICTURE",
			7 => "DATE_ACTIVE_FROM",
			8 => "ACTIVE_FROM",
			9 => "DATE_ACTIVE_TO",
			10 => "ACTIVE_TO",
			11 => "",
		),
		"FILTER_NAME" => "",
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
		"INCLUDE_SUBSECTIONS" => "Y",
		"MESSAGE_404" => "",
		"NEWS_COUNT" => "100",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_TEMPLATE" => ".default",
		"PAGER_TITLE" => "",
		"PARENT_SECTION" => "",
		"PARENT_SECTION_CODE" => "programma-na-2016-god",
		"PREVIEW_TRUNCATE_LEN" => "",
		"PROPERTY_CODE" => array(
			0 => "SPEAKER",
			1 => "",
		),
		"SET_BROWSER_TITLE" => "N",
		"SET_LAST_MODIFIED" => "N",
		"SET_META_DESCRIPTION" => "N",
		"SET_META_KEYWORDS" => "N",
		"SET_STATUS_404" => "N",
		"SET_TITLE" => "N",
		"SHOW_404" => "N",
		"SORT_BY1" => "ACTIVE_FROM",
		"SORT_BY2" => "SORT",
		"SORT_ORDER1" => "ASC",
		"SORT_ORDER2" => "ASC",
		"COMPONENT_TEMPLATE" => "program",
		"DISPLAY_SPEAKER_POSITION" => "N",
		"RESIZE_PREVIEW" => "Y",
		"RESIZE_TYPE_PREVIEW" => "0",
		"WIDTH_PREVIEW" => "103",
		"HEIGHT_PREVIEW" => "103"
	),
	false
);?>
<?$APPLICATION->IncludeComponent(
	"bitrix:news.list",
	"tickets",
	array(
		"IBLOCK_ID" => "#TICKETS_IBLOCK_ID#",
		"IBLOCK_TYPE" => "magwai_conf",
		"ACTIVE_DATE_FORMAT" => "d.m.Y",
		"ADD_SECTIONS_CHAIN" => "N",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "Y",
		"CACHE_TIME" => "36000000",
		"CACHE_TYPE" => "A",
		"CHECK_DATES" => "Y",
		"DETAIL_URL" => "",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"DISPLAY_DATE" => "Y",
		"DISPLAY_NAME" => "Y",
		"DISPLAY_PICTURE" => "Y",
		"DISPLAY_PREVIEW_TEXT" => "Y",
		"DISPLAY_TOP_PAGER" => "N",
		"FIELD_CODE" => array(
			0 => "ID",
			1 => "CODE",
			2 => "XML_ID",
			3 => "NAME",
			4 => "SORT",
			5 => "PREVIEW_TEXT",
			6 => "DATE_ACTIVE_FROM",
			7 => "ACTIVE_FROM",
			8 => "DATE_ACTIVE_TO",
			9 => "ACTIVE_TO",
			10 => "",
		),
		"FILTER_NAME" => "",
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
		"INCLUDE_SUBSECTIONS" => "Y",
		"MESSAGE_404" => "",
		"NEWS_COUNT" => "20",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_TEMPLATE" => ".default",
		"PAGER_TITLE" => "",
		"PARENT_SECTION" => "",
		"PARENT_SECTION_CODE" => "",
		"PREVIEW_TRUNCATE_LEN" => "",
		"PROPERTY_CODE" => array(
			0 => "PRICE",
			1 => "IS_NEW",
			2 => "DISCOUNT",
			3 => "",
		),
		"SET_BROWSER_TITLE" => "N",
		"SET_LAST_MODIFIED" => "N",
		"SET_META_DESCRIPTION" => "N",
		"SET_META_KEYWORDS" => "N",
		"SET_STATUS_404" => "N",
		"SET_TITLE" => "N",
		"SHOW_404" => "N",
		"SORT_BY1" => "SORT",
		"SORT_BY2" => "ID",
		"SORT_ORDER1" => "ASC",
		"SORT_ORDER2" => "ASC",
		"COMPONENT_TEMPLATE" => "tickets"
	),
	false
);?>
<?$APPLICATION->IncludeComponent(
	"magwai:block",
	"press-center",
	array(
		"ID" => "press-center",
		"COMPONENT_TEMPLATE" => "press-center"
	),
	false,
	array(
		"ACTIVE_COMPONENT" => "N"
	)
);?>
<?$APPLICATION->IncludeComponent(
	"bitrix:news.list",
	"news",
	array(
		"IBLOCK_ID" => "#NEWS_IBLOCK_ID#",
		"IBLOCK_TYPE" => "magwai_conf",
		"ACTIVE_DATE_FORMAT" => "d.m.Y",
		"ADD_SECTIONS_CHAIN" => "N",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "Y",
		"CACHE_TIME" => "36000000",
		"CACHE_TYPE" => "A",
		"CHECK_DATES" => "Y",
		"DETAIL_URL" => "",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"DISPLAY_DATE" => "Y",
		"DISPLAY_NAME" => "Y",
		"DISPLAY_PICTURE" => "Y",
		"DISPLAY_PREVIEW_TEXT" => "Y",
		"DISPLAY_TOP_PAGER" => "N",
		"FIELD_CODE" => array(
			0 => "ID",
			1 => "CODE",
			2 => "XML_ID",
			3 => "NAME",
			4 => "SORT",
			5 => "PREVIEW_TEXT",
			6 => "PREVIEW_PICTURE",
			7 => "DATE_ACTIVE_FROM",
			8 => "ACTIVE_FROM",
			9 => "DATE_ACTIVE_TO",
			10 => "ACTIVE_TO",
			11 => "",
		),
		"FILTER_NAME" => "",
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
		"INCLUDE_SUBSECTIONS" => "Y",
		"MESSAGE_404" => "",
		"NEWS_COUNT" => "20",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_TEMPLATE" => ".default",
		"PAGER_TITLE" => "",
		"PARENT_SECTION" => "",
		"PARENT_SECTION_CODE" => "",
		"PREVIEW_TRUNCATE_LEN" => "",
		"PROPERTY_CODE" => array(
			0 => "",
		),
		"SET_BROWSER_TITLE" => "N",
		"SET_LAST_MODIFIED" => "N",
		"SET_META_DESCRIPTION" => "N",
		"SET_META_KEYWORDS" => "N",
		"SET_STATUS_404" => "N",
		"SET_TITLE" => "N",
		"SHOW_404" => "N",
		"SORT_BY1" => "SORT",
		"SORT_BY2" => "ID",
		"SORT_ORDER1" => "ASC",
		"SORT_ORDER2" => "DESC",
		"COMPONENT_TEMPLATE" => "news"
	),
	false
);?>
<?
$successMessagePath = $_SERVER['DOCUMENT_ROOT'] . SITE_DIR . 'include/conf/success-subscribe.php';
$successMessage = file_exists($successMessagePath) ? file_get_contents($successMessagePath) : '';
$successMessageDescriptionPath = $_SERVER['DOCUMENT_ROOT'] . SITE_DIR . 'include/conf/success-subscribe-description.php';
$successMessageDescription = file_exists($successMessageDescriptionPath) ? file_get_contents($successMessageDescriptionPath) : '';
?>
<?$APPLICATION->IncludeComponent(
	"magwai:iblock.element.add.form",
	"subscribe",
	array(
		"IBLOCK_ID" => "#SUBSCRIBE_IBLOCK_ID#",
		"IBLOCK_TYPE" => "magwai_conf",
		"AJAX_MODE" => "Y",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_SHADOW" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"COMPONENT_TEMPLATE" => "subscribe",
		"CUSTOM_TITLE_DATE_ACTIVE_FROM" => "",
		"CUSTOM_TITLE_DATE_ACTIVE_TO" => "",
		"CUSTOM_TITLE_DETAIL_PICTURE" => "",
		"CUSTOM_TITLE_DETAIL_TEXT" => "",
		"CUSTOM_TITLE_IBLOCK_SECTION" => "",
		"CUSTOM_TITLE_NAME" => "Ваше имя",
		"CUSTOM_TITLE_PREVIEW_PICTURE" => "",
		"CUSTOM_TITLE_PREVIEW_TEXT" => "",
		"CUSTOM_TITLE_TAGS" => "",
		"DEFAULT_INPUT_SIZE" => "30",
		"DETAIL_TEXT_USE_HTML_EDITOR" => "N",
		"ELEMENT_ASSOC" => "CREATED_BY",
		"GROUPS" => array(),
		"LEVEL_LAST" => "Y",
		"LIST_URL" => "",
		"MAX_FILE_SIZE" => "0",
		"MAX_LEVELS" => "10",
		"MAX_USER_ENTRIES" => "10",
		"PREVIEW_TEXT_USE_HTML_EDITOR" => "N",
		"PROPERTY_CODES" => array(
			0 => "#SUBSCRIBE_EMAIL#",
			1 => "NAME",
		),
		"PROPERTY_CODES_REQUIRED" => array(
			0 => "#SUBSCRIBE_EMAIL#",
			1 => "NAME",
		),
		"RESIZE_IMAGES" => "Y",
		"SEF_MODE" => "N",
		"STATUS" => "ANY",
		"STATUS_NEW" => "N",
		"USER_MESSAGE_ADD" => "",
		"USER_MESSAGE_EDIT" => "",
		"USE_CAPTCHA" => "N",
		"CHECK_UNIQUE_EMAIL" => "Y",
		"SUCCESS_MESSAGE" => $successMessage,
		"SUCCESS_MESSAGE_DESCRIPTION" => $successMessageDescription,
	),
	false
);?>
<?$APPLICATION->IncludeComponent(
	"bitrix:news.list", 
	"partners", 
	array(
		"IBLOCK_ID" => "#PARTNERS_IBLOCK_ID#",
		"IBLOCK_TYPE" => "magwai_conf",
		"ACTIVE_DATE_FORMAT" => "d.m.Y",
		"ADD_SECTIONS_CHAIN" => "N",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "Y",
		"CACHE_TIME" => "36000000",
		"CACHE_TYPE" => "A",
		"CHECK_DATES" => "Y",
		"DETAIL_URL" => "",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"DISPLAY_DATE" => "Y",
		"DISPLAY_NAME" => "Y",
		"DISPLAY_PICTURE" => "Y",
		"DISPLAY_PREVIEW_TEXT" => "Y",
		"DISPLAY_TOP_PAGER" => "N",
		"FIELD_CODE" => array(
			0 => "ID",
			1 => "CODE",
			2 => "XML_ID",
			3 => "NAME",
			4 => "SORT",
			5 => "PREVIEW_TEXT",
			6 => "DATE_ACTIVE_FROM",
			7 => "ACTIVE_FROM",
			8 => "DATE_ACTIVE_TO",
			9 => "ACTIVE_TO",
			10 => "",
		),
		"FILTER_NAME" => "",
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
		"INCLUDE_SUBSECTIONS" => "Y",
		"MESSAGE_404" => "",
		"NEWS_COUNT" => "20",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_TEMPLATE" => ".default",
		"PAGER_TITLE" => "",
		"PARENT_SECTION" => "",
		"PARENT_SECTION_CODE" => "",
		"PREVIEW_TRUNCATE_LEN" => "",
		"PROPERTY_CODE" => array(
			0 => "LINK",
			1 => "",
		),
		"SET_BROWSER_TITLE" => "N",
		"SET_LAST_MODIFIED" => "N",
		"SET_META_DESCRIPTION" => "N",
		"SET_META_KEYWORDS" => "N",
		"SET_STATUS_404" => "N",
		"SET_TITLE" => "N",
		"SHOW_404" => "N",
		"SORT_BY1" => "SORT",
		"SORT_BY2" => "ID",
		"SORT_ORDER1" => "ASC",
		"SORT_ORDER2" => "ASC",
		"COMPONENT_TEMPLATE" => "partners",
		"ROWS" => "0"
	),
	false
);?>
<?$APPLICATION->IncludeComponent(
	"bitrix:news.list",
	"faq",
	array(
		"IBLOCK_ID" => "#FAQ_IBLOCK_ID#",
		"IBLOCK_TYPE" => "magwai_conf",
		"ACTIVE_DATE_FORMAT" => "d.m.Y",
		"ADD_SECTIONS_CHAIN" => "N",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "Y",
		"CACHE_TIME" => "36000000",
		"CACHE_TYPE" => "A",
		"CHECK_DATES" => "Y",
		"DETAIL_URL" => "",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"DISPLAY_DATE" => "Y",
		"DISPLAY_NAME" => "Y",
		"DISPLAY_PICTURE" => "Y",
		"DISPLAY_PREVIEW_TEXT" => "Y",
		"DISPLAY_TOP_PAGER" => "N",
		"FIELD_CODE" => array(
			0 => "",
			1 => "ID",
			2 => "CODE",
			3 => "XML_ID",
			4 => "NAME",
			5 => "SORT",
			6 => "PREVIEW_TEXT",
			7 => "DATE_ACTIVE_FROM",
			8 => "ACTIVE_FROM",
			9 => "DATE_ACTIVE_TO",
			10 => "ACTIVE_TO",
			11 => "",
		),
		"FILTER_NAME" => "",
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
		"INCLUDE_SUBSECTIONS" => "Y",
		"MESSAGE_404" => "",
		"NEWS_COUNT" => "20",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_TEMPLATE" => ".default",
		"PAGER_TITLE" => "",
		"PARENT_SECTION" => "",
		"PARENT_SECTION_CODE" => "",
		"PREVIEW_TRUNCATE_LEN" => "",
		"PROPERTY_CODE" => array(
			0 => "",
		),
		"SET_BROWSER_TITLE" => "N",
		"SET_LAST_MODIFIED" => "N",
		"SET_META_DESCRIPTION" => "N",
		"SET_META_KEYWORDS" => "N",
		"SET_STATUS_404" => "N",
		"SET_TITLE" => "N",
		"SHOW_404" => "N",
		"SORT_BY1" => "SORT",
		"SORT_BY2" => "ID",
		"SORT_ORDER1" => "DESC",
		"SORT_ORDER2" => "ASC",
		"COMPONENT_TEMPLATE" => "faq"
	),
	false,
	array(
	"ACTIVE_COMPONENT" => "Y"
	)
);?>
<section class="registration sect-type1" id="registration">
	<div class="container-fluid">
		<h2 class="h1 title-style1"><?$APPLICATION->IncludeComponent("bitrix:main.include", "", array(
					"AREA_FILE_SHOW" => "file",
					"PATH" => SITE_DIR . "include/conf/registration.php",
				)
			);?></h2>
		<?
		$successMessagePath = $_SERVER['DOCUMENT_ROOT'] . SITE_DIR . 'include/conf/success-register.php';
		$successMessage = file_exists($successMessagePath) ? file_get_contents($successMessagePath) : '';
		$successMessageDescriptionPath = $_SERVER['DOCUMENT_ROOT'] . SITE_DIR . 'include/conf/success-register-description.php';
		$successMessageDescription = file_exists($successMessageDescriptionPath) ? file_get_contents($successMessageDescriptionPath) : '';
		?>
		<?$APPLICATION->IncludeComponent(
			"magwai:iblock.element.add.form",
			"register",
			array(
				"IBLOCK_ID" => "#REGISTER_IBLOCK_ID#",
				"IBLOCK_TYPE" => "magwai_conf",
				"AJAX_MODE" => "Y",
				"AJAX_OPTION_HISTORY" => "N",
				"AJAX_OPTION_JUMP" => "N",
				"AJAX_OPTION_SHADOW" => "N",
				"AJAX_OPTION_STYLE" => "Y",
				"CUSTOM_TITLE_DATE_ACTIVE_FROM" => "",
				"CUSTOM_TITLE_DATE_ACTIVE_TO" => "",
				"CUSTOM_TITLE_DETAIL_PICTURE" => "",
				"CUSTOM_TITLE_DETAIL_TEXT" => "",
				"CUSTOM_TITLE_IBLOCK_SECTION" => "",
				"CUSTOM_TITLE_NAME" => "Ваше имя",
				"CUSTOM_TITLE_PREVIEW_PICTURE" => "",
				"CUSTOM_TITLE_PREVIEW_TEXT" => "",
				"CUSTOM_TITLE_TAGS" => "",
				"DEFAULT_INPUT_SIZE" => "30",
				"DETAIL_TEXT_USE_HTML_EDITOR" => "N",
				"ELEMENT_ASSOC" => "CREATED_BY",
				"EVENT_NAME" => "CONF_REGISTER",
				"GROUPS" => array(),
				"LEVEL_LAST" => "Y",
				"LIST_URL" => "",
				"MAX_FILE_SIZE" => "0",
				"MAX_LEVELS" => "10",
				"MAX_USER_ENTRIES" => "10",
				"PREVIEW_TEXT_USE_HTML_EDITOR" => "N",
				"PROPERTY_CODES" => array(
					0 => "#REGISTER_EMAIL#",
					1 => "#REGISTER_COMPANY#",
					2 => "#REGISTER_POSITION#",
					3 => "#REGISTER_PHONE#",
					4 => "#REGISTER_TICKET#",
					5 => "#REGISTER_MESSAGE#",
					6 => "#REGISTER_REFERER#",
					7 => "NAME",
				),
				"PROPERTY_CODES_REQUIRED" => array(
					0 => "#REGISTER_EMAIL#",
					1 => "#REGISTER_PHONE#",
					2 => "#REGISTER_TICKET#",
					3 => "NAME",
				),
				"RESIZE_IMAGES" => "Y",
				"SEF_MODE" => "N",
				"STATUS" => "ANY",
				"STATUS_NEW" => "NEW",
				"USER_MESSAGE_ADD" => "",
				"USER_MESSAGE_EDIT" => "",
				"USE_CAPTCHA" => "N",
				"COMPONENT_TEMPLATE" => "register",
				"CHECK_UNIQUE_EMAIL" => "Y",
				"SUCCESS_MESSAGE" => $successMessage,
				"SUCCESS_MESSAGE_DESCRIPTION" => $successMessageDescription
			),
			false
		);?>
	</div>
</section>
<section class="contacts" id="contacts">
	<?$APPLICATION->IncludeComponent(
		"magwai:map.google.view",
		"map",
		array(
			"CONTROLS" => array(
				0 => "SMALL_ZOOM_CONTROL",
			),
			"INIT_MAP_TYPE" => "ROADMAP",
			"MAP_DATA" => "a:4:{s:10:\"google_lat\";d:55.756516672063;s:10:\"google_lon\";d:37.615345134735;s:12:\"google_scale\";i:14;s:10:\"PLACEMARKS\";a:1:{i:0;a:3:{s:4:\"TEXT\";s:32:\"Москва###RN###ул. Неизвестная, 1\";s:3:\"LON\";d:37.615299224854;s:3:\"LAT\";d:55.756051582436;}}}",
			"MAP_HEIGHT" => "100%",
			"MAP_ID" => "",
			"MAP_WIDTH" => "100%",
			"OPTIONS" => array(
				0 => "ENABLE_DBLCLICK_ZOOM",
				1 => "ENABLE_DRAGGING",
				2 => "ENABLE_KEYBOARD",
			),
			"COMPONENT_TEMPLATE" => "map",
			"API_KEY" => "AIzaSyDKGnDLdIwKxYZRFCA-6a7J6KvJ3sJLW9o"
		),
		false,
		array(
			"ACTIVE_COMPONENT" => "Y"
		)
	);?>
	<div class="contacts-data">
		<div class="contact"><span class="fa fa-map-marker"></span><span class="semibold"><?$APPLICATION->IncludeComponent("bitrix:main.include", "", array(
						"AREA_FILE_SHOW" => "file",
						"PATH" => SITE_DIR . "include/conf/map-address-name.php",
					)
				);?>:</span><?$APPLICATION->IncludeComponent("bitrix:main.include", "", array(
					"AREA_FILE_SHOW" => "file",
					"PATH" => SITE_DIR . "include/conf/map-address.php",
				)
			);?></div>
		<div class="contact"><span class="fa fa-phone"></span><span class="semibold"><?$APPLICATION->IncludeComponent("bitrix:main.include", "", array(
						"AREA_FILE_SHOW" => "file",
						"PATH" => SITE_DIR . "include/conf/map-phone-name.php",
					)
				);?>:</span>
			<?
			$phonePath = $_SERVER['DOCUMENT_ROOT'] . SITE_DIR . "include/conf/map-phone.php";
			$phone = (file_exists($phonePath)) ? str_replace(array("\040", "(", ")"), "", strip_tags(file_get_contents($phonePath))) : '';
			?>
			<a href="tel:<?=$phone?>"><?$APPLICATION->IncludeComponent("bitrix:main.include", "", array(
						"AREA_FILE_SHOW" => "file",
						"PATH" => SITE_DIR . "include/conf/map-phone.php",
					)
				);?></a>
		</div>
		<div class="contact"><span class="fa fa-envelope"></span><span class="semibold"><?$APPLICATION->IncludeComponent("bitrix:main.include", "", array(
						"AREA_FILE_SHOW" => "file",
						"PATH" => SITE_DIR . "include/conf/map-email-name.php",
					)
				);?>:</span>
			<? $emailPath = $_SERVER['DOCUMENT_ROOT'] . SITE_DIR . "include/conf/map-email.php"; ?>
			<a href="mailto:<? if(file_exists($emailPath)) require $emailPath; ?>"><?$APPLICATION->IncludeComponent("bitrix:main.include", "", array(
						"AREA_FILE_SHOW" => "file",
						"PATH" => SITE_DIR . "include/conf/map-email.php",
					)
				);?></a>
		</div>
	</div>
</section>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>