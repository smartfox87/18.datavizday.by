<?
define('STOP_STATISTICS', true);
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
$GLOBALS['APPLICATION']->RestartBuffer();

$successMessagePath = $_SERVER['DOCUMENT_ROOT'] . SITE_DIR . 'include/conf/success-buy.php';
$successMessage = file_exists($successMessagePath) ? file_get_contents($successMessagePath) : '';
$successMessageDescriptionPath = $_SERVER['DOCUMENT_ROOT'] . SITE_DIR . 'include/conf/success-buy-description.php';
$successMessageDescription = file_exists($successMessageDescriptionPath) ? file_get_contents($successMessageDescriptionPath) : '';
?>
<div class="modal-box">
	<a href="javascript:void(0)" class="fa fa-close arcticmodal-close close-modal"></a>
	<?$APPLICATION->IncludeComponent(
		"magwai:iblock.element.add.form",
		"buy",
		array(
			"IBLOCK_ID" => "#BUY_IBLOCK_ID#",
			"IBLOCK_TYPE" => "magwai_conf",
			"AJAX_MODE" => "Y",
			"AJAX_OPTION_HISTORY" => "N",
			"AJAX_OPTION_JUMP" => "N",
			"AJAX_OPTION_SHADOW" => "N",
			"AJAX_OPTION_STYLE" => "Y",
			"CUSTOM_TITLE_DATE_ACTIVE_FROM" => "",
			"CUSTOM_TITLE_DATE_ACTIVE_TO" => "",
			"CUSTOM_TITLE_DETAIL_PICTURE" => "",
			"CUSTOM_TITLE_DETAIL_TEXT" => "",
			"CUSTOM_TITLE_IBLOCK_SECTION" => "",
			"CUSTOM_TITLE_NAME" => "Ваше имя",
			"CUSTOM_TITLE_PREVIEW_PICTURE" => "",
			"CUSTOM_TITLE_PREVIEW_TEXT" => "",
			"CUSTOM_TITLE_TAGS" => "",
			"DEFAULT_INPUT_SIZE" => "30",
			"DETAIL_TEXT_USE_HTML_EDITOR" => "N",
			"ELEMENT_ASSOC" => "CREATED_BY",
			"EVENT_NAME" => "CONF_BUY",
			"GROUPS" => array(),
			"LEVEL_LAST" => "Y",
			"LIST_URL" => "",
			"MAX_FILE_SIZE" => "0",
			"MAX_LEVELS" => "100",
			"MAX_USER_ENTRIES" => "100",
			"PREVIEW_TEXT_USE_HTML_EDITOR" => "N",
			"PROPERTY_CODES" => array(
				0 => "#BUY_EMAIL#",
				1 => "#BUY_PHONE#",
				2 => "#BUY_TICKET#",
				3 => "NAME",
			),
			"PROPERTY_CODES_REQUIRED" => array(
				0 => "#BUY_EMAIL#",
				1 => "#BUY_TICKET#",
				2 => "NAME",
			),
			"RESIZE_IMAGES" => "Y",
			"SEF_MODE" => "N",
			"STATUS" => "ANY",
			"STATUS_NEW" => "NEW",
			"USER_MESSAGE_ADD" => "",
			"USER_MESSAGE_EDIT" => "",
			"USE_CAPTCHA" => "N",
			"COMPONENT_TEMPLATE" => "buy",
			"CHECK_UNIQUE_EMAIL" => "N",
			"TICKET_ID" => ($_REQUEST['TICKET_ID']) ? $_REQUEST['TICKET_ID'] : 'N',
			"SUCCESS_MESSAGE" => $successMessage,
			"SUCCESS_MESSAGE_DESCRIPTION" => $successMessageDescription,
			"IS_BUY" => "Y",
		),
		false
	);?>
</div>
<?
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_after.php");
?>