<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
$this->setFrameMode(true);
?>
<section class="online-translation sect-type1 organizers-wrap" id="<?=$arResult['ID']?>">

	<div class="container-fluid" style="position: relative;">
		<h2 class="h1 title-style1 type2"><?
			$APPLICATION->IncludeFile(SITE_DIR . "include/conf/block-online-description-name.php", Array(), Array(
				"MODE" => "text",
				"NAME" => GetMessage('MAGWAI_ONLINE_TRANSLATION_NAME'),
			));
			?></h2>
		<div class="col1" style="color: #FFF;">
			<?
			$APPLICATION->IncludeFile(SITE_DIR . "include/conf/description-link.php", Array(), Array(
				"MODE" => "html",
				"NAME" => GetMessage('MAGWAI_ONLINE_TRANSLATION_LINK'),
			));
			?>
		</div>
		<div class="col2" style="color: #FFF;">
			<h3 class="h2"><?
				$APPLICATION->IncludeFile(SITE_DIR . "include/conf/block-online-description-title.php", Array(), Array(
					"MODE" => "html",
					"NAME" => GetMessage('MAGWAI_ONLINE_TRANSLATION_TITLE'),
				));
				?></h3>
            <div class="ns_organizers">
			<?
			$APPLICATION->IncludeFile(SITE_DIR . "include/conf/block-online-description-description.php", Array(), Array(
				"MODE" => "html",
				"NAME" => GetMessage('MAGWAI_ONLINE_TRANSLATION_DESCRIPTION'),
			));
			?>
            </div>
		</div>
	</div>
</section>