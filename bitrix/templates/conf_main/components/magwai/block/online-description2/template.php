<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
$this->setFrameMode(true);
?>
<section class="online-translation sect-type1 registration-wrap" id="<?=$arResult['ID']?>">

	<div class="container-fluid" style="position: relative;">
		<h2 class="h1 title-style1 type2"><?
			$APPLICATION->IncludeFile(SITE_DIR . "include/conf/block-online-description2-name.php", Array(), Array(
				"MODE" => "text",
				"NAME" => GetMessage('MAGWAI_ONLINE_DESCRIPTION2_NAME'),
			));
			?></h2>
		<div class="col1" style="color: #FFF;">
			<?
			$APPLICATION->IncludeFile(SITE_DIR . "include/conf/description2-link.php", Array(), Array(
				"MODE" => "html",
				"NAME" => GetMessage('MAGWAI_ONLINE_DESCRIPTION2_LINK'),
			));
			?>
		</div>
		<div class="col2" style="color: #FFF;">
			<h3 class="h2"><?
				$APPLICATION->IncludeFile(SITE_DIR . "include/conf/block-online-description2-title.php", Array(), Array(
					"MODE" => "html",
					"NAME" => GetMessage('MAGWAI_ONLINE_DESCRIPTION2_TITLE'),
				));
				?></h3>
            <div class="ns_registration">
			<?
			$APPLICATION->IncludeFile(SITE_DIR . "include/conf/block-online-description2-description.php", Array(), Array(
				"MODE" => "html",
				"NAME" => GetMessage('MAGWAI_ONLINE_DESCRIPTION2_DESCRIPTION'),
			));
			?>
            </div>
		</div>
	</div>
</section>