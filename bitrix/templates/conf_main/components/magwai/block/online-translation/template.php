<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
$this->setFrameMode(true);
?>
<?
/* include area [background block "Tickets"] */
$imagePath = $_SERVER['DOCUMENT_ROOT'] . SITE_DIR . "include/conf/background-tickets.php";
$image = preg_replace('/<img\s+[^>]*?src="([^"]+)"[^>]+>/i', "$1", file_get_contents($imagePath));
?>

<section class="online-translation sect-type1"  <? if($image): ?>style="background: url('<?=$image?>') no-repeat 50% 0/cover;"<? endif; ?> id="<?=$arResult['ID']?>">

	<div class="container-fluid" style="position: relative;">
		<h2 class="h1 title-style1 type2"><?
			$APPLICATION->IncludeFile(SITE_DIR . "include/conf/block-online-translation-name.php", Array(), Array(
				"MODE" => "text",
				"NAME" => GetMessage('MAGWAI_ONLINE_TRANSLATION_NAME'),
			));
			?></h2>
		<div class="col1" style="color: #FFF;">
			<?
			$APPLICATION->IncludeFile(SITE_DIR . "include/conf/translation-link.php", Array(), Array(
				"MODE" => "html",
				"NAME" => GetMessage('MAGWAI_ONLINE_TRANSLATION_LINK'),
			));
			?>
		</div>
		<div class="col2" style="color: #FFF;">
			<h3 class="h2"><?
				$APPLICATION->IncludeFile(SITE_DIR . "include/conf/block-online-translation-title.php", Array(), Array(
					"MODE" => "html",
					"NAME" => GetMessage('MAGWAI_ONLINE_TRANSLATION_TITLE'),
				));
				?></h3>
			<?
			$APPLICATION->IncludeFile(SITE_DIR . "include/conf/block-online-translation-description.php", Array(), Array(
				"MODE" => "html",
				"NAME" => GetMessage('MAGWAI_ONLINE_TRANSLATION_DESCRIPTION'),
			));
			?>
		</div>
	</div>
</section>