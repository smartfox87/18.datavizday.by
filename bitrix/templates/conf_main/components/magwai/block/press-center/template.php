<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
$this->setFrameMode(true);
?>
<section class="faq sect-type1" id="<?=$arResult['ID']?>">
	<div class="container-fluid">
		<h2 class="h1 title-style1"><?
			$APPLICATION->IncludeFile(SITE_DIR . "include/conf/block-press-center-name.php", Array(), Array(
				"MODE" => "text",
				"NAME" => GetMessage('MAGWAI_PRESS_CENTER_NAME'),
			));
			?></h2>
		<div class="col1">
			<h3 class="subtitle"><?
				$APPLICATION->IncludeFile(SITE_DIR . "include/conf/block-press-center-title.php", Array(), Array(
					"MODE" => "html",
					"NAME" => GetMessage('MAGWAI_PRESS_CENTER_TITLE'),
				));
				?></h3>
			<?
			$APPLICATION->IncludeFile(SITE_DIR . "include/conf/block-press-center-description.php", Array(), Array(
				"MODE" => "html",
				"NAME" => GetMessage('MAGWAI_PRESS_CENTER_DESCRIPTION'),
			));
			?>
		</div>
	</div>
</section>
