<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arFilter["LID"] = 'ru';
$arEvent = Array();
$dbType = CEventType::GetList($arFilter, $order="ASC");
while($arType = $dbType->GetNext())
{
	$arEventTypes[$arType["EVENT_NAME"]] = "[" . $arType["EVENT_NAME"] . "] " . $arType["NAME"];
}


$arTemplateParameters["EVENT_NAME"] = Array(
	"NAME" => GetMessage("MAGWAI_IBLOCK_EMAIL_TEMPLATES"),
	"TYPE"=>"LIST",
	"VALUES" => $arEventTypes,
	"DEFAULT"=>"",
	"MULTIPLE"=>"N",
	"COLS"=>25,
);

