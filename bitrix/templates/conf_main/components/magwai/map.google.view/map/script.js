if (!window.BX_GMapAddPlacemark)
{
	window.BX_GMapAddPlacemark = function(arPlacemark, map_id)
	{
		var map = GLOBAL_arMapObjects[map_id];
		
		if (null == map)
			return false;

		if(!arPlacemark.LAT || !arPlacemark.LON)
			return false;

		if (BX.type.isNotEmptyString(arPlacemark.TEXT))
		{
			var myOptions = {
				content: arPlacemark.TEXT,
				boxStyle: {
					width: "0",
					height: "0"
				},
				disableAutoPan: true,
				pixelOffset: new google.maps.Size(-34, -79),
				position: new google.maps.LatLng(arPlacemark.LAT, arPlacemark.LON),
				closeBoxURL: "",
				isHidden: false,
				pane: "floatPane",
				enableEventPropagation: true
				//,domready: tool(i)
			};
			var ibLabel = new InfoBox(myOptions);
			ibLabel.open(map);
		}
	}
}

if (null == window.BXWaitForMap_view)
{
	function BXWaitForMap_view(map_id)
	{
		if (null == window.GLOBAL_arMapObjects)
			return;
	
		if (window.GLOBAL_arMapObjects[map_id])
			window['BX_SetPlacemarks_' + map_id]();
		else
			setTimeout('BXWaitForMap_view(\'' + map_id + '\')', 300);
	}
}