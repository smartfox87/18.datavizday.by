<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die(); ?>
<?
foreach ($arResult['POSITION']['PLACEMARKS'] as &$arMarker) {
	if ($arText = explode("\r\n", $arMarker['TEXT'])) {
		$arText[0] = '<strong>' . $arText[0] . '</strong>';
		$arMarker['TEXT'] = implode("\r\n", $arText);
	}
	$arMarker['TEXT'] = '<div class="place-lbl type1"><span class="title-place">' . $arMarker['TEXT'] . '</span></div>';
}
?>

