<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
$this->setFrameMode(true);
?>
<? $frame = $this->createFrame()->begin(''); ?>
	<? if ($arResult["CONFIRM_UNSUBSCIBE"] == "Y"): ?>
		<script type="text/javascript">
			$(function(){
				$('.modal-box.arcticmodal-unsubscribe').arcticmodal();
			});
		</script>
		<!-- Modal -->
		<div style="display: none">
			<div class="modal-box send-msg arcticmodal-unsubscribe">
				<a href="javascript:void(0)" class="fa fa-close arcticmodal-close close-modal"></a>
				<h1 class="title-s-m"><? if (!empty($arResult["ERROR"])): ?><?=$arResult['ERROR']?><? else: ?><?=GetMessage('MAGWAI_UNSUBSCRIBE_SUCCESS_UNSUBSCRIBE_L')?><? endif; ?></h1>
				<a href="javascript:void(0)" class="btn btn-style2 arcticmodal-close">OK</a>
			</div>
		</div>
	<?endif?>
<? $frame->end(); ?>

