<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<? $this->setFrameMode(true); ?>
<?if (!empty($arResult)):?>
<nav class="main-nav">

<?
//$arResult[0]['SELECTED'] = true; // todo

foreach($arResult as $key => $arItem):
	if($arParams["MAX_LEVEL"] == 1 && $arItem["DEPTH_LEVEL"] > 1) 
		continue;
?>
	<a href="<?=$arItem["LINK"]?>" data-id="<?=($key + 1)?>"><?=$arItem["TEXT"]?></a>
<?endforeach?>

</nav>
<?endif?>