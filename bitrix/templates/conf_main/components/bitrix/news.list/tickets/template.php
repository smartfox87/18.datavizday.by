<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<?
/* include area [background block "Tickets"] */
$imagePath = $_SERVER['DOCUMENT_ROOT'] . SITE_DIR . "include/conf/background-tickets.php";
$image = preg_replace('/<img\s+[^>]*?src="([^"]+)"[^>]+>/i', "$1", file_get_contents($imagePath));
?>
<section class="tickets sect-type1"
         <? if($image): ?>style="background: url('<?=$image?>') no-repeat 50% 0/cover;"<? endif; ?>
         id="tickets">
	<div class="container-fluid  container-fluid--centr">
		<h2 class="h1 title-style1 type2"><?=$arResult['NAME']?></h2>
        <? if (count($arResult["ITEMS"]) > 1):?>
		<div class="owl-carousel carousel3">
        <? endif;?>
		<?foreach($arResult["ITEMS"] as $arItem):?>
			<?
			$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
			$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
			?>
			<div class="ticket-item">
				<? if($arItem['PROPERTIES']['IS_NEW']['VALUE_XML_ID'] == 1): ?>
					<div class="lbl lbl-new"><span>NEW</span></div>
				<? elseif($arItem['PROPERTIES']['DISCOUNT']['VALUE']): ?>
					<div class="lbl lbl-sale"><span><?=GetMessage('MAGWAI_TICKETS_TEMPLATE_DISCOUNT')?>&nbsp;<?=$arItem['PROPERTIES']['DISCOUNT']['VALUE']?>%</span></div>
				<? endif; ?>
				<div class="header-t-i" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
					<h3 class="title-t-i"><?=$arResult['ELEMENT_NAME']?> <?=$arItem['NAME']?><br>
						<? if($arItem['PROPERTIES']['PRICE']['VALUE']): ?>
							<span class="color1"><?=$arItem['PROPERTIES']['PRICE']['VALUE']?>
						<? endif; ?>
						<s><?=GetMessage('MAGWAI_TICKETS_TEMPLATE_RUB')?></s></span>
						<? if($arItem['PROPERTIES']['VALID']['VALUE']): ?>
							<span style="text-transform:lowercase;"><?=$arItem['PROPERTIES']['VALID']['VALUE']?>
						<? endif; ?>
					</h3>
				</div>
				<div class="txt-t-i">
					<div class="row">
						<? if(count($arItem['PROPERTIES']['PARAMS']['VALUE'])): ?>
							<div class="col-sm-6">
								<ul class="list-style1">
									<? $isOutput = true; ?>
									<? foreach($arItem['PROPERTIES']['PARAMS']['VALUE'] as $value): ?>
										<? if($isOutput): ?>
											<li<? if($arResult['TICKETS_PARAMS'][$value]['ACTIVE'] != 'Y'): ?> class="err"<? endif; ?>><?=$arResult['TICKETS_PARAMS'][$value]['NAME']?></li>
										<? endif; ?>
										<? $isOutput = !$isOutput; ?>
									<? endforeach; ?>
								</ul>
							</div>
							<? if(count($arItem['PROPERTIES']['PARAMS']['VALUE']) > 1): ?>
							<div class="col-sm-6">
								<ul class="list-style1">
									<? $isOutput = false; ?>
									<? foreach($arItem['PROPERTIES']['PARAMS']['VALUE'] as $value): ?>
										<? if($isOutput): ?>
											<li<? if($arResult['TICKETS_PARAMS'][$value]['ACTIVE'] != 'Y'): ?> class="err"<? endif; ?>><?=$arResult['TICKETS_PARAMS'][$value]['NAME']?></li>
										<? endif; ?>
										<? $isOutput = !$isOutput; ?>
									<? endforeach; ?>
								</ul>
							</div>
							<? endif; ?>
						<? endif; ?>
					</div>
				</div>
				<div class="text-center">
					<a class="btn btn-style2 show-modal"
					   href="<?=SITE_DIR?>ajax/buy.php?TICKET_ID=<?=$arItem['ID']?>&ncc=1"><?=GetMessage('MAGWAI_TICKETS_TEMPLATE_BUY')?></a>
				</div>
			</div>
		<?endforeach;?>
          <? if (count($arResult["ITEMS"])>1):?>
          </div>
          <? endif;?>
	</div>
</section>