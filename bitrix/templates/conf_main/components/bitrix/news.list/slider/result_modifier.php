<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die(); ?>
<?

if (!function_exists('GetYouTubeID')) {
	/**
	 * get youtube video ID from URL
	 *
	 * @param string $url
	 * @return string Youtube video id or FALSE if none found.
	 */
	function GetYouTubeID($url) {
		$pattern =
			'%^# Match any youtube URL
        (?:https?://)?  # Optional scheme. Either http or https
        (?:www\.)?      # Optional www subdomain
        (?:             # Group host alternatives
          youtu\.be/    # Either youtu.be,
        | youtube\.com  # or youtube.com
          (?:           # Group path alternatives
            /embed/     # Either /embed/
          | /v/         # or /v/
          | /watch\?v=  # or /watch\?v=
          )             # End path alternatives.
        )               # End host alternatives.
        ([\w-]{10,12})  # Allow 10-12 for 11 char youtube id.
        $%x'
		;
		$result = preg_match($pattern, $url, $matches);
		if ($result) {
			return $matches[1];
		}
		return false;
	}
}


$arResult['IS_TIMER'] = false;
$arResult['IS_VIDEO'] = false;
$arResult['IS_GALLERY'] = false;
foreach($arResult['ITEMS'] as &$arItem) {
	if ($arItem['PROPERTIES']['TIMER']['VALUE']) {
		$arResult['IS_TIMER'] = true;
	}
	if ($arItem['PROPERTIES']['VIDEO']['VALUE']) {
		$arItem['PROPERTIES']['VIDEO']['VALUE'] = GetYouTubeID($arItem['PROPERTIES']['VIDEO']['VALUE']);
		$arResult['IS_VIDEO'] = true;
	}
	if ($arItem['PROPERTIES']['SHOW_TYPE']['VALUE_XML_ID'] == 3) {
		$arResult['IS_GALLERY'] = true;
	}
}
$this->__component->arResultCacheKeys = array_merge(
	$this->__component->arResultCacheKeys,
	array('IS_TIMER', 'IS_VIDEO', 'IS_GALLERY')
);