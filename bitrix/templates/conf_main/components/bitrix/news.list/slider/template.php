<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<? if($arResult['IS_TIMER']): ?>
	<script type="text/javascript">
		window.countDownInit = {
			seconds: {
				borderColor: '#FFF',
				borderWidth: '2',
				title: ['<?=GetMessage('MAGWAI_SLIDER_TEMPLATE_S_1')?>', '<?=GetMessage('MAGWAI_SLIDER_TEMPLATE_S_2')?>', '<?=GetMessage('MAGWAI_SLIDER_TEMPLATE_S_3')?>'],
				titleSelector: '.type-seconds'
			},
			minutes: {
				borderColor: '#FFF',
				borderWidth: '2',
				title: ['<?=GetMessage('MAGWAI_SLIDER_TEMPLATE_M_1')?>', '<?=GetMessage('MAGWAI_SLIDER_TEMPLATE_M_2')?>', '<?=GetMessage('MAGWAI_SLIDER_TEMPLATE_M_3')?>'],
				titleSelector: '.type-minutes'
			},
			hours: {
				borderColor: '#FFF',
				borderWidth: '2',
				title: ['<?=GetMessage('MAGWAI_SLIDER_TEMPLATE_H_1')?>', '<?=GetMessage('MAGWAI_SLIDER_TEMPLATE_H_2')?>', '<?=GetMessage('MAGWAI_SLIDER_TEMPLATE_H_3')?>'],
				titleSelector: '.type-hours'
			},
			days: {
				borderColor: '#FFF',
				borderWidth: '2',
				title: ['<?=GetMessage('MAGWAI_SLIDER_TEMPLATE_D_1')?>', '<?=GetMessage('MAGWAI_SLIDER_TEMPLATE_D_2')?>', '<?=GetMessage('MAGWAI_SLIDER_TEMPLATE_D_3')?>'],
				titleSelector: '.type-days'
			}
		};

		window.countDown = {};

	</script>
<? endif; ?>
<div id="about" class="owl-carousel carousel1">
	<?foreach($arResult["ITEMS"] as $key => $arItem):?>
		<?
		$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
		$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
		?>
		<div class="<? if($arItem["PREVIEW_PICTURE"]['SRC']): ?>owl-lazy <? endif; ?>slide"
		     <? if($arItem["PREVIEW_PICTURE"]['SRC']): ?>data-src="<?=$arItem["PREVIEW_PICTURE"]['SRC']?>"<? endif; ?>
		     data-hash="slide-<?=($key + 1)?>"> <? /* market */ ?>

			<? if($arItem['PROPERTIES']['VIDEO']['VALUE']): ?>
				<div class="tubular-container">
					<div class="tubular-player" id="tubular-player-<?=($key + 1)?>" data-id="<?=($key + 1)?>"></div>
				</div>
				<div class="tubular-shield"></div>
			<? endif; ?>

			<div class="container-fluid" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
				<div class="txt-fix">
					<? if($arItem['PROPERTIES']['SUBTITLE']['VALUE']): ?>
						<p class="semibold text-uppercase color1 subtitle"><?=$arItem['PROPERTIES']['SUBTITLE']['VALUE']?></p>
					<? endif; ?>
					<h1 class="title-slide"><?=$arItem['NAME']?></h1>
					<? if($arItem['PREVIEW_TEXT'] && !$arItem['PROPERTIES']['TIMER']['VALUE']): ?>
					<div class="hidden-xs">
						<p class="text"><?=$arItem['PREVIEW_TEXT']?></p>
					</div>
					<? endif; ?>
				</div>

				<?
				$frame = $this->createFrame()->begin('');
				$frame->setAnimation(true);
				?>
				<? if($arItem['PROPERTIES']['TIMER']['VALUE']): ?>
					<? if(time() < MakeTimeStamp($arItem['PROPERTIES']['TIMER']['VALUE'], CSite::GetDateFormat())): ?>
						<div class="countdown countdown-container"
						     data-countdown-id="<?=$key?>">
							<div class="clock row">
								<div class="clock-item clock-days countdown-time-value col-xs-3">
									<div class="wrap">
										<div class="inner">
											<div class="clock-canvas" id="canvas_days_<?=$key?>"></div>
											<div class="text">
												<span>
													<p class="val">0</p>
													<p class="type-days type-time"></p>
												</span>
											</div>
										</div>
									</div>
								</div>
								<div class="clock-item clock-hours countdown-time-value col-xs-3">
									<div class="wrap">
										<div class="inner">
											<div class="clock-canvas" id="canvas_hours_<?=$key?>"></div>
											<div class="text">
												<span>
													<p class="val">0</p>
													<p class="type-hours type-time"></p>
												</span>
											</div>
										</div>
									</div>
								</div>
								<div class="clock-item clock-minutes countdown-time-value col-xs-3">
									<div class="wrap">
										<div class="inner">
											<div class="clock-canvas" id="canvas_minutes_<?=$key?>"></div>
											<div class="text">
												<span>
													<p class="val">0</p>
													<p class="type-minutes type-time"></p>
												</span>
											</div>
										</div>
									</div>
								</div>
								<div class="clock-item clock-seconds countdown-time-value col-xs-3">
									<div class="wrap">
										<div class="inner">
											<div class="clock-canvas" id="canvas_seconds_<?=$key?>"></div>
											<div class="text">
												<span>
													<p class="val">0</p>
													<p class="type-seconds type-time"></p>
												</span>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					<? else: ?>
						<div class="countdown-container timer-end"><?=$arParams['TIMER_END_TEXT']?></div>
					<? endif; ?>
					<script type="text/javascript">
						window.countDown[<?=$key?>] = {
							id: <?=$key?>,
							start: <?=MakeTimeStamp($arItem['DATE_ACTIVE_FROM'], CSite::GetDateFormat())?>,
							end: <?=MakeTimeStamp($arItem['PROPERTIES']['TIMER']['VALUE'], CSite::GetDateFormat())?>,
							now: <?=time()?>,
							selectors: {
								value_seconds: '[data-countdown-id="<?=$key?>"] .clock-seconds .val',
								canvas_seconds: 'canvas_seconds_<?=$key?>',
								value_minutes: '[data-countdown-id="<?=$key?>"] .clock-minutes .val',
								canvas_minutes: 'canvas_minutes_<?=$key?>',
								value_hours: '[data-countdown-id="<?=$key?>"] .clock-hours .val',
								canvas_hours: 'canvas_hours_<?=$key?>',
								value_days: '[data-countdown-id="<?=$key?>"] .clock-days .val',
								canvas_days: 'canvas_days_<?=$key?>',
								title: '.type-time'
							}
						};
					</script>
				<? endif; ?>
				<? $frame->end(); ?>

				<? if($arItem['PROPERTIES']['SHOW_TYPE']['VALUE']): ?>
					<? switch($arItem['PROPERTIES']['SHOW_TYPE']['VALUE_XML_ID']):
						case 1: ?>
							<? $buttonURL = SITE_DIR . 'ajax/buy.php?ncc=1'; ?>
							<? $buttonClass = 'show-modal'; ?>
						<? break; ?>
						<? case 2: ?>
							<? $buttonURL = $arItem['PROPERTIES']['BUTTON_LINK']['VALUE']; ?>
							<? $buttonClass = ''; ?>
						<? break; ?>
						<? case 3: ?>
							<? $buttonURL = SITE_DIR. 'ajax/program.php?ncc=1&PROGRAM_SECTION_ID=' . $arItem['PROPERTIES']['PROGRAM']['VALUE'] . '&ELEMENT_ID=' . $arItem['ID']; ?>
							<? $buttonClass = 'show-modal-program'; ?>
						<? break; ?>
					<? endswitch; ?>
                  <? if ($arItem['PROPERTIES']['BUTTON_NAME']['VALUE']):?>
					<a class="btn btn-style2<? if($buttonClass): ?><?="\040"?><?=$buttonClass?><? endif; ?>"
					href="<? if($buttonURL): ?><?=$buttonURL?><? endif; ?>"><?=$arItem['PROPERTIES']['BUTTON_NAME']['VALUE']?></a>
                    <? endif;?>
				<? endif; ?>
			</div>
		</div>
	<?endforeach;?>
</div>
<? if($arResult['IS_VIDEO']): ?>
	<script type="text/javascript">
		window.sliderVideo = {};

		$(function() {
			<? foreach($arResult["ITEMS"] as $key => $arItem): ?>
				<? if($arItem['PROPERTIES']['VIDEO']['VALUE']): ?>
					window.sliderVideo[<?=($key + 1)?>] = '<?=$arItem['PROPERTIES']['VIDEO']['VALUE']?>';
				<? endif; ?>
			<? endforeach; ?>
			window.tubular();
		});
	</script>
<? endif; ?>