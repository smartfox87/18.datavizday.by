<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die(); ?>
<?
if ($arResult['IS_TIMER']) {
	$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . '/js/kinetic.js');
	$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . '/js/jquery.final-countdown.js');
}
if ($arResult['IS_VIDEO']) {
	$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . '/js/magwai.tubular.js');
}
if ($arResult['IS_GALLERY']) {
	$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . '/js/jquery.magnific-popup.min.js');
	$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH . '/css/magnific-popup.css');
}
?>
