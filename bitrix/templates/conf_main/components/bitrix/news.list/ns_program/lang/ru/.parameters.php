<?
$MESS["NS_PROGRAM_PARAMS_PARENT_SECTION_CODE"] = 'Программа';
$MESS["NS_PROGRAM_PARAMS_RESIZE_PREVIEW"] = 'Уменьшать изображение спикера';
$MESS["NS_PROGRAM_PARAMS_RESIZE_TYPE_PREVIEW"] = 'Тип масштабирования';
$MESS["NS_PROGRAM_PARAMS_RESIZE_TYPE_PREVIEW_1"] = 'C сохранением пропорций, c шириной из макс. значения высоты/ширины';
$MESS["NS_PROGRAM_PARAMS_RESIZE_TYPE_PREVIEW_2"] = 'C сохранением пропорций';
$MESS["NS_PROGRAM_PARAMS_RESIZE_TYPE_PREVIEW_3"] = 'C сохранением пропорций, обрезая лишнее';
$MESS["NS_PROGRAM_PARAMS_WIDTH_PREVIEW"] = "Ширина";
$MESS["NS_PROGRAM_PARAMS_HEIGHT_PREVIEW"] = "Высота";
$MESS["NS_PROGRAM_PARAMS_DISPLAY_SPEAKER_POSITION"] = "Отображать должность спикера";


