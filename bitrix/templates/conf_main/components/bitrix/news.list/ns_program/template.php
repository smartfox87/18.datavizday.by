<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<section class="program sect-type1" id="program" xmlns="http://www.w3.org/1999/html">
	<div class="container-fluid">
		<h2 class="h1 title-style1"><?=$arResult['SECTION_NAME']?></h2>
		<div class="tabs tabs1">
			<div class="ctrls-tabs">
				<?foreach($arResult["HALLS"] as $hallId => $arHall):?>
                  <? if (array_shift($arHall['ITEMS'])['PROPERTIES']['TOP_BLOCK']['VALUE'] == 'YES'):?>
				<a class="ctrl-tabs" href="javascript:void(0)"><strong><?=$arResult['HALLS_PROPERTIES'][$hallId]['NAME']?></strong><br><span><?=$arResult['HALLS_PROPERTIES'][$hallId]['PROPERTY_SHORT_NAME_VALUE']?></span><br><?=$arHall['HALL_TIME_FROM'];?>-<?=$arHall['HALL_TIME_TO'];?></a>
              <? endif;?>
				<?endforeach;?>
			</div>
			<div class="items-tabs">
				<?foreach($arResult["HALLS"] as $arHall):?>
              <? if (array_shift($arHall['ITEMS'])['PROPERTIES']['TOP_BLOCK']['VALUE'] == 'YES'):?>
					<div class="item-tabs">
						<div class="scroll-pane">
							<?foreach($arHall['ITEMS'] as $key => $arItem):?>
								<?
								$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
								$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
								$arSpeaker = $arResult['SPEAKERS'][$arItem['PROPERTIES']['SPEAKER']['VALUE']];
								?>
								<article class="write-item2<? if($key >= 2): ?> hidden-xs<? endif; ?>"
								         id="<?=$this->GetEditAreaId($arItem['ID']);?>">
									<? if($arSpeaker): ?>
										<div class="img-w-i2">
											<?if($arParams["DISPLAY_PICTURE"]!="N" && is_array($arSpeaker["PREVIEW_PICTURE"])):?>
												<img src="<?=$arSpeaker["PREVIEW_PICTURE"]['SRC']?>" alt="<?=$arSpeaker['NAME']?>">
											<? else: ?>
												<img src="<?=SITE_TEMPLATE_PATH . '/img/default-profile.png'?>" alt="<?=$arSpeaker['NAME']?>">
											<? endif; ?>
										</div>
									<? endif; ?>
									<div class="text-item2<? if($arItem['CHILDREN']): ?> children<? endif; ?><? if($arItem['PARENT']): ?> parent<? endif; ?><? if(!$arSpeaker): ?> not-speaker<? endif; ?>">
										<header>
											<? if($arSpeaker): ?>
											<div class="el-w-i2">
												<span class="fa fa-bullhorn"></span>&nbsp;&nbsp;<?=$arSpeaker['NAME']?>
											</div>
											<? endif; ?>
											<? if(!$arItem['CHILDREN']): ?>
												<div class="el-w-i2">
													<span class="fa fa-clock-o"></span>&nbsp;&nbsp;<?=$arItem['TIME']?><? if($arItem['TIME_TO']): ?>&nbsp;-&nbsp;<?=$arItem['TIME_TO']?><? endif; ?>
												</div>
											<? endif; ?>
											<? if(!$arItem['CHILDREN'] || !$arSpeaker): ?>
												<h3 class="title-w-i2 title-m-b"><?=$arItem['NAME']?></h3>
												<? if(trim($arItem['PREVIEW_TEXT'])): ?>
													<p><?=$arItem['PREVIEW_TEXT']?></p>
												<? endif; ?>
											<? endif; ?>
										</header>
									</div>
								</article>
							<?endforeach;?>
						</div>
						<div class="text-center visible-xs"><a class="btn btn-style2 show-events" href="javascript:void(0)"><?=GetMessage('NS_PROPRAM_TEMPLATE_ALL')?></a></div>
					</div>
              <? endif;?>
				<?endforeach;?>
			</div>
		</div>
		<div class="tabs tabs1">
			<div class="ctrls-tabs">
				<?foreach($arResult["HALLS"] as $hallId => $arHall):?>
              <? if (array_shift($arHall['ITEMS'])['PROPERTIES']['TOP_BLOCK']['VALUE'] != 'YES'):?>
				<a class="ctrl-tabs" href="javascript:void(0)"><strong><?=$arResult['HALLS_PROPERTIES'][$hallId]['NAME']?></strong><br><span><?=$arResult['HALLS_PROPERTIES'][$hallId]['PROPERTY_SHORT_NAME_VALUE']?></span><br><?=$arHall['HALL_TIME_FROM'];?>-<?=$arHall['HALL_TIME_TO'];?></a>
              <? endif;?>
				<?endforeach;?>
			</div>
			<div class="items-tabs">
				<?foreach($arResult["HALLS"] as $arHall):?>
                  <? var_dump(array_shift($arHall['ITEMS'])['PROPERTIES']['TOP_BLOCK']['VALUE'])?>
              <? if (array_shift($arHall['ITEMS'])['PROPERTIES']['TOP_BLOCK']['VALUE'] != 'YES'):?>
                  <? var_dump($arHall['ITEMS'])?>
					<div class="item-tabs">
						<div class="scroll-pane">
							<?foreach($arHall['ITEMS'] as $key => $arItem):?>
								<?
								$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
								$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
								$arSpeaker = $arResult['SPEAKERS'][$arItem['PROPERTIES']['SPEAKER']['VALUE']];
								?>
								<article class="write-item2<? if($key >= 2): ?> hidden-xs<? endif; ?>"
								         id="<?=$this->GetEditAreaId($arItem['ID']);?>">
									<? if($arSpeaker): ?>
										<div class="img-w-i2">
											<?if($arParams["DISPLAY_PICTURE"]!="N" && is_array($arSpeaker["PREVIEW_PICTURE"])):?>
												<img src="<?=$arSpeaker["PREVIEW_PICTURE"]['SRC']?>" alt="<?=$arSpeaker['NAME']?>">
											<? else: ?>
												<img src="<?=SITE_TEMPLATE_PATH . '/img/default-profile.png'?>" alt="<?=$arSpeaker['NAME']?>">
											<? endif; ?>
										</div>
									<? endif; ?>
									<div class="text-item2<? if($arItem['CHILDREN']): ?> children<? endif; ?><? if($arItem['PARENT']): ?> parent<? endif; ?><? if(!$arSpeaker): ?> not-speaker<? endif; ?>">
										<header>
											<? if($arSpeaker): ?>
											<div class="el-w-i2">
												<span class="fa fa-bullhorn"></span>&nbsp;&nbsp;<?=$arSpeaker['NAME']?>
											</div>
											<? endif; ?>
											<? if(!$arItem['CHILDREN']): ?>
												<div class="el-w-i2">
													<span class="fa fa-clock-o"></span>&nbsp;&nbsp;<?=$arItem['TIME']?><? if($arItem['TIME_TO']): ?>&nbsp;-&nbsp;<?=$arItem['TIME_TO']?><? endif; ?>
												</div>
											<? endif; ?>
											<? if(!$arItem['CHILDREN'] || !$arSpeaker): ?>
												<h3 class="title-w-i2 title-m-b"><?=$arItem['NAME']?></h3>
												<? if(trim($arItem['PREVIEW_TEXT'])): ?>
													<p><?=$arItem['PREVIEW_TEXT']?></p>
												<? endif; ?>
											<? endif; ?>
										</header>
									</div>
								</article>
							<?endforeach;?>
						</div>
						<div class="text-center visible-xs"><a class="btn btn-style2 show-events" href="javascript:void(0)"><?=GetMessage('NS_PROPRAM_TEMPLATE_ALL')?></a></div>
					</div>
              <? endif;?>
				<?endforeach;?>
			</div>
		</div>
	</div>
</section>