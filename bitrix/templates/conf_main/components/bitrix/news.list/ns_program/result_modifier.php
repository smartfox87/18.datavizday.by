<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die(); ?>
<?
$arSpeakersID = array();
$arHalls = array();
$arHallsId = array();
$prevDateHash = false;
$countItems = count($arResult["ITEMS"]);
foreach ($arResult["ITEMS"] as $key => &$arItem)
{
	if ($arItem['PROPERTIES']['SPEAKER']['VALUE']) {
		$arSpeakersID[] = $arItem['PROPERTIES']['SPEAKER']['VALUE'];
	}
    $arHallsId[] = $arItem['PROPERTIES']['HALL']['VALUE'];

    $activeFromTimeStamp = MakeTimeStamp($arItem["ACTIVE_FROM"]);
	$activeToTimeStamp = MakeTimeStamp($arItem["ACTIVE_TO"]);
	$siteDateFormat = CSite::GetDateFormat();

	$hallKey = $arItem['PROPERTIES']['HALL']['VALUE'];
	$dateDayFormat = CIBlockFormatProperties::DateFormat('j', $activeFromTimeStamp, $siteDateFormat);
	$dateMonthFormat = CIBlockFormatProperties::DateFormat('F', $activeFromTimeStamp, $siteDateFormat);
	$timeFormat = CIBlockFormatProperties::DateFormat('G:i', $activeFromTimeStamp, $siteDateFormat);

    if(!isset($arHalls[$hallKey]['HALL_TIME_FROM']))
    {
        $arHalls[$hallKey]['HALL_TIME_FROM'] = $timeFormat;
    }
    $arHalls[$hallKey]['HALL_TIME_TO'] = '';

	if ($arItem["ACTIVE_TO"]) {
		$timeFormatTo = CIBlockFormatProperties::DateFormat('G:i', $activeToTimeStamp, CSite::GetDateFormat());
		$arItem['TIME_TO'] = $timeFormatTo;
        $arHalls[$hallKey]['HALL_TIME_TO'] = $timeFormatTo;
	}


	$curDateHash = md5($arItem['PROPERTIES']['HALL']['VALUE'].$arItem["ACTIVE_FROM"].$arItem["ACTIVE_TO"]);
	if ($key != ($countItems - 1)) {
		$nextDateHash = md5($arResult["ITEMS"][($key + 1)]['PROPERTIES']['HALL']['VALUE'].$arResult["ITEMS"][($key + 1)]["ACTIVE_FROM"].$arResult["ITEMS"][($key + 1)]["ACTIVE_TO"]);
		$arItem['PARENT'] = ($curDateHash == $nextDateHash);
	} else {
		$arItem['PARENT'] = false;
	}

	$arItem['CHILDREN'] = ($curDateHash == $prevDateHash);
	$prevDateHash = $curDateHash;

	$arItem['TIME'] = $timeFormat;
    $arHalls[$hallKey]['DAY'] = $dateDayFormat;
    $arHalls[$hallKey]['MONTH'] = $dateMonthFormat;
    $arHalls[$hallKey]['ITEMS'][] = $arItem;
}
$arResult['HALLS'] = $arHalls;

$arHallsId = array_unique($arHallsId);
$arSpeakersID = array_unique($arSpeakersID);
$res = CIBlock::GetProperties($arResult['ID'], array(), array());
while($arIBlockProperties = $res->Fetch())
{
    if ($arIBlockProperties['LINK_IBLOCK_ID'] && ($arIBlockProperties['CODE'] === 'SPEAKER'))
    {
        $arOrder = array("SORT" => "ASC");
        $arFilter = array(
            'ID' => $arSpeakersID,
            'IBLOCK_ID' => $arIBlockProperties['LINK_IBLOCK_ID'],
        );
        $arSelectFields = array("ID", "NAME", "PREVIEW_PICTURE", "PROPERTY_POSITION");
        $rsElements = CIBlockElement::GetList($arOrder, $arFilter, FALSE, FALSE, $arSelectFields);
        $arSpeakers = array();
        while ($arElement = $rsElements->GetNext())
        {
            if (($arParams["RESIZE_PREVIEW"] == 'Y') && $arParams["WIDTH_PREVIEW"] && $arParams["HEIGHT_PREVIEW"])
            {
                if ($arElement["PREVIEW_PICTURE"])
                {
                    $arImg = CFile::ResizeImageGet(
                        $arElement["PREVIEW_PICTURE"],
                        array("width" => $arParams["WIDTH_PREVIEW"], "height" => $arParams["HEIGHT_PREVIEW"]),
                        intval($arParams["RESIZE_TYPE_PREVIEW"]),
                        true
                    );
                    if ($arImg)
                    {
                        $arElement["PREVIEW_PICTURE"] = array();
                        $arElement["PREVIEW_PICTURE"]["SRC"] = $arImg["src"];
                        $arElement["PREVIEW_PICTURE"]["WIDTH"] = $arImg["width"];
                        $arElement["PREVIEW_PICTURE"]["HEIGHT"] = $arImg["height"];
                    }
                }
            } elseif ($arElement["PREVIEW_PICTURE"]) {
                $arElement["PREVIEW_PICTURE"] = CFile::GetFileArray($arElement["PREVIEW_PICTURE"]);
            }
            $arSpeakers[$arElement['ID']] = $arElement;
        }
        $arResult['SPEAKERS'] = $arSpeakers;
    }
    elseif($arIBlockProperties['LINK_IBLOCK_ID'] && ($arIBlockProperties['CODE'] === 'HALL'))
    {
        $arOrder = array();
        $arFilter = array(
            'ID' => $arHallsId,
            'IBLOCK_ID' => $arIBlockProperties['LINK_IBLOCK_ID'],
        );
        $arSelect = array("ID", "NAME", "PROPERTY_SHORT_NAME");
        $res = CIBlockElement::GetList($arOrder, $arFilter, false, false, $arSelect);
        while($arField = $res->Fetch())
        {
            $arResult['HALLS_PROPERTIES'][$arField['ID']] = $arField;
        }

    }

}


