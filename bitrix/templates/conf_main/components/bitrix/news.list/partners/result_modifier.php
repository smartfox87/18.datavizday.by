<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die(); ?>
<?
$countRows = intval((($arParams['ROWS']) ? $arParams['ROWS'] : 0) + 1);
$arCols = array();
$counterCols = 0;
$counterRows = 0;
$countItems = $arResult['ITEMS'];
foreach ($arResult['ITEMS'] as $arItem) {
	$arCols[$counterCols][] = $arItem;
	$counterRows++;
	if ($counterRows >= $countRows) {
		$counterRows = 0;
		$counterCols++;
	}
}
$arResult['COLS'] = $arCols;
