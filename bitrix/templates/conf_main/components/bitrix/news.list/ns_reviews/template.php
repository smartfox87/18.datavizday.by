<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<section class="reviews sect-type1" id="reviews">
	<div class="container-fluid">
		<h2 class="h1 title-style1"><?=$arResult['NAME']?></h2>
		<div class="owl-carousel carousel4">
		<?foreach($arResult["ITEMS"] as $arItem):?>
			<?
			$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
			$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
			?>
			<article class="new-item" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
				<a class="img-n-i show-modal"
				   href="<?=SITE_DIR?>ajax/reviews.php?ncc=1&ELEMENT_ID=<?=$arItem['ID']?>">
					<div class="padding-height">
						<?if($arParams["DISPLAY_PICTURE"]!="N" && is_array($arItem["PREVIEW_PICTURE"])):?>
							<img class="owl-lazy scale"
							     data-src="<?=$arItem["PREVIEW_PICTURE"]['SRC']?>"
							     alt="<?=$arItem['NAME']?>"
							     data-scale="best-fill"
							     data-align="center">
						<? endif; ?>
					</div>
				</a>
				<div class="clearfix">
					<h3 class="title-n-i">
						<a class="show-modal"
						   href="<?=SITE_DIR?>ajax/reviews.php?ncc=1&ELEMENT_ID=<?=$arItem['ID']?>"><?=$arItem['NAME']?></a>
					</h3>
				</div>
				<? if($arItem['PREVIEW_TEXT']): ?>
					<p><?=$arItem['PREVIEW_TEXT']?></p>
				<? endif; ?>
			</article>
		<?endforeach;?>
		</div>
	</div>
</section>