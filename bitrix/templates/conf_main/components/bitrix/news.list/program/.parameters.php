<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

if ($arCurrentValues["IBLOCK_ID"]) {

	if(!CModule::IncludeModule("iblock"))
		return;
	$arSort = array("SORT" => "ASC");
	$arFilter = array("IBLOCK_ID" => $arCurrentValues["IBLOCK_ID"]);
	$rsSections = CIBlockSection::GetList($arSort, $arFilter);
	$arSections = array();
	while ($arSection = $rsSections->GetNext())
	{
		$arSections[$arSection["CODE"]] = $arSection["NAME"];
	}

	$arTemplateParameters["PARENT_SECTION_CODE"] = Array(
		"PARENT" => "BASE",
		"NAME" => GetMessage('MAGWAI_PROGRAM_PARAMS_PARENT_SECTION_CODE'),
		"TYPE" => "LIST",
		"VALUES" => $arSections,
		"DEFAULT" => '={$_REQUEST["SECTION_CODE"]}',
	);
}

$arTemplateParameters["DISPLAY_SPEAKER_POSITION"] = Array(
	"NAME" => GetMessage('MAGWAI_PROGRAM_PARAMS_DISPLAY_SPEAKER_POSITION'),
	"TYPE" => "CHECKBOX",
	"DEFAULT" => "N",
);

$arTemplateParameters["RESIZE_PREVIEW"] = Array(
	"NAME" => GetMessage('MAGWAI_PROGRAM_PARAMS_RESIZE_PREVIEW'),
	"TYPE" => "CHECKBOX",
	"REFRESH" => "Y",
	"DEFAULT" => "N",
);

if ($arCurrentValues["RESIZE_PREVIEW"] == "Y") {
	$arTemplateParameters["RESIZE_TYPE_PREVIEW"] = Array(
		"NAME" => GetMessage('MAGWAI_PROGRAM_PARAMS_RESIZE_TYPE_PREVIEW'),
		"TYPE" => "LIST",
		"VALUES" => array(
			'0' => GetMessage('MAGWAI_PROGRAM_PARAMS_RESIZE_TYPE_PREVIEW_1'),
			'1' => GetMessage('MAGWAI_PROGRAM_PARAMS_RESIZE_TYPE_PREVIEW_2'),
			'2' => GetMessage('MAGWAI_PROGRAM_PARAMS_RESIZE_TYPE_PREVIEW_3'),
		),
		"DEFAULT" => '0',
	);
	$arTemplateParameters["WIDTH_PREVIEW"] = Array(
		"NAME" => GetMessage('MAGWAI_PROGRAM_PARAMS_WIDTH_PREVIEW'),
		"TYPE" => "STRING",
		"DEFAULT" => "0",
	);
	$arTemplateParameters["HEIGHT_PREVIEW"] = Array(
		"NAME" => GetMessage('MAGWAI_PROGRAM_PARAMS_HEIGHT_PREVIEW'),
		"TYPE" => "STRING",
		"DEFAULT" => "0",
	);
}