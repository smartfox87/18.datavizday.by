<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="container-fluid">
    <h2 class="h1 title-style1 type4"><?
        $APPLICATION->IncludeFile(SITE_DIR."include/conf/listeners-title.php", array(), array(
            "MODE" => "text",
            "NAME" => GetMessage('TT_NS_TOPICS_TITLE'),
        ));
    ?></h2>

    <div class="owl-carousel carousel5_1">
		<?foreach($arResult["ITEMS"] as $arItem):?>
			<?
			$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
			$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
			?>
			<article class="write-item" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
				<?if($arParams["DISPLAY_PICTURE"]!="N" && $arItem['PROPERTIES']['ICON2']['VALUE']):?>
					<div class="icon-w-i listeners-icon">
                        <img src="<?=$arItem['DISPLAY_PROPERTIES']['ICON2']['FILE_VALUE']['SRC'];?>" alt="<?=$arItem['NAME'];?>" />
					</div>
				<? endif; ?>
				<h2 class="title-w-i"><?=$arItem['NAME']?></h2>
				<? if($arItem['PREVIEW_TEXT']): ?>
					<p><?=$arItem['PREVIEW_TEXT']?></p>
				<? endif; ?>
			</article>
		<?endforeach;?>
	</div>
</div>