<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="container-fluid">
	<div class="owl-carousel carousel5">
		<?foreach($arResult["ITEMS"] as $arItem):?>
			<?
			$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
			$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
			?>
			<article class="write-item" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
				<? if($arResult['PROPERTIES']['ICON']['VALUE']): ?>
				<div class="icon-w-i fa fa-<?=$arResult['PROPERTIES']['ICON']['VALUE']?>"></div>
				<? endif; ?>
				<h2 class="title-w-i"><?=$arItem['NAME']?></h2>
				<? if($arItem['PREVIEW_TEXT']): ?>
					<p><?=$arItem['PREVIEW_TEXT']?></p>
				<? endif; ?>
			</article>
		<?endforeach;?>
	</div>
</div>