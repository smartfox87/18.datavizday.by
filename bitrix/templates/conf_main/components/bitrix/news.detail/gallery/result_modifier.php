<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die(); ?>
<?
foreach ($arResult['PROPERTIES']['PICTURES']['VALUE'] as $key => $picture) {
	$arImgPreview = CFile::ResizeImageGet(
		$picture,
		array('width' => 200, 'height' => 150),
		BX_RESIZE_IMAGE_EXACT,
		true
	);
	if ($arImgPreview) {
		$arResult['PROPERTIES']['PICTURES']['PREVIEW_PICTURES'][$key]['SRC'] = $arImgPreview["src"];
		$arResult['PROPERTIES']['PICTURES']['PREVIEW_PICTURES'][$key]["WIDTH"] = $arImgPreview["width"];
		$arResult['PROPERTIES']['PICTURES']['PREVIEW_PICTURES'][$key]["HEIGHT"] = $arImgPreview["height"];
	}
	$arImgDetail = CFile::ResizeImageGet(
		$picture,
		array('width' => 1920, 'height' => 1080),
		BX_RESIZE_IMAGE_PROPORTIONAL_ALT,
		true
	);
	if ($arImgDetail) {
		$arResult['PROPERTIES']['PICTURES']['DETAIL_PICTURES'][$key]['SRC'] = $arImgDetail["src"];
		$arResult['PROPERTIES']['PICTURES']['DETAIL_PICTURES'][$key]["WIDTH"] = $arImgDetail["width"];
		$arResult['PROPERTIES']['PICTURES']['DETAIL_PICTURES'][$key]["HEIGHT"] = $arImgDetail["height"];
	}
}