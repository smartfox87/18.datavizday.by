<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<div class="col1">
    <? if($arParams["DISPLAY_PICTURE"]!="N"): ?>
        <? if(is_array($arResult["DETAIL_PICTURE"])): ?>
            <div class="img-new">
                <img src="<?=$arResult["DETAIL_PICTURE"]['SRC']?>" alt="<?=$arResult['NAME']?>">
            </div>
        <? elseif(is_array($arResult["PREVIEW_PICTURE"])): ?>
            <div class="img-new">
                <img src="<?=$arResult["PREVIEW_PICTURE"]['SRC']?>" alt="<?=$arResult['NAME']?>">
            </div>
        <? endif; ?>
    <? endif; ?>
</div>
