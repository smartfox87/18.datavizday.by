<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<div class="cols2">
	<div class="col1">
		<?if($arParams["DISPLAY_PICTURE"]!="N" && is_array($arResult["PREVIEW_PICTURE"])):?>
			<div class="img-speaker">
				<img src="<?=$arResult["PREVIEW_PICTURE"]['SRC']?>" alt="<?=$arResult['NAME']?>">
			</div>
		<? endif; ?>
		<div class="soc-prof">
			<? if($arResult['PROPERTIES']['VK_LINK']['VALUE']): ?>
				<a href="<?=$arResult['PROPERTIES']['VK_LINK']['VALUE']?>" target="_blank" class="fa fa-vk" title="���������"></a>
			<? endif; ?>
			<? if($arResult['PROPERTIES']['FB_LINK']['VALUE']): ?>
				<a href="<?=$arResult['PROPERTIES']['FB_LINK']['VALUE']?>" target="_blank" class="fa fa-facebook" title="Facebook"></a>
			<? endif; ?>
			<? if($arResult['PROPERTIES']['TW_LINK']['VALUE']): ?>
				<a href="<?=$arResult['PROPERTIES']['TW_LINK']['VALUE']?>" target="_blank" class="fa fa-twitter" title="Twitter"></a>
			<? endif; ?>
		</div>
	</div>
	<div class="col2">
		<h1 class="title-style3"><?=$arResult['NAME']?></h1>
		<? if($arResult['PROPERTIES']['POSITION']['VALUE']): ?>
			<p class="status-speaker"><?=$arResult['PROPERTIES']['POSITION']['VALUE']?></p>
		<? endif; ?>
		<hr>
		<? if($arResult['PREVIEW_TEXT']): ?>
			<p><?=$arResult['PREVIEW_TEXT']?></p>
		<? endif; ?>
	</div>
</div>
