window.tubular = function() {

    var options = {
        ratio: 16/9, // usually either 4/3 or 16/9 -- tweak as needed
        videoId: 'ZCAnLxRvNNc', // toy robot in space is a good default, no?
        mute: true,
        repeat: true,
        width: $(window).width(),
        wrapperZIndex: 99,
        playButtonClass: 'tubular-play',
        pauseButtonClass: 'tubular-pause',
        muteButtonClass: 'tubular-mute',
        volumeUpClass: 'tubular-volume-up',
        volumeDownClass: 'tubular-volume-down',
        increaseVolumeBy: 10,
        start: 0
    };

    // load yt iframe js api
    var tag = document.createElement('script');
    tag.src = "https://www.youtube.com/player_api";
    var firstScriptTag = document.getElementsByTagName('script')[0];
    firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

    var isIPAD = (navigator.userAgent.match(/iPad/i) != null);
    var isIPHONE = (navigator.userAgent.match(/iPhone/i) != null) || (navigator.userAgent.match(/iPod/i) != null);
    window.isApple = (isIPAD || isIPHONE);

    // set up iframe player, use global scope so YT api can talk
    window.player = {};
    window.onYouTubeIframeAPIReady = function() {
        $.each(window.sliderVideo, function(index, value) {
            var playerObject = new YT.Player('tubular-player-' + index, {
                width: options.width + (options.width/100 * 1),
                height: $('.slide .container-fluid').height(),
                videoId: value,
                playerVars: {
                    controls: 0,
                    showinfo: 0,
                    modestbranding: 1,

                    playsinline: 1
                },
                events: {
                    'onReady': onPlayerReady,
                    'onStateChange': onPlayerStateChange
                }
            });
            player[index] = playerObject;

            if (window.isApple) {
                var tubularShield = $('#tubular-player-' + index).parent().next('.tubular-shield');
                var imagePreview = $('<img/>').attr('src', 'http://img.youtube.com/vi/' + value + '/0.jpg')
                    .css('width', 'auto').css('height', tubularShield.height() + 'px');
                tubularShield.append(imagePreview);
            }
        });
    };

    window.onPlayerReady = function(e) {
        e.target.mute();
        if (!window.isApple) {
            resize();
            e.target.seekTo(options.start);
            e.target.playVideo();
        }
    };

    window.onPlayerStateChange = function(state) {
        if (state.data === 0 && options.repeat) { // video ended and repeat option is set true
            state.target.seekTo(options.start); // restart
        }
    };

    // resize handler updates width, height and offset of player after resize/init
    var resize = function() {
        var width = $(window).width(),
            pWidth, // player width, to be defined
            height = $(window).height(),
            pHeight, // player height, tbd
            $tubularPlayer = $('.tubular-player');

        // when screen aspect ratio differs from video, video must center and underlay one dimension

        if (width / options.ratio < height) { // if new video height < window height (gap underneath)
            pWidth = Math.ceil(height * options.ratio); // get new player width
            $tubularPlayer.width(pWidth).height(height).css({left: (width - pWidth) / 2, top: 0}); // player width is greater, offset left; reset top
        } else { // new video width < window width (gap to right)
            pHeight = Math.ceil(width / options.ratio); // get new player height
            $tubularPlayer.width(width).height(pHeight).css({left: 0, top: (height - pHeight) / 2}); // player height is greater, offset top; reset left
        }

    }

    // events
    if (window.isApple) {
        $('.carousel1 .container-fluid').on('click', function (event) {
            if (!$(event.target).hasClass('show-modal') &&
                !$(event.target).hasClass('owl-prev') &&
                !$(event.target).hasClass('owl-next')
            ) {
                if ($(this).parent().find('.tubular-player').length > 0) {
                    $(this).parent().find('.tubular-shield').html('');
                    var playerID = $(this).parent().find('.tubular-player').data('id');
                    window.player[playerID].mute();
                    window.player[playerID].playVideo();
                }
            }
        });
    }
    $(window).on('resize.tubular', function() {
        resize();
    });
}
