<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die(); ?>
<?
IncludeTemplateLangFile(__FILE__);
?>
		</div>
		<footer class="footer" style="background: #1f2c39;">
			<div class="container-fluid">
				<div class="copyright">&copy;&nbsp;<?$APPLICATION->IncludeComponent("bitrix:main.include", "", array(
						"AREA_FILE_SHOW" => "file",
						"PATH" => SITE_DIR . "include/conf/conf-name.php",
					)
				);?></div>
				<div class="develop">
					<div id="bx-composite-banner" class="composite-banner"></div>
					<a href="https://www.newsite.by" target="_blank"><?$APPLICATION->IncludeComponent("bitrix:main.include", "", array(
						"AREA_FILE_SHOW" => "file",
						"PATH" => SITE_DIR . "include/conf/devel_pre.php",
					)
				);?></a><?$APPLICATION->IncludeComponent("bitrix:main.include", "", array(
						"AREA_FILE_SHOW" => "file",
						"PATH" => SITE_DIR . "include/conf/devel_name.php",
					)
				);?>
				</div>
				<div class="soc-icons">
					<? $vkPath = $_SERVER['DOCUMENT_ROOT'] . SITE_DIR . 'include/conf/vk.php'; ?>
					<? if($vkLink = trim(file_get_contents($vkPath))): ?>
						<a href="<?=$vkLink?>" class="fa fa-vk"></a>
					<? endif; ?>
					<? $fbPath = $_SERVER['DOCUMENT_ROOT'] . SITE_DIR . 'include/conf/fb.php'; ?>
					<? if($fbLink = trim(file_get_contents($fbPath))): ?>
						<a href="<?=$fbLink?>" class="fa fa-facebook"></a>
					<? endif; ?>
					<? $twPath = $_SERVER['DOCUMENT_ROOT'] . SITE_DIR . 'include/conf/tw.php'; ?>
					<? if($twLink = trim(file_get_contents($twPath))): ?>
						<a href="<?=$twLink?>" class="fa fa-twitter"></a>
					<? endif; ?>
				</div>

				<?\Bitrix\Main\Page\Frame::getInstance()->startDynamicWithID("template-settings");?>
				<?if ($GLOBALS['USER']->IsAdmin()):?>
					<section>
						<div class="container">
							<div class="include-area">
								<table class="table">
									<caption><?=GetMessage('SETTING_AREAS')?></caption>
									<thead>
									<tr>
										<th>#</th>
										<th><?=GetMessage('SETTING_AREAS_AREAS')?></th>
										<th><?=GetMessage('SETTING_AREAS_VALUE')?></th>
									</tr>
									</thead>
									<tbody>
									<?
									$arTemplateSetting = array(
										/*'logo' => GetMessage('SETTING_AREAS_logo'),
										'translation-link' => GetMessage('SETTING_AREAS_translation-link'),*/
										'component-buy' => GetMessage('SETTING_AREAS_component-buy'),
										'background-tickets' => GetMessage('SETTING_AREAS_background-tickets'),
										'vk' => GetMessage('SETTING_AREAS_vk'),
										'fb' => GetMessage('SETTING_AREAS_fb'),
										'tw' => GetMessage('SETTING_AREAS_tw'),
										'success-buy' => GetMessage('SETTING_AREAS_success-buy'),
										'success-buy-description' => GetMessage('SETTING_AREAS_success-buy-description'),
										'paid-success' => GetMessage('SETTING_AREAS_paid-success'),
										'paid-success-description' => GetMessage('SETTING_AREAS_paid-success-description'),
										'paid-fail' => GetMessage('SETTING_AREAS_paid-fail'),
										'success-register' => GetMessage('SETTING_AREAS_success-register'),
										'success-register-description' => GetMessage('SETTING_AREAS_success-register-description'),
										'success-subscribe' => GetMessage('SETTING_AREAS_success-subscribe'),
										'success-subscribe-description' => GetMessage('SETTING_AREAS_success-subscribe-description'),
									);
									?>
									<? $counter = 1; $isSetting = true; // for component-buy.php ?>
									<? foreach($arTemplateSetting as $fileName => $text): ?>
										<tr>
											<th><?=$counter?></th>
											<td><?=$text?></td>
											<td>
												<? if(strpos($fileName, 'component') === false): ?>
													<?$APPLICATION->IncludeComponent("bitrix:main.include", "", array(
															"AREA_FILE_SHOW" => "file",
															"PATH" => SITE_DIR . "include/conf/" . $fileName . ".php",
														)
													);?>
												<? else: ?>
													<? $componentPath = $_SERVER['DOCUMENT_ROOT'] . SITE_DIR . "include/conf/" . $fileName . ".php"; ?>
													<? require $componentPath; ?>
												<? endif; ?>
											</td>
										</tr>
										<? $counter++; ?>
									<? endforeach; ?>
									<? $isSetting = false; ?>
									</tbody>
								</table>
							</div>
						</div>
					</section>

					<div class="style-switcher">
						<div class="txt-c style-switcher__header"><?=GetMessage('MAGWAI_FOOTER_SETTING')?><span class="style-switcher__icon"></span></div>
						<div class="style-switcher__body">
							<form id="style-controller" action="">
								<div class="style-switcher__block">
									<div class="style-switcher__b-title"><?=GetMessage('MAGWAI_FOOTER_SETTING_COLOR')?></div>
									<!-- date attribute must match the file name-->
									<div class="style-switcher__colors">
										<?
											$themeColorDafault = 'e87162';
											$themeColor = COption::GetOptionString("main", "conf_theme_color");
											$themeColor = ($themeColor) ? $themeColor : $themeColorDafault;
											$arThemesColor = array('e87162', '05a4cd', '13c08d');
										?>
										<a class="<? if($themeColor == $arThemesColor[0]): ?>active <? endif; ?>style-switcher__colors-item"
										   href="javascript:void(0)"
										   style="background: #e87162;"
										   data-theme-color="e87162"></a>
										<a class="<? if($themeColor == $arThemesColor[1]): ?>active <? endif; ?>style-switcher__colors-item"
										   href="javascript:void(0)"
										   style="background: #05a4cd;"
										   data-theme-color="05a4cd"></a>
										<a class="<? if($themeColor == $arThemesColor[2]): ?>active <? endif; ?>style-switcher__colors-item"
										   href="javascript:void(0)"
										   style="background: #13c08d;"
										   data-theme-color="13c08d"></a>
										<input class="<? if(!in_array($themeColor, $arThemesColor)): ?>active <? endif; ?>style-switcher__colors-item"
										       id="colorpicker"
										       type="text"
										       style="background: #f00;">
									</div>
								</div>
								<div class="style-switcher__reset">
									<button class="btn btn-style1 style-switcher__reset-btn"
									        data-theme-color="e87162"
									        type="button"><?=GetMessage('MAGWAI_FOOTER_SETTING_DEFAULT')?></button>
								</div>
							</form>
						</div>
					</div>

				<? endif; ?>
				<?\Bitrix\Main\Page\Frame::getInstance()->finishDynamicWithID("template-settings", "");?>

			</div>
		</footer>
	</div>
	<?$APPLICATION->IncludeComponent(
		"magwai:unsubscribe",
		"lite",
		array(
			"MAIL_ID" => $_REQUEST["mid"],
			"MAIL_MD5" => $_REQUEST["mhash"],
			"COMPONENT_TEMPLATE" => ".default",
			"IBLOCK_TYPE" => "magwai_conf",
			"IBLOCK_ID" => "11"
		),
		false,
		array('HIDE_ICONS' => 'Y')
	);?>

	<? if($_REQUEST["InvId"]): // если есть запрос от Robokassa ?>
		<? $componentBuyPath = $_SERVER['DOCUMENT_ROOT'] . SITE_DIR . "include/conf/component-buy.php"; ?>
		<? if(file_exists($componentBuyPath)): ?>
			<? require $componentBuyPath; ?>
		<? endif; ?>
	<? endif; ?>

	<?$APPLICATION->IncludeComponent("bitrix:main.include", "", array(
			"AREA_FILE_SHOW" => "file",
			"PATH" => SITE_DIR . "include/conf/preloader.php",
		),
		false,
		array('HIDE_ICONS' => 'Y')
	);?>

    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-106440577-1', 'auto');
        ga('send', 'pageview');
    </script>

</body>
</html>