<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
	"NAME" => GetMessage("MAGWAI_BLOCK_NAME"),
	"DESCRIPTION" => GetMessage("MAGWAI_BLOCK_DESCRIPTION"),
	"ICON" => "/images/detail.gif",
	"SORT" => 10,
	"CACHE_PATH" => "Y",
	"PATH" => array(
		"ID" => "magwai",
		"NAME" => GetMessage('MAGWAI_BLOCK_PM_DESC_PATH_NAME'),
		"SORT" => 10,
	),
);

?>