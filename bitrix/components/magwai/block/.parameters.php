<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentParameters = array(
	"GROUPS" => array(
	),
	"PARAMETERS" => array(
		"ID" => Array(
			"NAME" => GetMessage('MAGWAI_BLOCK_PARAMS_ID'),
			"TYPE" => "STRING",
			"DEFAULT" => "",
		),
	),
);