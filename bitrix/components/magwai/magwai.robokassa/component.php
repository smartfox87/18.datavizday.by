<? if(!defined("B_PROLOG_INCLUDED")||B_PROLOG_INCLUDED!==true)die();

if (!CModule::IncludeModule("iblock"))
{
	ShowError(GetMessage("IBLOCK_MODULE_NOT_INSTALLED"));
	return;
}

global $USER;
$arParamsRequired = array(
	'PAYMENT_TEST_MODE',
	'PAYMENT_LOGIN',
	'PAYMENT_PASSWORD',
	'PAYMENT_PASSWORD_2',
	'EVENT_TYPE_NAME',
);
foreach ($arParams as $paramKey => $paramValue) {
	if (in_array($paramKey, $arParamsRequired) && empty($paramValue)) {
		ShowError(GetMessage('MAGWAI_PAY_PARAMS_INVALID'));
		return;
	}
}

if (!function_exists('getProduct')) {
	/**
	 * @param $ID
	 * @param array $arProperty
	 * @return bool
	 */
	function getProduct($ID, $arProperty = array('PROPERTY_PRICE')) {
		$arSort = array('SORT' => 'ASC');
		$arFilter = array('ID' => $ID);
		$arSelectFields = array_merge(array('ID','ACTIVE', 'NAME'), $arProperty);
		$arProduct = CIBlockElement::GetList($arSort, $arFilter, FALSE, FALSE, $arSelectFields)->Fetch();

		if (in_array('PROPERTY_PRICE', $arProperty) && $arProduct['PROPERTY_PRICE_VALUE']) {
			$arProduct['PROPERTY_PRICE_VALUE'] = number_format($arProduct['PROPERTY_PRICE_VALUE'], 2, '.', '');
		}

		return ($arProduct) ? $arProduct : false;
	}
}
if (!function_exists('getOrder')) {
	/**
	 * @param $ID
	 * @param array $arProperty
	 * @return bool
	 */
	function getOrder($ID, $arProperty = array()) {
		$arSort = array('SORT' => 'ASC');
		$arFilter = array('ID' => $ID);
		$arSelect = array_merge(array('ID', 'IBLOCK_ID', 'ACTIVE', 'NAME'), $arProperty);
		$obOrder = CIBlockElement::GetList($arSort, $arFilter, FALSE, FALSE, $arSelect)->GetNextElement();
		$arOrder = $obOrder->GetFields();
		$arOrder['PROPERTIES'] = $obOrder->GetProperties();

		return ($arOrder) ? $arOrder : false;
	}
}
if (!function_exists('getSignature')) {
	/**
	 * @param $cost
	 * @param $orderID
	 * @param $password
	 * @param string $shpString
	 * @return string
	 */
	function getSignature($cost, $orderID, $password, $shpString = '') {
		$string = "{$cost}:{$orderID}:{$password}";
		if (!empty($shpString)) {
			$string .= ":$shpString";
		}
		return strtoupper(md5($string));
	}
}

if($this->startResultCache())
{
	$arResult = array();

	if ($arParams['REQUEST_ORDER_ID'] && !$arParams['IS_SETTING'])
	{ // если получили запрос от robokassa

		if ($arParams['REQUEST_SIGNATURE'])
		{ // result OR success

			$arResult['STATUS']['COST'] = htmlspecialcharsEx($_REQUEST["OutSum"]);
			$arResult['STATUS']['ORDER_ID'] = htmlspecialcharsEx($_REQUEST["InvId"]);
			$arResult['STATUS']['PRODUCT_ID'] = htmlspecialcharsEx($_REQUEST["Shp_item"]);
			$arResult['STATUS']['SIGNATURE'] = strtoupper(htmlspecialcharsEx($_REQUEST["SignatureValue"]));

			$signatureResult  = getSignature(
				$arResult['STATUS']['COST'],
				$arResult['STATUS']['ORDER_ID'],
				$arParams['PAYMENT_PASSWORD_2'],
				'Shp_item=' . $arResult['STATUS']['PRODUCT_ID']
			);

			$signatureSuccess  = getSignature(
				$arResult['STATUS']['COST'],
				$arResult['STATUS']['ORDER_ID'],
				$arParams['PAYMENT_PASSWORD'],
				'Shp_item=' . $arResult['STATUS']['PRODUCT_ID']
			);

			switch($arResult['STATUS']['SIGNATURE'])
			{
				case $signatureResult: // result

					// установка значения "Оплачен"
					$arOrder = CIBlockElement::GetByID($arResult['STATUS']['ORDER_ID'])->Fetch();
					$arPropertyEnum = CIBlockPropertyEnum::GetList(
						array("SORT"=>"ASC"),
						array(
							'XML_ID' => 'ORDER_PAID',
							'IBLOCK_ID'=> $arOrder['IBLOCK_ID'],
							'CODE' => 'PAY'
						)
					)->Fetch();
					$payPropertyPaidEnumID = $arPropertyEnum['ID'];
					
					CIBlockElement::SetPropertyValuesEx(
						$arResult['STATUS']['ORDER_ID'],
						false,
						array('PAY' => $payPropertyPaidEnumID)
					);

					$arOrder = getOrder($arResult['STATUS']['ORDER_ID'], array('PROPERTY_*'));

					/* отправка почтового уведомления */
					if ($arOrder['PROPERTIES']['EMAIL']['VALUE'])
					{
						$arProduct = getProduct($arResult['STATUS']['PRODUCT_ID']);

						$arText = array();
						$arText[] = '<p>' . GetMessage('MAGWAI_ROBOKASSA_T_USER_NAME') . ': ' . $arOrder['NAME'] . '</p>';
						foreach ($arOrder['PROPERTIES'] as $arProperty) {
							$value = '';
							switch ($arProperty['PROPERTY_TYPE']) {
								case 'E':
									$arElementLink = CIBlockElement::GetByID($arProperty['VALUE'])->Fetch();
									$value = $arElementLink['NAME'];
									break;
								default:
									$value = $arProperty['VALUE'];
							}
							$value = ($value) ? $value : '-';
							$arText[] = '<p>' . $arProperty['NAME'] . ': ' . $value . '</p>';
						}

						$arSend = array(
							'AUTHOR_EMAIL' => $arOrder['PROPERTIES']['EMAIL']['VALUE'],
							'ORDER_ID' => $arResult['STATUS']['ORDER_ID'],
							'PRODUCT_NAME' => $arProduct['NAME'],
							'PRODUCT_PRICE' => $arProduct['PROPERTY_PRICE_VALUE'],
							'TEXT' => implode("\r\n", $arText)
						);
						$eventName = $arParams['EVENT_TYPE_NAME'];
						CEvent::Send($eventName, SITE_ID, $arSend);
					}

					/* показать ответ Robokassa об успешной проверке оплаты */
					$GLOBALS['APPLICATION']->RestartBuffer();
					echo "OK{$arResult['STATUS']['ORDER_ID']}\n";
					die;
					/* end */
				case $signatureSuccess: // success
					$arResult['ACTION_CODE'] = 'success';
					break;
				default:
					ShowError('Bad signature');
					return;
			}

		}
		else
		{ // fail
			$arResult['ACTION_CODE'] = 'fail';
		}

	}
	else
	{ // если нет запросов, то формируем оплату
		if (!$arParams['IS_SETTING']) // компонент используется не в блоке настроек
		{
			$arProduct = getProduct($arParams['PRODUCT_ID']);
			if (!$arProduct['PROPERTY_PRICE_VALUE'])
			{
				ShowError('Price empty');
				return;
			}

			$arResult['FORM_DATA'] = array(
				'PAYMENT_LOGIN' => $arParams['PAYMENT_LOGIN'],
				'ORDER_ID' => $arParams['ORDER_ID'],
				'PRODUCT_ID' => $arProduct['ID'],
				'PRODUCT_NAME' => $arProduct['NAME'],
				'COST' => $arProduct['PROPERTY_PRICE_VALUE'],
				'SIGNATURE' => md5(
					$arParams['PAYMENT_LOGIN']
					. ':' . $arProduct['PROPERTY_PRICE_VALUE']
					. ':' . $arParams['ORDER_ID']
					. ':' . $arParams['PAYMENT_PASSWORD']
					. ':Shp_item=' . $arProduct['ID']
				),
				'DESCRIPTION' => $arProduct['NAME'],
				'IS_TEST' => ($arParams['PAYMENT_TEST_MODE'] == 'Y'),
				'PAYMENT_URL' => 'https://auth.robokassa.ru/Merchant/Index.aspx',
			);
		}
		$arResult['ACTION_CODE'] = 'form';
	}

	$this->setResultCacheKeys(array(
		'FORM_DATA', 'STATUS'
	));
	$this->includeComponentTemplate();
}
