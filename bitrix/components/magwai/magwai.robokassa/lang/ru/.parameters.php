<?
$MESS ['MAGWAI_PAY_GROUP_PAY_OPTION'] = 'Параметры для подключния к платежной системе «Robokassa»';
$MESS ['MAGWAI_PAY_GROUP_SYSTEM'] = 'Технические параметры (не изменять)';
$MESS ['MAGWAI_PAY_GROUP_EVENT'] = 'Почтовые уведомления';

$MESS ['MAGWAI_PAY_PAYMENT_TEST_MODE'] = 'Режим тестирования';
$MESS ['MAGWAI_PAY_LOGIN'] = 'Логин';
$MESS ['MAGWAI_PAY_PAYMENT_PASSWORD'] = 'Платежный пароль';
$MESS ['MAGWAI_PAY_PAYMENT_PASSWORD_2'] = 'Платежный пароль 2';

$MESS ['MAGWAI_PAY_STATE_ACTIVE'] = 'Включён';
$MESS ['MAGWAI_PAY_STATE_DISABLED'] = 'Выключен';

$MESS ['MAGWAI_PAY_EMAIL_TYPE_EVENT'] = 'Тип почтового события для уведомлений';

$MESS ['MAGWAI_PAY_PRODUCT_ID'] = "ID товара";
$MESS ['MAGWAI_PAY_ORDER_ID'] = "ID заказа";
$MESS ['MAGWAI_PAY_REQUEST_ORDER_ID'] = 'получаемый ID заказа';
$MESS ['MAGWAI_PAY_REQUEST_SIGNATURE'] = 'получаемый SIGNATURE';
$MESS ['MAGWAI_PAY_AUTHOR_EMAIL'] = 'Email покупателя';
//$MESS ['MAGWAI_PAY_IS_SETTING'] = 'Отображать компонент только для настроек параметров компонента';
?>