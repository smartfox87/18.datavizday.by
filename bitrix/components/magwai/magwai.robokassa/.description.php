<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
	"NAME" => GetMessage('MAGWAI_PAY_DESC_NAME'),
	"DESCRIPTION" => GetMessage('MAGWAI_PAY_DESC_DESCRIPTION'),
	"ICON" => "/images/magwai-component.gif",
	"CACHE_PATH" => "Y",
	"PATH" => array(
		"ID" => "magwai",
		"NAME" => GetMessage('MAGWAI_PAY_DESC_PATH_NAME'),
		"SORT" => 10,
	),
);
?>