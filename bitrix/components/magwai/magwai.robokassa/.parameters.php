<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

if (!CModule::IncludeModule("iblock"))
	return;

/* �� */
$dbIBlockType = CIBlockType::GetList(
	array("sort" => "asc"),
	array("ACTIVE" => "Y")
);
while ($arIBlockType = $dbIBlockType->Fetch())
{
	if ($arIBlockTypeLang = CIBlockType::GetByIDLang($arIBlockType["ID"], LANGUAGE_ID))
	{
		$arIBlockTypeList[$arIBlockType["ID"]] = $arIBlockTypeLang["NAME"];
	}
}
/* ���� �������� �������  */
$arFilter["LID"] = 'ru';
$arEvent = Array();
$dbType = CEventType::GetList($arFilter, $order = "ASC");
while ($arType = $dbType->GetNext())
{
	$arEventTypes[$arType["EVENT_NAME"]] = $arType["NAME"];
}

$arComponentParameters = array(
	"GROUPS" => array(
		"PAY" => array(
			"NAME" => GetMessage("MAGWAI_PAY_GROUP_PAY_OPTION"),
			"SORT" => "10"
		),
		"EVENT" => array(
			"NAME" => GetMessage("MAGWAI_PAY_GROUP_EVENT"),
			"SORT" => "20"
		),
		"SYSTEM" => array(
			"NAME" => GetMessage("MAGWAI_PAY_GROUP_SYSTEM"),
			"SORT" => "30"
		),
	),

	"PARAMETERS" => array(
		"PAYMENT_TEST_MODE" => array(
			"PARENT" => "PAY",
			"NAME" => '* ' . GetMessage("MAGWAI_PAY_PAYMENT_TEST_MODE"),
			"TYPE" => "LIST",
			"ADDITIONAL_VALUES" => "N",
			"VALUES" => array('Y' => GetMessage("MAGWAI_PAY_STATE_ACTIVE"), 'N' => GetMessage("MAGWAI_PAY_STATE_DISABLED")),
			"DEFAULT" => "Y",
			"REFRESH" => "N",
			"SORT" => "10"
		),
		"PAYMENT_LOGIN" => Array(
			"PARENT" => "PAY",
			"NAME" => '* ' . GetMessage("MAGWAI_PAY_LOGIN"),
			"TYPE" => "STRING",
			"DEFAULT" => "",
			"SORT" => "20"
		),
		"PAYMENT_PASSWORD" => Array(
			"PARENT" => "PAY",
			"NAME" => '* ' . GetMessage("MAGWAI_PAY_PAYMENT_PASSWORD"),
			"TYPE" => "STRING",
			"DEFAULT" => "",
			"SORT" => "30"
		),
		"PAYMENT_PASSWORD_2" => Array(
			"PARENT" => "PAY",
			"NAME" => '* ' . GetMessage("MAGWAI_PAY_PAYMENT_PASSWORD_2"),
			"TYPE" => "STRING",
			"DEFAULT" => "",
			"SORT" => "40"
		),


		"EVENT_TYPE_NAME" => Array(
			"PARENT" => "EVENT",
			"NAME" => '* ' . GetMessage("MAGWAI_PAY_EMAIL_TYPE_EVENT"),
			"TYPE" => "LIST",
			"VALUES" => $arEventTypes,
			"DEFAULT" => "",
			"MULTIPLE" => "N",
			"COLS" => 25,
		),


		"PRODUCT_ID" => Array(
			"PARENT" => "SYSTEM",
			"NAME" => GetMessage("MAGWAI_PAY_PRODUCT_ID"),
			"TYPE" => "STRING",
			"DEFAULT" => '={$_REQUEST["buy"]["PRODUCT_ID"]}',
		),
		"ORDER_ID" => Array(
			"PARENT" => "SYSTEM",
			"NAME" => GetMessage("MAGWAI_PAY_ORDER_ID"),
			"TYPE" => "STRING",
			"DEFAULT" => '={$_REQUEST["buy"]["ORDER_ID"]}',
		),
		"REQUEST_ORDER_ID" => Array(
			"PARENT" => "SYSTEM",
			"NAME" => GetMessage("MAGWAI_PAY_REQUEST_ORDER_ID"),
			"TYPE" => "STRING",
			"DEFAULT" => '={$_REQUEST["InvId"]}',
		),
		"REQUEST_SIGNATURE" => Array(
			"PARENT" => "SYSTEM",
			"NAME" => GetMessage("MAGWAI_PAY_REQUEST_SIGNATURE"),
			"TYPE" => "STRING",
			"DEFAULT" => '={$_REQUEST["SignatureValue"]}',
		),
//		"IS_SETTING" => Array(
//			"PARENT" => "SYSTEM",
//			"NAME" => GetMessage("MAGWAI_PAY_IS_SETTING"),
//			"TYPE" => "CHECKBOX",
//			"DEFAULT" => 'N',
//		),

		"CACHE_TIME" => array(
			"DEFAULT" => 36000000
		),
	)
);

?>