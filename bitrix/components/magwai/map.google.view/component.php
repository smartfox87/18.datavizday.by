<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arParams['MAP_ID'] =
	(strlen($arParams["MAP_ID"])<=0 || !preg_match("/^[A-Za-z_][A-Za-z01-9_]*$/", $arParams["MAP_ID"])) ?
		'MAP_'.RandString() : $arParams['MAP_ID'];

$strPositionInfo = $arParams['~MAP_DATA'];

$arResult['POSITION'] = unserialize($strPositionInfo);

/* ��� ��������� ���������� ���������� ����� ����� ������ - ��������� ��������������� ������ ����� ����������, SITE_CHARSET �� ������� */
if (!$arResult['POSITION']) {
	$arResult['POSITION'] = unserialize(iconv('UTF-8', 'windows-1251', $strPositionInfo));
	foreach($arResult['POSITION']['PLACEMARKS'] as &$arPlacemarks) {
		$arPlacemarks['TEXT'] = iconv('windows-1251', 'UTF-8', $arPlacemarks['TEXT']);
	}
}

if (($arParams['~MAP_DATA']) && CheckSerializedData($strPositionInfo) && ($arResult['POSITION']))
{
	if (is_array($arResult['POSITION']) && is_array($arResult['POSITION']['PLACEMARKS']) && ($cnt = count($arResult['POSITION']['PLACEMARKS'])))
	{
		for ($i = 0; $i < $cnt; $i++)
		{
			$arResult['POSITION']['PLACEMARKS'][$i]['TEXT'] = str_replace('###RN###', "\r\n", $arResult['POSITION']['PLACEMARKS'][$i]['TEXT']);
		}
	}

}
else
{
	$arResult['POSITION'] = array(
		'google_lon' => 37.64,
		'google_lat' => 55.76,
		'google_scale' => 10,
	);
}

$this->IncludeComponentTemplate();
?>