<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

if (!CModule::IncludeModule("iblock"))
{
	ShowError(GetMessage("IBLOCK_MODULE_NOT_INSTALLED"));
	return;
}

$arResult["ERROR"] = "";
$arResult["SUCCESS"] = "";

if ($arParams["MAIL_ID"] && $arParams["MAIL_MD5"])
{
	$arResult['CONFIRM_UNSUBSCIBE'] = 'Y';
	
	$arOrder = array("SORT" => "ASC");
	$arFilter = array(
		'IBLOCK_ID' => $arParams["IBLOCK_ID"],
		'ID' => $arParams["MAIL_ID"],
		"ACTIVE" => "Y",
	);
	$arSelectFields = array("ID", "ACTIVE", "ACTIVE_FROM", "NAME", "PROPERTY_EMAIL");
	$rsSub = CIBlockElement::GetList($arOrder, $arFilter, FALSE, FALSE, $arSelectFields);
	if ($arSub = $rsSub->Fetch())
	{
		$currentHash = md5($arSub['ACTIVE_FROM'] . $arSub['PROPERTY_EMAIL_VALUE']);
		if ($currentHash != $arParams["MAIL_MD5"])
		{
			$arResult["ERROR"] = GetMessage("INCORRECT_HASH_L");
		} else
		{
			$arResult["EMAIL"] = $arSub["EMAIL"];
		}
	} else {
		$arResult["ERROR"] = GetMessage("SUBSCRIBE_NOT_FOUND_L");
	}

	if ($arResult["ERROR"] == "")
	{
		$element = new CIBlockElement;
		$arUpdate = Array(
			"ACTIVE" => "N",
		);
		if ($element->Update($arParams["MAIL_ID"], $arUpdate)) {
			$arResult["SUCCESS"] = 'Y';
		} else {
			$arResult["ERROR"] = 'Error update element';
		}
	}
}
$this->IncludeComponentTemplate();
?>