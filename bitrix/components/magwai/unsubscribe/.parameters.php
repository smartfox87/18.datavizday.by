<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

if(!CModule::IncludeModule("iblock"))
	return;

$arTypesEx = CIBlockParameters::GetIBlockTypes(array("-"=>" "));

$arIBlocks=array();
$db_iblock = CIBlock::GetList(array("SORT"=>"ASC"), array("SITE_ID"=>$_REQUEST["site"], "TYPE" => ($arCurrentValues["IBLOCK_TYPE"]!="-"?$arCurrentValues["IBLOCK_TYPE"]:"")));
while($arRes = $db_iblock->Fetch())
	$arIBlocks[$arRes["ID"]] = $arRes["NAME"];



$arComponentParameters = array(
	"PARAMETERS" => array(
		"IBLOCK_TYPE" => array(
			"PARENT" => "BASE",
			"NAME" => GetMessage("MAGWAI_UNSUBSCRIBE_T_IBLOCK_DESC_LIST_TYPE"),
			"TYPE" => "LIST",
			"VALUES" => $arTypesEx,
			"DEFAULT" => "news",
			"REFRESH" => "Y",
		),
		"IBLOCK_ID" => array(
			"PARENT" => "BASE",
			"NAME" => GetMessage("MAGWAI_UNSUBSCRIBE_T_IBLOCK_DESC_LIST_ID"),
			"TYPE" => "LIST",
			"VALUES" => $arIBlocks,
			"DEFAULT" => '={$_REQUEST["ID"]}',
			"ADDITIONAL_VALUES" => "Y",
			"REFRESH" => "Y",
		),
		"MAIL_ID" => Array(
			"PARENT" => "BASE",
			"NAME" => GetMessage("MAGWAI_UNSUBSCRIBE_MAIL_ID_L"),
			"TYPE" => "STRING",
			"DEFAULT" => '={$_REQUEST["mid"]}',
		),
		"MAIL_MD5" => Array(
			"PARENT" => "BASE",
			"NAME" => GetMessage("MAGWAI_UNSUBSCRIBE_MAIL_MD5_L"),
			"TYPE" => "STRING",
			"DEFAULT" => '={$_REQUEST["mhash"]}',
		),
	),
);
?>
