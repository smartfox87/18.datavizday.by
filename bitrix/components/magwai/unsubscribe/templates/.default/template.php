<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
$this->setFrameMode(true);
?>
<? $frame = $this->createFrame()->begin(''); ?>
	<? if ($arResult["CONFIRM_UNSUBSCIBE"] == "Y"): ?>
		<? if (!empty($arResult["ERROR"])): ?>
			<? \Bitrix\Main\Diag\Debug::dump($arResult['ERROR']); ?>
		<? else: ?>
			<? \Bitrix\Main\Diag\Debug::dump(GetMessage('MAGWAI_UNSUBSCRIBE_SUCCESS_UNSUBSCRIBE_L')); ?>
		<? endif; ?>
	<? endif; ?>
<? $frame->end(); ?>