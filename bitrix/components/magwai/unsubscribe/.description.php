<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
	"NAME" => GetMessage("MAGWAI_UNSUBSCRIBE_NAME"),
	"DESCRIPTION" => GetMessage("MAGWAI_UNSUBSCRIBE_DESCRIPTION"),
	"ICON" => "/images/detail.gif",
	"SORT" => 10,
	"CACHE_PATH" => "Y",
	"PATH" => array(
		"ID" => "magwai",
		"NAME" => GetMessage('MAGWAI_UNSUBSCRIBE_PM_DESC_PATH_NAME'),
		"SORT" => 10,
	),
);

?>