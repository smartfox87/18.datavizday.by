<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
	"NAME" => GetMessage("MAGWAI_IBLOCK_ELEMENT_ADD_FORM_NAME"),
	"DESCRIPTION" => GetMessage("MAGWAI_IBLOCK_ELEMENT_ADD_FORM_DESCRIPTION"),
	"ICON" => "/images/detail.gif",
	"SORT" => 10,
	"CACHE_PATH" => "Y",
	"PATH" => array(
		"ID" => "magwai",
		"NAME" => GetMessage("MAGWAI_T_IBLOCK_PATH_NAME"),
		"SORT" => 10,
	),
);
?>