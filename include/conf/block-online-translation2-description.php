<div class="ns_materials">
    <a href="http://www.tableau.com/partner-trial?id=72945" class="btn btn-style1 btn-listeners">Скачать Tableau Desktop</a>
    <a href="http://www.tableau.com/trial/tableau-server?id=72945" class="btn btn-style1 btn-listeners">Скачать Tableau Server</a>
    <a href="https://www.tableau.com/trial/tableau-online?id=72945" class="btn btn-style1 btn-listeners">Подключить Tableau Online</a>
</div>
<br>
<table width="100%">
    <tbody>
    <tr>
        <td>
            <ul>
                <li> <a href="/upload/whitepapers/5_невыполнимых_задач_для_электронных_таблиц.pdf"><span style="color: #ffffff;"><u>5 невыполнимых задач для электронных таблиц</u></span></a></li>
                <li><a href="/upload/whitepapers/Magic_Quadrant_Gartner.pdf"><span style="color: #ffffff;"><u> Magic Quadrant Gartner</u></span></a></li>
                <li> <a href="/upload/whitepapers/10_тенденций_бизнес-аналитики_в_2017_году.pdf"><span style="color: #ffffff;"><u>10 тенденций бизнес-аналитики в 2017 году</u></span></a></li>
                <li><a href="/upload/whitepapers/5_рекомендаций_для_мобильной_бизнес-аналитики.pdf"><span style="color: #ffffff;"><u> 5 рекомендаций для мобильной бизнес-аналитики</u></span></a></li>
                <li><a href="/upload/whitepapers/Designing_Great_Visualizations.pdf"> <span style="color: #ffffff;"><u>Designing Great Visualizations</u></span></a></li>
            </ul>
        </td>
        <td>
            <ul>
                <li> <a href="/upload/whitepapers/Современный_подход_к_бизнес-аналитике.pdf"><span style="color: #ffffff;"><u>Современный подход к бизнес-аналитике</u></span></a></li>
                <li><a href="/upload/whitepapers/10_главных_тенденций_2017_года_в_области_больших_данных.pdf"><span style="color: #ffffff;"><u>10 главных тенденций 2017 года в области больших данных</u></span></a></li>
                <li> <a href="/upload/whitepapers/Top_10_Cloud_trends_for_2017.pdf"><span style="color: #ffffff;"><u>Top 10 Cloud trends for 2017</u></span></a></li>
                <li><a href="/upload/whitepapers/Как_достичь_легкости_восприятия_отчетов.pdf"><span style="color: #ffffff;"><u> Как достичь легкости восприятия отчетов</u></span></a></li>
                <li><a href="/upload/whitepapers/10 Best Practices for Building Effective.pdf"><span style="color: #ffffff;"><u>10 Best Practices for Building Effective Dashboards</u></span></a></li>
            </ul>
        </td>
    </tr>
    </tbody>
</table>
<p>
    &nbsp;
</p>